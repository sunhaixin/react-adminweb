import React, { Component } from 'react';
import StandardTable from '@/components/StandardTable';
import { CommonInterface } from '@/services/params';
import  ShareManAge from '../ShareManAge';

const { WebNoData } = CommonInterface;

export default class WebDataManAgeNo extends Component{
    render(){
        return (
            <ShareManAge 
            title= "PcWeb测评数据(无身份证号)" 
            shareName= "WebNoDataList"
            shareValue={WebNoData}
            exportUrl="admin/data/pcweb2/export"
            />
        )
    }
}