import React, { Component } from 'react';
import { connect } from 'dva';
import StandardTable from '@/components/StandardTable';
import { Card, Row, Col, Icon, Select, Form, DatePicker, Button} from 'antd';
import { CommonInterface } from '@/services/params';
import { EvaList }  from '@/services/datamanage';
import styles from './common.less';

const { PcDatas, formItemLayout, DataManAgeColumns } = CommonInterface;
const FormItem = Form.Item;
const Option = Select.Option;

@connect(({datamanage, loading }) => ({
    datamanage,
    loadingWebYes: loading.effects['datamanage/WebYesDataList'],
    loadingWebNo: loading.effects['datamanage/WebNoDataList'],
    loadingPcEva: loading.effects['datamanage/PcEvaDataList'],
    loadingAppEva: loading.effects['datamanage/AppEvaDataList']
}))@Form.create()

export default class ShareManAge extends Component{
    state ={
        selectedRows: [],
        hid: "", eva: "",
        startValue: null,
        endValue: null
    }

    componentDidMount(){
        const { dispatch, shareName, shareValue } = this.props;
        dispatch({
          type: `datamanage/${shareName}`,
          payload: shareValue
        });
    }

    renderForm(){
        const { 
            dispatch,
            form:{ getFieldDecorator, },
            datamanage: { hospitalList }
          } = this.props;
        
        return(
            <Row gutter={{ md: 8, lg: 24 }}>
                <Col md={6}>
                    <FormItem
                        {...formItemLayout}
                        label="请选择医院"
                    >
                         <Select 
                         allowClear
                         placeholder="全部医院"
                         onChange={(e) => this.changeHospital(e)}
                         onFocus={() =>dispatch({
                            type: 'datamanage/fetchhospital',
                            payload: PcDatas
                         })}
                         
                         style={{ width: 200}}>
                             {hospitalList? hospitalList.map((item) =>{
                                return(
                                <Option key={item.key}>{item.Name}</Option>
                                )
                            }) : ''}
                        </Select>
                    </FormItem>
                </Col>
                <Col md={6}>
                    <FormItem
                        {...formItemLayout}
                        label="测评类型"
                    >
                         <Select 
                         allowClear
                         placeholder="全部测评"
                         onChange={(e) => this.changeEva(e)}
                         style={{ width: 200}}>
                             {EvaList.map((item) =>{
                                  return(
                                    <Option key={item.id}>{item.title}</Option>
                                    )
                             })}
                        </Select>
                    </FormItem>
                </Col>
                <Col md={6}>
                    <FormItem
                        {...formItemLayout}
                        label="起止时间"
                    >
                    <DatePicker 
                    onChange={(date, dateString) => 
                    this.onChangeStart(date, dateString)}/>
                    </FormItem>
                </Col>
                <Col md={6}>
                    <FormItem
                        {...formItemLayout}
                        label="截止时间"
                    >
                    <DatePicker
                     onChange={(date, dateString) => 
                     this.onChangeEnd(date, dateString)}/>
                    </FormItem>
                </Col>
                {/* <Col md={4}>
                    <Button onClick={() => this.exportList()}>导出数据</Button>
                </Col> */}
            </Row>
        )
    }

    changeHospital(e){
        const { dispatch, shareName, shareValue} = this.props;
        const { eva, startValue, endValue } = this.state;
        if(e == undefined){
            e = 0;
            this.setState({  hid: e });
        }else{
            this.setState({  hid: e });
        }
        let fixed =`hid=${e}&page=1&start=0&limit=10000`;
        let wEva= '',  WstartValue = '',  WendValue = '';
        if (eva && eva != 0) {
            wEva = `&eva=${eva}`;
        }
        if (startValue && startValue != ''){
            WstartValue = `&starttime=${startValue}`;
        }
        if (endValue && endValue != ''){
            WendValue = `&endtime=${endValue}`;
        }
        dispatch({
            type: `datamanage/${shareName}`,
            payload: {
                url: shareValue.url,
                params: `${fixed}${wEva}${WstartValue}${WendValue}`
            }
        });
    }

    changeEva(e){
        const { dispatch, shareName, shareValue } = this.props;
        const { hid, startValue, endValue } = this.state;
        let fixed;
        if(e == undefined){
            e = 0;
            this.setState({  eva: e });
            fixed =`aid=${e}&page=1&start=0&limit=10000`;
        }else{
            this.setState({  eva: e });
            fixed =`eva=${e}&page=1&start=0&limit=10000`;
        }
        let whid= '',  WstartValue = '',  WendValue = '';
        if (hid) {
            whid = `&hid=${hid}`;
        }else{
            whid = `&hid=0`;
        }
        if (startValue && startValue != ''){
            WstartValue = `&starttime=${startValue}`;
        }
        if (endValue && endValue != ''){
            WendValue = `&endtime=${endValue}`;
        }
        dispatch({
            type: `datamanage/${shareName}`,
            payload: {
                url: shareValue.url,
                params: `${fixed}${whid}${WstartValue}${WendValue}`
            }
        });
    }

    onChangeStart(date, dateString){
        const { dispatch, shareName, shareValue } = this.props;
        this.setState({ startValue: dateString });
        let fixed = "page=1&start=0&limit=10000";
        const { hid , eva, endValue } = this.state;
        let whid = '', wEva= '', WendValue = '';
        if (hid) {
            whid = `&hid=${hid}`;
        }else{
            whid = `&hid=0`;
        }
        if (eva && eva != 0) {
            wEva = `&eva=${eva}`;
        }
        if (endValue && endValue != ''){
            WendValue = `&endtime=${endValue}`;
        }
        dispatch({
          type: `datamanage/${shareName}`,
          payload: {
            url: shareValue.url,
            params: `${fixed}${whid}${wEva}${WendValue}&starttime=${dateString}`
          }
        });
    }

    onChangeEnd(date, dateString){
        const { dispatch, shareName, shareValue } = this.props;
        this.setState({ endValue: dateString });
        let fixed = "page=1&start=0&limit=10000";
        const { hid , eva, startValue } = this.state;
        let whid = '', wEva= '', WstartValue = '';
        if (hid) {
            whid = `&hid=${hid}`;
        }else{
            whid = `&hid=0`;
        }
        if (eva && eva != 0) {
            wEva = `&eva=${eva}`;
        }
        if (startValue && startValue != ''){
            WstartValue = `&starttime=${startValue}`;
        }
        dispatch({
          type: `datamanage/${shareName}`,
          payload: {
            url: shareValue.url,
            params: `${fixed}${whid}${wEva}${WstartValue}&endtime=${dateString}`
          }
        });
    }

    // exportList(){
    //     const { exportUrl, dispatch } = this.props;
    //     const { hid , eva, startValue, endValue } = this.state;
    //     let whid = '', wEva= '', WstartValue = '', WendValue = '';
    //     if (hid) {
    //         whid = `hid=${hid}`;
    //     }else{
    //         whid = `hid=0`;
    //     }
    //     if (eva && eva != 0) {
    //         wEva = `&eva=${eva}`;
    //     }
    //     if (startValue && startValue != ''){
    //         WstartValue = `&startValue=${startValue}`;
    //     }
    //     if (endValue && endValue != ''){
    //         WendValue = `&endtime=${endValue}`;
    //     }
    //     dispatch({
    //         type: "datamanage/exportData",
    //         payload: {
    //           url: exportUrl,
    //           params: `${whid}${wEva}${WstartValue}${WendValue}`
    //         }
    //       });
    // }

    columns = [
        {
            title: '记录类型',
            dataIndex: 'Title',
            width: '15%'
        },
        {
            title: '医院名称',
            dataIndex: 'HospitalName',
            width: '15%'
        },
        {
            title: '姓名',
            dataIndex: 'PatientName',
            width: '10%'
        },
        {
            title: '测评日期',
            dataIndex: 'CreateTime',
            width: '15%'
        },
        {
            title: '更新日期',
            dataIndex: 'UpdateTime',
            width: '15%'
        }
    ]

    ShareDataList(){
        const { 
            datamanage:{ 
                WebYesDataList, 
                AppEvaDataList,  
                PcEvaDataList,
                WebNoDataList
            }, title} = this.props;
        let list = [];
        if(title == "PcWeb测评数据(有身份证号)"){
            list = WebYesDataList;
        }else if(title == "PcWeb测评数据(无身份证号)"){
            list = WebNoDataList;
        }else if(title == "APP测评数据"){
            list = AppEvaDataList;
        }else if(title == "Pc测评数据"){
            list = PcEvaDataList;
        }
        return list
    }

    ShareDataLoading(){
        const { loadingWebYes, loadingWebNo, loadingPcEva, loadingAppEva, title } = this.props;
        let loadings;
        if(title == "PcWeb测评数据(有身份证号)"){
            loadings = loadingWebYes;
        }else if(title == "PcWeb测评数据(无身份证号)"){
            loadings = loadingWebNo;
        }else if(title == "APP测评数据"){
            loadings = loadingAppEva;
        }else if(title == "Pc测评数据"){
            loadings = loadingPcEva;
        }
        return loadings
    }

    shareDataColumns(){
      let columnsData = [];
      if(this.props.title == "PcWeb测评数据(无身份证号)"){
        columnsData = this.columns;   
      }else{
        columnsData = DataManAgeColumns;
      }
      return columnsData;
    }


    render(){
        return (
            <div style={{ flex: 1}}>
                <Row>
                    <Col>
                        <Card 
                        title={this.props.title}
                        bordered={false} style={{height: "780px"}}>    
                            <div className={styles.tableListForm}>
                                    {this.renderForm()}
                            </div>  
                            <Row>
                                <div className={styles.tableListOperator}>
                                    <StandardTable
                                        pagination={{ pageSize: 50 }} 
                                        scroll={{ y: 450 }}
                                        data={this.ShareDataList()}
                                        selectedRows={this.state.selectedRows}
                                        loading={this.ShareDataLoading()}
                                        columns={this.shareDataColumns()}
                                        pagination={{
                                            size: "small",
                                            pageSize: 100,
                                            showSizeChanger: true,
                                            showQuickJumper: true,
                                            showTotal: function (total, range) {  //设置显示一共几条数据
                                                return  '第 ' + range[0] + ' - ' + range[1] + ' 条'+ '  ' + `（共 ${total} 条）`; 
                                            }
                                        }}
                                        />
                                </div>
                            </Row>                 
                        </Card>                     
                    </Col>
                </Row>
            </div>
        )
    }
}