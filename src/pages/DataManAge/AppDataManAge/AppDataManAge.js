import React, { Component } from 'react';
import StandardTable from '@/components/StandardTable';
import { CommonInterface } from '@/services/params';
import  ShareManAge from '../ShareManAge';

const { AppEvaData } = CommonInterface;

export default class AppDataManAge extends Component{
    render(){
        return (
            <ShareManAge 
            title= "APP测评数据" 
            shareName= "AppEvaDataList"
            shareValue={AppEvaData}
            exportUrl="admin/data/app/export"
            />
        )
    }
}