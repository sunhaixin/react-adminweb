import * as services  from '@/services/datamanage';

export default {
  namespace: 'datamanage',

  state: {
    PcEvaDataList: [],
    AppEvaDataList: [],
    WebYesDataList: [],
    WebNoDataList: [],
    hospitalList: []
  },

  effects: {
     *PcEvaDataList({ payload:{ url, params }}, { call, put }){
        const res = yield call(services.fetchDataList, url, params);
        yield put({ type: 'save', payload: { PcEvaDataList: res } });
     },
     *AppEvaDataList({ payload:{ url, params }}, { call, put }){
        const res = yield call(services.fetchDataList, url, params);
        yield put({ type: 'save', payload: { AppEvaDataList: res } });
     },
     *WebYesDataList({ payload:{ url, params }}, { call, put }){
      const res = yield call(services.fetchDataList, url, params);
      yield put({ type: 'save', payload: { WebYesDataList: res } });
     },
     *WebNoDataList({ payload:{ url, params }}, { call, put }){
      const res = yield call(services.fetchDataList, url, params);
      yield put({ type: 'save', payload: { WebNoDataList: res } });
     },
     *fetchhospital({ payload:{ url, params } }, { call, put }){
        const res = yield call(services.fetchhospital, url, params);
        yield put({ type: 'save', payload: { hospitalList: res } });
     },
     *exportData({ payload:{ url, params } }, { call, put }){
        yield call(services.exportData, url, params);
     }
  },

  reducers: {
    save(state, { payload }) {
      return {
        ...state,
        ...payload,
      };
    }
  },
};
