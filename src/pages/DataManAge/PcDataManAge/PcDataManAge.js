import React, { Component } from 'react';
import StandardTable from '@/components/StandardTable';
import { CommonInterface } from '@/services/params';
import  ShareManAge from '../ShareManAge';

const { PcEvaData } = CommonInterface;

export default class PcDataManAge extends Component{
    render(){
        return (
            <ShareManAge 
            title= "Pc测评数据" 
            shareName= "PcEvaDataList"
            shareValue={PcEvaData}
            exportUrl="admin/data/pc/export"
            />
        )
    }
}