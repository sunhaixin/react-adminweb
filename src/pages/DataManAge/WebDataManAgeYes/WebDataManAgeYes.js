import React, { Component } from 'react';
import StandardTable from '@/components/StandardTable';
import { CommonInterface } from '@/services/params';
import  ShareManAge from '../ShareManAge';

const { WebYesData } = CommonInterface;

export default class WebDataManAgeYes extends Component{
    render(){
        return (
            <ShareManAge 
            title= "PcWeb测评数据(有身份证号)" 
            shareName= "WebYesDataList"
            shareValue={WebYesData}
            exportUrl="admin/data/pcweb/export"
            />
        )
    }
}