import * as services  from '@/services/api';
import { CommonInterface } from '@/services/params';

const { MedicData } = CommonInterface;

export default {
  namespace: 'medicConjoin',

  state: {
    MedicQueryList: [],
    LookYiliantiList: []
  },

  effects: {
     *fetchMedicQuery({ payload:{ url, params } }, { call, put }){
        const res = yield call(services.MedicQuery, url, params);
        yield put({ type: 'save', payload: { MedicQueryList: res } });
     },

     *fetchSearch({payload}, { call, put }){
        const { url, params  } = MedicData;
        const res = yield call(services.MedicQuery, url, params)
        var tempList = [];
        if(payload){
          res.forEach((e) =>{
              if(e.title.indexOf(payload) != -1){
                tempList.push(e);
              }
          })
          yield put({ type: 'save', payload: { MedicQueryList: tempList } })
        }else{
          yield put({ type: 'save', payload: { MedicQueryList: res } })
        }
    },

    *fetchAddYilianti({ payload:{ name }}, { call, put }){
      yield call(services.AddYilianti, name);
      const { url, params  } = MedicData;
      const res = yield call(services.MedicQuery, url, params);
      yield put({ type: 'save', payload: { MedicQueryList: res } });
    },

    *fetchLookYilianti({ payload: { url, params}}, { call, put}){
      const res = yield call(services.LookYilianti, url, params);
      yield put({ type: 'save', payload: { LookYiliantiList: res } });
    }
  },

  reducers: {
    save(state, { payload }) {
      return {
        ...state,
        ...payload,
      };
    }
  },
};
