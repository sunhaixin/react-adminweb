import React, { Component } from 'react';
import { connect } from 'dva';
import { Card, Row, Col, Tree, Spin, Form, Input, Button, Modal} from 'antd';
import { CommonInterface } from '@/services/params';

import styles from './MedicConjoin.less';

const FormItem = Form.Item;
const { TreeNode } = Tree;
const DirectoryTree = Tree.DirectoryTree;
const { MedicData, formItemLayout } = CommonInterface;

@connect(({medicConjoin, loading }) => ({
    medicConjoin,
    loading: loading.effects['medicConjoin/fetchMedicQuery'],
    loadings: loading.models.medicConjoin
}))@Form.create()

export default class MedicConjoin extends Component {
    state ={
        medicTitle: "",
        visible: false
    };

    componentDidMount() {
        const { dispatch } = this.props;
        dispatch({
          type: 'medicConjoin/fetchMedicQuery',
          payload: MedicData
        });
    }

    changeSearch(e){
        const { dispatch } = this.props;
        dispatch({
            type: 'medicConjoin/fetchSearch',
            payload: e.target.value
        });
    }

    renderForm(){
        return(
            <Row gutter={{ md: 8, lg: 24 }}>
                <Col sm={24}>
                    <FormItem>
                        <Button 
                        onClick={() => this.setState({visible: true})}
                        style={{ width: '100%'}}>新增医联体</Button>
                    </FormItem>
                    <FormItem label="搜索">
                        <Input onChange={(e) => this.changeSearch(e)}/> 
                    </FormItem>
                    <Modal
                        title="添加医联体"
                        visible={this.state.visible}
                        onOk={this.AddYiliantiOk}
                        onCancel={() => this.setState({ visible: false})}
                    >
                    { this.AddYilianti()}
                    </Modal>
                </Col>
            </Row>
          )
    }

    AddYilianti(){
        const { 
            form:{ getFieldDecorator }
          } = this.props;
         return(
          <Form>
             <FormItem
              {...formItemLayout}
              label="医联名称"
            >
               {getFieldDecorator('YiliantiName',{
                   rules: [{
                    required: true, message: '请填写医联体名称!',
                  }],
               })(
              <Input />
            )}
            </FormItem>
          </Form>
         )
    }

    AddYiliantiOk = () =>{
        const {  form:{ validateFieldsAndScroll }, dispatch } = this.props;
        validateFieldsAndScroll((err, values) => {
            dispatch({
                type: 'medicConjoin/fetchAddYilianti',
                payload: {
                    name: values.YiliantiName,
                }
            });
            this.setState({
                visible: false,
            })
        })
    }

    renderTreeNodes = data => data.map((item) => {
        return <TreeNode {...item} dataRef={item} Name={item} />;
    })

    SelectTreeNode(e,selectedKeys){
        const { dispatch } = this.props;
        if(selectedKeys && selectedKeys.selectedNodes.length!=0){
            this.setState({
                medicTitle: selectedKeys.selectedNodes[0].props.title
            })
            if(e[0]){
                dispatch({
                    type: 'medicConjoin/fetchLookYilianti',
                    payload:{
                        url: "admin/MedicalTreatmentCombination/hospital/query",
                        params: `_dc=1546072737732&mtcid=${e[0]}`
                    }
                })
            }
        }else{
            this.setState({
                medicTitle: ""
            })
            dispatch({
                type: 'medicConjoin/save',
                payload:{ LookYiliantiList:""}
            })
        }
    }

    render() {
        const { medicConjoin:{ MedicQueryList, LookYiliantiList } , loading, loadings } = this.props;
        const { medicTitle } = this.state;
        return (
            <div style={{ flex: 1}}>
                <Row gutter={24}>
                    <Col xl={6} lg={24} md={24} sm={24} xs={24}>
                        <Card title="医联体名称" bordered={false} style={{height: "780px"}}> 
                                <div className={styles.tableListForm}>
                                    {this.renderForm()}
                                </div>
                                <Row>
                                    <div className={styles.departBody}>
                                        <Spin spinning={loading}>
                                            <Tree 
                                            onSelect={(e,selectedKeys) => 
                                            this.SelectTreeNode(e,selectedKeys)}
                                            >
                                            {this.renderTreeNodes(MedicQueryList)}
                                            </Tree>
                                        </Spin>
                                </div>
                                </Row>
                        </Card>
                    </Col>
                    <Col xl={18} lg={24} md={24} sm={24} xs={24}>
                        <Card title={medicTitle? medicTitle: "医联体成员"} bordered={false} style={{height: "780px"}}>
                                <Row>
                                    <div className={styles.tableListOperator}>
                                        <Spin spinning={loadings}>
                                            <DirectoryTree
                                            multiple
                                            defaultExpandAll
                                            >
                                            {LookYiliantiList?LookYiliantiList.map((c, i) =>{
                                                return(
                                                    <TreeNode title={c.Text} key={c.Id}>
                                                    {c.Children? c.Children.map((c1,i1) =>{
                                                        return(
                                                            <TreeNode title={c1.Text} key={c1.Id}>
                                                                {c1.Children? c1.Children.map((c2,i2) =>{
                                                                    return(
                                                                        <TreeNode title={c2.Text} key={c2.Id}>
                                                                            {c2.Children? c2.Children.map((c3,i3) =>{
                                                                                return <TreeNode title={c3.Text} key={c3.Id} />
                                                                            }):""}
                                                                        </TreeNode>
                                                                    )
                                                                }):""}
                                                            </TreeNode>
                                                        )
                                                    }):""}
                                                    </TreeNode>
                                                )
                                            }):""}
                                            </DirectoryTree>
                                        </Spin>
                                       
                                    </div>
                                </Row>
                        </Card>
                    </Col>
                </Row>
            </div>
        )
    }
}
