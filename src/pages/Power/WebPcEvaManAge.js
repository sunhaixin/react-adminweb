import React, { Component } from 'react';
import { connect } from 'dva';
import { Card, Row, Col, Icon, Modal, Button} from 'antd';
import StandardTable from '@/components/StandardTable';
import styles from './common.less';
import { CommonInterface  } from '@/services/params';

const { iconstyle, WebEvaList } = CommonInterface;

@connect(({ powers, loading }) => ({
    powers,
    loading: loading.effects['powers/fetchPcWebEvaList'],
  }))

class WebPcEvaManAge extends Component {

    componentDidMount() {
      const { dispatch } = this.props;
      dispatch({
        type: 'powers/fetchPcWebEvaList',
        payload: WebEvaList
      });
    }

    state ={
        selectedRows: [],
    }

    columns =[
        {
            title: '医院编号',
            dataIndex: 'Id',
            width: '15%',
            align: 'center'
        },
        {
            title: '医院名称',
            dataIndex: 'hospitalName',
            width: '15%',
            align: 'center'
        },
        {
            title: '授权可用时间',
            dataIndex: 'ServiesEndTime',
            width: '15%',
            align: 'center'
        },
        {
            title: '剩余测评次数',
            dataIndex: 'Remainder',
            width: '15%',
            align: 'center'
        },
        {
            title: '总测评次数',
            dataIndex: 'ServiesCount',
            width: '15%',
            align: 'center'
        },
        {
            title: '操作',
            width: '15%',
            align: 'center',
            dataIndex: 'Operation',
            render: (text, record) =>(
                <span>
                    <Icon type="form" style={iconstyle}/> 
                </span>
            ),
        },
    ]
    
    render() {
      const { selectedRows } = this.state;
      const {  powers:{ PcWebEvaList }, loading } = this.props;
      return (
            <Card title="PC网页授权管理" bordered={false}>
                <div className={styles.tableList}>
                    <div className={styles.tableListOperator}>
                        <StandardTable
                        pagination={{ pageSize: 50 }} 
                        scroll={{ y: 450 }}
                        selectedRows={selectedRows}
                        data={PcWebEvaList}
                        loading={loading}
                        columns={this.columns}
                        pagination={{
                            size: "small",
                            pageSize: 50,
                            showSizeChanger: true,
                            showQuickJumper: true,
                            showTotal: function (total, range) {  //设置显示一共几条数据
                                return  '第 ' + range[0] + ' - ' + range[1] + ' 条'+ '  ' + `（共 ${total} 条）`; 
                            }
                        }}
                        />
                    </div>
                </div>
            </Card>
      );
    }
}

export default WebPcEvaManAge;