import React, { Component } from 'react';
import { connect } from 'dva';
import { Card, Row, Col, Icon, Modal, Button, Input, Form} from 'antd';
import StandardTable from '@/components/StandardTable';
import styles from './common.less';
import { CommonInterface  } from '@/services/params';

const FormItem = Form.Item;
const { iconstyle, amdinType, formItemLayout } = CommonInterface;

@connect(({ powers, loading }) => ({
    powers,
    loading: loading.effects['powers/fetchAdminType']
}))@Form.create()

class Rolepermission extends Component {

    componentDidMount() {
      const { dispatch } = this.props;
      dispatch({
        type: 'powers/fetchAdminType',
        payload: amdinType
      });
    }

    state ={
        selectedRows: [],
        visible: false
    }

    columns =[
        {
            title: '账号类型',
            dataIndex: 'typeName',
            width: '30%',
            align: 'center'
        },
        {
            title: 'Code',
            dataIndex: 'Code',
            width: '30%',
            align: 'center'
        },
        {
            title: '操作',
            width: '30%',
            align: 'center',
            dataIndex: 'Operation',
            render: (text, record) =>(
                <span>
                    <Icon type="form" style={iconstyle}/> 
                </span>
            ),
        },
    ]

    renderForm(){
      return(
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
            <Col md={4} sm={24}>
                <Button onClick={() =>{ this.setState({ visible: true })}}>新增账号类型</Button>
            </Col>
            <Modal
                title="新增账号类型"
                visible={this.state.visible} 
                onOk={this.AddRolepermissionOK}
                onCancel={() =>  
                this.setState({
                    visible: false,
                })}
                >
                {this.AddRolepermission()}
             </Modal>
        </Row>
     )
    }

    AddRolepermission(){
        const { 
            form:{ getFieldDecorator }
          } = this.props;
          return(
            <Form>
               <FormItem
                {...formItemLayout}
                label="账号类型"
              >
                 {getFieldDecorator('RolepermissionType',{
                     rules: [{
                      required: true, message: '请填写账号类型!',
                    }],
                 })(
                <Input />
              )}
              </FormItem>
              <FormItem
                {...formItemLayout}
                label="Code"
              >
                 {getFieldDecorator('RolepermissionCode',{
                     rules: [{
                      required: true, message: '请填写Code!',
                    }],
                 })(
                <Input />
              )}
              </FormItem>
            </Form>
           )
    }

    AddRolepermissionOK = () =>{
        const {  form:{ validateFieldsAndScroll }, dispatch } = this.props;
        validateFieldsAndScroll((err, values) => {
            dispatch({
                type: 'powers/RolepermissionAdd',
                payload: {
                    name: values.RolepermissionType,
                    code: values.RolepermissionCode
                }
            });
            this.setState({
                visible: false,
            })
        })
    }

    render() {
       const { selectedRows } = this.state;
       const {  powers:{ AdminTypes }, loading } = this.props;
   
       return (
            <Card title="账号权限管理" bordered={false}>
                <div className={styles.tableList}>
                    <div className={styles.tableListForm}>{this.renderForm()}</div>
                    <div className={styles.tableListOperator}>
                        <StandardTable
                        pagination={{ pageSize: 50 }} 
                        scroll={{ y: 450 }}
                        selectedRows={selectedRows}
                        data={AdminTypes}
                        loading={loading}
                        columns={this.columns}
                        pagination={{
                            size: "small",
                            pageSize: 40,
                            showSizeChanger: true,
                            showQuickJumper: true,
                            showTotal: function (total, range) {  //设置显示一共几条数据
                                return  '第 ' + range[0] + ' - ' + range[1] + ' 条'+ '  ' + `（共 ${total} 条）`; 
                            }
                        }}
                        />
                    </div>
                </div>
            </Card>
       );
     }
}

export default Rolepermission;