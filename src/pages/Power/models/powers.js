import * as services  from '@/services/hospitalType';

export default {
    namespace: 'powers',
  
    state: {
      AdminTypes: [],
      PcWebEvaList: [],
      ServiesItemList: []
    },
  
    effects: {
        *fetchAdminType({ payload:{ url, params } }, { call, put}){
            const res = yield call(services.fetchAdminType, url, params);
            yield put({ type: 'save', payload: { AdminTypes: res } });
        },

        *fetchPcWebEvaList({ payload: { url, params }}, { call, put }){
            const res = yield call(services.PcWebEvaList, url, params);
            yield put({ type: 'save', payload: { PcWebEvaList: res } });
        },

        *ServiesItem({ payload:{ url, params }}, { call, put }){
            const res = yield call(services.ServiesItem, url, params);
            yield put({ type: 'save', payload: { ServiesItemList: res } });
        },

        *ServiesItemAdd({ payload:{ eva, name }}, { call, put }){
            yield call(services.ServiesItemAdd, eva, name);
            let url = "admin/serviesItem/query";
            let params=  "_dc=1545986657871&page=1&start=0&limit=1000";
            let res = yield call(services.ServiesItem, url, params);
            yield put({ type: 'save', payload: { ServiesItemList: res } });
        },

        *RolepermissionAdd({ payload:{ code, name }}, { call, put }){
            yield call(services.RolepermissionAdd, code, name);
            let url = "admin/accounttype/query";
            let params = "_dc=1545893438947&query=&page=1&start=0&limit=1000";
            let res = yield call(services.fetchAdminType, url, params);
            yield put({ type: 'save', payload: { AdminTypes: res } });
        }
    },
  
    reducers: {
      save(state, { payload }) {
        return {
          ...state,
          ...payload,
        };
      }
    },
  };
  