import React, { Component } from 'react';
import { connect } from 'dva';
import { Card, Row, Col, Icon, Modal, Button, Form, Input, InputNumber} from 'antd';
import StandardTable from '@/components/StandardTable';
import styles from './common.less';
import { CommonInterface  } from '@/services/params';

const FormItem = Form.Item;
const { ServiesParams, formItemLayout } = CommonInterface;

@connect(({ powers, loading }) => ({
    powers,
    loading: loading.effects['powers/ServiesItem']
}))@Form.create()

class ServiesItemManAge extends Component {

    componentDidMount() {
        const { dispatch } = this.props;
        dispatch({
            type: 'powers/ServiesItem',
            payload: ServiesParams
        });
    }

    state ={
        selectedRows: [],
        visible: false
    }

    columns =[
        {
            title: '服务编号',
            dataIndex: 'Id',
            width: '20%',
            align: 'center'
        },
        {
            title: '服务名称',
            dataIndex: 'Name',
            width: '20%',
            align: 'center'
        },
        {
            title: '服务代号',
            dataIndex: 'Eva',
            width: '20%',
            align: 'center'
        },
    ]

    renderForm(){
        return(
          <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
              <Col md={4} sm={24}>
                  <Button onClick={() =>{ this.setState({ visible: true })}}>新增</Button>
              </Col>
              <Modal
                title="新增服务"
                visible={this.state.visible} 
                onOk={this.AddServiesItemOK}
                onCancel={() =>  
                this.setState({
                    visible: false,
                })}
                >
                {this.AddServiesItem()}
             </Modal>
          </Row>
       )
    }

    AddServiesItem(){
        const { 
            form:{ getFieldDecorator }
          } = this.props;
         return(
          <Form>
             <FormItem
              {...formItemLayout}
              label="服务名称"
            >
               {getFieldDecorator('ServiesItemName',{
                   rules: [{
                    required: true, message: '请填写服务名称!',
                  }],
               })(
              <Input />
            )}
            </FormItem>
            <FormItem
              {...formItemLayout}
              label="eva"
            >
               {getFieldDecorator('ServiesItemEva',{
                   rules: [{
                    required: true, message: '请填写eva!',
                  }],
               })(
              <InputNumber />
            )}
            </FormItem>
          </Form>
         )
    }

    AddServiesItemOK = () =>{
        const {  form:{ validateFieldsAndScroll }, dispatch } = this.props;
        validateFieldsAndScroll((err, values) => {
            dispatch({
                type: 'powers/ServiesItemAdd',
                payload: {
                    name: values.ServiesItemName,
                    eva: values.ServiesItemEva
                }
            });
            this.setState({
                visible: false,
            })
        })
    }
    
    render() {
      const { selectedRows } = this.state;
      const { loading, powers:{ ServiesItemList } } = this.props;
      return (
            <Card title="服务项目管理" bordered={false}>
               <div className={styles.tableListForm}>{this.renderForm()}</div>
                <div className={styles.tableList}>
                    <div className={styles.tableListOperator}>
                        <StandardTable
                        pagination={{ pageSize: 50 }} 
                        scroll={{ y: 450 }}
                        selectedRows={selectedRows}
                        data={ServiesItemList}
                        loading={loading}
                        columns={this.columns}
                        pagination={{
                            size: "small",
                            pageSize: 40,
                            showSizeChanger: true,
                            showQuickJumper: true,
                            showTotal: function (total, range) {  //设置显示一共几条数据
                                return  '第 ' + range[0] + ' - ' + range[1] + ' 条'+ '  ' + `（共 ${total} 条）`; 
                            }
                        }}
                        />
                    </div>
                </div>
            </Card>
      );
    }
}

export default ServiesItemManAge;