import * as services from '@/services/doctors';

export default {
  namespace: 'doctors',

  state: {
    DoctorList: [],
    selected: ""
  },

  effects: {
    *fetchDoctor({ payload: { url, params }},{ call, put} ){
      yield put({ type: 'save', payload: { DoctorList: [] } })
      let hid = params.split("&")[1].split("=")[1];
      const res = yield call(services.fetchDoctorList, url, params);
      yield put({ type: 'save', payload: { DoctorList: res,  selected: hid} });
    },
   
    *fetchDoctorAdd({payload:{Comment, Department, DepartmentUid, Email, Expert, Gender, HospitalId, IsSSO, Jurisdiction, Mobile, Name, Title}}, { call, put}){
      yield call(services.fetchDoctorAdd, Comment, Department, DepartmentUid, Email, Expert, Gender, HospitalId, IsSSO, Jurisdiction, Mobile, Name, Title);
      let url =  "admin/doctor/query";
      let params=  `_dc=1545370068287&hid=${HospitalId}&page=1&start=0&limit=25`;
      const res = yield call(services.fetchDoctorList, url, params);
      yield put({ type: 'save', payload: { DoctorList: res } });
    },

    *fetchDoctorUpdate({payload:{Comment, Department, DepartmentUid, Email, Expert, Gender, HospitalId, IsSSO, Jurisdiction, Mobile, Name, Title, Id}}, { call, put}){
      yield call(services.fetchDoctorUpdate, Comment, Department, DepartmentUid, Email, Expert, Gender, HospitalId, IsSSO, Jurisdiction, Mobile, Name, Title, Id);
      let url =  "admin/doctor/query";
      let params=  `_dc=1545370068287&hid=${HospitalId}&page=1&start=0&limit=25`;
      const res = yield call(services.fetchDoctorList, url, params);
      yield put({ type: 'save', payload: { DoctorList: res } });
    },

    *fetchDoctorDelete({ payload:{ Id}}, { call, put, select}){
      yield call(services.fetchDoctorDelete, Id);
      const data = yield select(state=>state.doctors);
      let url =  "admin/doctor/query";
      let params=  `_dc=1545370068287&hid=${data.selected}&page=1&start=0&limit=25`;
      const res = yield call(services.fetchDoctorList, url, params);
      yield put({ type: 'save', payload: { DoctorList: res } });
    },

   *fetchDoctorPswd({ payload: { Id, Pswd}}, { call,put }){
      yield call(services.fetchDoctorPswd, Id, Pswd);
   }
  },

  reducers: {
    save(state, { payload }) {
      return {
        ...state,
        ...payload,
      };
    }
  },
};
