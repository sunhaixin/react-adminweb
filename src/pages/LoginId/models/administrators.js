import * as services from '@/services/administrators';

export default {
  namespace: 'administrators',

  state: {
    loading: false,
    adminList: {},
    adminType: [],
    selected: ""
  },

  effects: {
    *fetchAdminQuery({ payload: { url, params}}, { call, put }){
      let hid = params.split("&")[1].split("=")[1];
      const res = yield call(services.fetchAdminQuery, url, params);
      yield put({ type: 'save', payload: { adminList: res, selected: hid} });
    },

    *fetchAdminType({ payload: { url, params}}, { call, put }){
      const res = yield call(services.fetchAdminType, url, params);
      yield put({ type: 'save', payload: { adminType: res,} });
    },
    
    *fetchAdminAdd({ payload: { AccountType, AccountTypeCode, HospitalId, LoginId, Memo, Pswd, SysName, UserName }}, { call, put }){
      yield call(services.fetchAdminAdd, AccountType, AccountTypeCode, HospitalId, LoginId, Memo, Pswd, SysName, UserName);
      if(HospitalId){
        let url= "admin/account/query";
        let params = `_dc=1545811706273&hid=${HospitalId}&page=1&start=0&limit=1000` ;
        const res = yield call(services.fetchAdminQuery, url, params);
        yield put({ type: 'save', payload: { adminList: res } })
       }
    },

    *fetchAdminUpdate({ payload:{ AccountType, AccountTypeCode, HospitalId, Id, LoginId, Memo, SysName, UserName }}, { call, put }){
      yield call(services.fetchAdminUpdate, AccountType, AccountTypeCode, HospitalId, Id, LoginId, Memo, SysName, UserName);
      if(HospitalId){
        let url= "admin/account/query";
        let params = `_dc=1545811706273&hid=${HospitalId}&page=1&start=0&limit=1000` ;
        const res = yield call(services.fetchAdminQuery, url, params);
        yield put({ type: 'save', payload: { adminList: res } })
       }
    },

    *fetchAdminDel({ payload:{ Id }}, { call, put, select}){
      yield call(services.fetchAdminDel, Id);
      const data = yield select(state=>state.administrators);
      let url= "admin/account/query";
      let params = `_dc=1545811706273&hid=${data.selected}&page=1&start=0&limit=1000` ;
      const res = yield call(services.fetchAdminQuery, url, params);
      yield put({ type: 'save', payload: { adminList: res } })
    },

    *fetchAdminPswd({ payload: { Id, Pswd }}, { call, put }){
      yield call(services.fetchAdminPswd, Id, Pswd);
    }
  },

  reducers: {
    save(state, { payload }) {
      return {
        ...state,
        ...payload,
      };
    }
  },
};
