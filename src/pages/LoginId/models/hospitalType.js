import * as services  from '@/services/hospitalType';

export default {
    namespace: 'hospitalType',
  
    state: {
      Typelist: []
    },
  
    effects: {
      *fetchTypeList({ payload:{ url, params } }, { call, put }){
        const res = yield call(services.typeList, url, params);
        yield put({ type: 'save', payload: { Typelist: res } });
      },//

      *fetchAddType({ payload: { Name }}, { call, put}){
        yield call(services.typeAdd, Name);
        let url = "admin/hospitaltype/query";
        let params = "_dc=1545036768678&page=1&start=0&limit=25";
        let res =yield call(services.typeList, url, params);
        yield put({ type: 'save', payload: { Typelist: res } });
      },//

      *fetchUpdateType({ payload: { Id, Name}}, { call, put }){
        yield call(services.typeUpdate, Id, Name);
        let url = "admin/hospitaltype/query";
        let params = "_dc=1545036768678&page=1&start=0&limit=25";
        let res =yield call(services.typeList, url, params);
        yield put({ type: 'save', payload: { Typelist: res } });
      },//
    },
  
    reducers: {
      save(state, { payload }) {
        return {
          ...state,
          ...payload,
        };
      }
    },
  };
  