import * as services  from '@/services/api';
import { CommonInterface }  from '@/services/params';
const { ChartData } = CommonInterface;

export default {
  namespace: 'monitor',

  state: {
    tagsList: [],
    loading: false,
  },

  effects: {
    *fetchTags({ payload:{ url, params } }, { call, put }){
      const res = yield call(services.TagsConstants, url, params);
      yield put({ type: 'save', payload: { tagsList: res } })
    },
    
   
    *fetchSearch({payload}, { call, put }){
      const { url, params  } = ChartData
      const res = yield call(services.TagsConstants, url, params)
      var tempList = [];
      if(payload){
        res.forEach((e) =>{
            if(e.title.indexOf(payload) != -1){
              tempList.push(e);
            }
        })
        yield put({ type: 'save', payload: { tagsList: tempList } })
      }else{
        yield put({ type: 'save', payload: { tagsList: res } })
      }
    },
  },

  reducers: {
    save(state, { payload }) {
      return {
        ...state,
        ...payload,
      };
    }
  },
};
