import * as services  from '@/services/department';

export default {
  namespace: 'department',

  state: {
    departmentList: {},
    loading: false,
    selected: ""
  },

  effects: {
    *fetchTreeNode({ payload:{ url, params } }, { call, put }){
        let hid = params.split("&")[1].split("=")[1];
        const res = yield call(services.TagsTreeNode, url, params);
        yield put({ type: 'save', payload: { departmentList: res, selected: hid} })
    },
      
    *fetchAddkeSi({payload: { HospitalId, Memo, Name} }, { call, put }){
        yield call(services.TagsAddkeShi, HospitalId, Memo, Name);
        if(HospitalId){
         let url= "admin/department/query";
         let params = `_dc=1545114961248&hid=${HospitalId}&page=1&start=0&limit=250` ;
         const res = yield call(services.TagsTreeNode, url, params);
         yield put({ type: 'save', payload: { departmentList: res } })
        }
     },
 
    *fetchUpdatekeSi({ payload: {HospitalId, Memo, Name, Uid }}, { call, put }){
       yield call(services.TagsUpdatekeSi, HospitalId, Memo, Name, Uid);
       if(HospitalId){
         let url= "admin/department/query";
         let params = `_dc=1545114961248&hid=${HospitalId}&page=1&start=0&limit=250` ;
         const res = yield call(services.TagsTreeNode, url, params);
         yield put({ type: 'save', payload: { departmentList: res } })
        }
    }
  },

  reducers: {
    save(state, { payload }) {
      return {
        ...state,
        ...payload,
      };
    }
  },
};
