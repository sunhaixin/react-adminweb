import * as services  from '@/services/hospital';

export default {
  namespace: 'hospital',

  state: {
    HGLData: [],
    HGLType: [],
    HGLProvince: [],
    HGLCity: [],
    HGLDistrict: [],
    loading: false
  },

  effects: {
    *fetch({ payload }, { call, put }) {
      const res = yield call(services.fakeChartData, payload);
      yield put({ type: 'save', payload: { HGLData: res } });
    },

    *fetchChange({ payload }, { call, put }) {
      const res = yield call(services.fakeChangeData, payload);
      yield put({ type: 'save', payload: { HGLData: res } });
    },

    *fetchConstants({ payload:{ url, params } }, { call, put }){
      const res = yield call(services.fetchConstants, url, params);
      if(url == "admin/hospitaltype/query"){
        yield put({ type: 'save', payload: { HGLType: res } });
      }else if(url == "admin/basic/province"){
        yield put({ type: 'save', payload: { HGLProvince: res } });
      }else if(url == "admin/basic/city"){
        yield put({ type: 'save', payload: { HGLCity: res } });
      }else if(url == "admin/basic/district"){
        yield put({ type: 'save', payload: { HGLDistrict: res } });
      }
    },
  },

  reducers: {
    save(state, { payload }) {
      return {
        ...state,
        ...payload,
      };
    }
  },
};
