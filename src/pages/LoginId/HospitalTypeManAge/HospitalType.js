import React, { Component } from 'react';
import { connect } from 'dva';
import { Card, Form, Row, Col, Button, Input, Icon, Modal, InputNumber} from 'antd';
import StandardTable from '@/components/StandardTable';
import { CommonInterface, CompareDate  } from '@/services/params';

import styles from './HospitalType.less';

const FormItem = Form.Item;
const { typeData, iconstyle, formItemLayout } = CommonInterface;

@connect(({ hospitalType, loading }) => ({
  hospitalType,
  loading: loading.effects['hospitalType/fetchTypeList'],
}))@Form.create()

class HospitalType extends Component {

    componentDidMount() {
      const { dispatch } = this.props;
      dispatch({
        type: 'hospitalType/fetchTypeList',
        payload: typeData
      });
    }

    state ={
        selectedRows: [],
        visible: false,
        visibles: false,
        typeNames: "",
        typeUid: ""
    }

    columns =[
      {
        title: '类型名称',
        dataIndex: 'typeName',
        width: '70%',
        align: 'center'
      },
      {
        title: '创建时间',
        dataIndex: 'CreateTime',
        sorter: true,
        width: '15%',
        sorter: (a, b) => CompareDate(a.CreateTime,b.CreateTime),
      },
      {
        title: '操作',
        width: '15%',
        align: 'center',
        dataIndex: 'Operation',
        render: (text, record) =>(
          <span>
            <Icon type="form" onClick={()=> this.UpdateType(record.typeName,record.key)}
            style={iconstyle}/> 
        </span>
        ),
      },
    ]

    renderForm(){
      return(
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col md={4} sm={24}>
              <Button onClick={this.showModal}>新增</Button>
              <Modal
                title="新增医院类型"
                visible={this.state.visible}
                onOk={this.handleTypeOk}
                onCancel={() =>  
                this.setState({
                  visible: false,
                })}
              >
              { this.AddHospitalType()}
              </Modal>
          </Col>
        </Row>
     )
    }

    AddHospitalType= () =>{
        const { 
          form:{ getFieldDecorator }
        } = this.props;
       return(
        <Form>
           <FormItem
            {...formItemLayout}
            label="类型名称"
          >
             {getFieldDecorator('hospitalTypeName')(
            <Input />
          )}
          </FormItem>
        </Form>
       )
    }

    UpdateHospitalType = () =>{
      const { 
        form:{ getFieldDecorator }
      } = this.props;
      const { typeNames } =  this.state;
     return(
      <Form>
         <FormItem
          {...formItemLayout}
          label="类型名称"
        >
           {getFieldDecorator('TypeName',  {
            initialValue: typeNames
        })(
          <Input />
        )}
        </FormItem>
      </Form>
     )
    }

    handleTypeOk =() =>{
      const {  form:{ validateFieldsAndScroll }, dispatch } = this.props;
      validateFieldsAndScroll((err, values) => {
          dispatch({
            type: 'hospitalType/fetchAddType',
            payload: {
              Name: values.hospitalTypeName
            }
          });
          this.setState({
            visible: false,
          })
      })
    }

    UpdateTypeOk = () =>{
      const {  form:{ validateFieldsAndScroll }, dispatch } = this.props;
      const { typeUid } = this.state;
      validateFieldsAndScroll((err, values) => {
          dispatch({
            type: 'hospitalType/fetchUpdateType',
            payload: {
              Id: typeUid,
              Name: values.TypeName
            }
          });
          this.setState({
            visibles: false,
          })
      })
    }

    UpdateType = (Name,key) =>{
      //  console.log(Name,key)
      const { dispatch } =this.props;
      this.setState({
        visibles: true,
        typeNames: Name,
        typeUid: key
      });
    }

    showModal = () => {
      const { dispatch } =this.props;
      this.setState({
        visible: true,
      });
    }

    render() {
       const { selectedRows, visibles } = this.state;
       const {  hospitalType:{ Typelist }, loading } = this.props;
   
       return (
          <Card title="医院类型信息" bordered={false}>
               <div className={styles.tableList}>
                 <div className={styles.tableListForm}>{this.renderForm()}</div>
                 <div className={styles.tableListOperator}>
                     <StandardTable
                       pagination={{ pageSize: 50 }} 
                       scroll={{ y: 450 }}
                       selectedRows={selectedRows}
                       loading={loading}
                       data={Typelist}
                       columns={this.columns}
                       pagination={{
                         size: "small",
                         pageSize: 40,
                         showSizeChanger: true,
                         showQuickJumper: true,
                         showTotal: function (total, range) {  //设置显示一共几条数据
                             return  '第 ' + range[0] + ' - ' + range[1] + ' 条'+ '  ' + `（共 ${total} 条）`; 
                         }
                       }}
                     />
                 </div>
                 <Modal
                    title="修改医院类型"
                    visible={visibles}
                    onOk={this.UpdateTypeOk}
                    onCancel={() =>  
                    this.setState({
                      visibles: false,
                    })}
                  >
                  { this.UpdateHospitalType()}
                  </Modal>
               </div>
          </Card>
       );
     }
}

export default HospitalType;