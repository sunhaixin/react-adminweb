import React, { Component } from 'react';
import { connect } from 'dva';
import styles from './Department.less';
import { Card, Row, Col, Input, Form, Button, Modal, Icon, Select} from 'antd';
import StandardTable from '@/components/StandardTable';
import GridContent from '@/components/PageHeaderWrapper/GridContent';
import { CommonInterface, CompareDate } from '@/services/params';
import  ShareManAge from '../ShareManAge';

const { iconstyle, formItemLayout } = CommonInterface;
const FormItem = Form.Item;
const Option = Select.Option;

@connect(({ monitor, department, loading }) => ({
  monitor,
  department,
  loadings: loading.models.department,
}))@Form.create()

export default class Department extends Component {

  state ={
    selectedRows: [],
    departUid: "",
    departName: "",
    departMemo: "",
    visible: false,
    visibles: false
  }

  columns =[
    {
      title: '医院名称',
      dataIndex: 'HospitalName',
      width: '15%',
      align: 'center'
    },
    {
      title: '科室名称',
      dataIndex: 'Name',
      editable: true,
      width: '15%',
      align: 'center'
    },
    {
      title: '备注',
      dataIndex: 'Memo',
      editable: true,
      width: '15%',
      align: 'center'
    },
    {
      title: '创建时间',
      dataIndex: 'CreateTime',
      sorter: true,
      sorter: (a, b) => CompareDate(a.CreateTime,b.CreateTime),
    },
    {
      title: '更新时间',
      dataIndex: 'UpdateTime',
      sorter: true,
      sorter: (a, b) => CompareDate(a.CreateTime,b.CreateTime),
    },
    {
      title: '操作',
      width: '15%',
      dataIndex: 'Operation',
      render: (text, record) =>(
        <span>
          <Icon type="form" onClick={()=> {
            this.setState({ 
              visible: true, 
              departUid: record.key, 
              departName: record.Name,
              departMemo:  record.Memo
            });
          }}
          style={iconstyle}/> 
      </span>
      ),
    }
  ]

  UpdateDepartOK(){
     const { departUid, departName, departMemo  } = this.state;
     const { dispatch,  department: {selected} } = this.props;
    dispatch({
      type: 'department/fetchUpdatekeSi',
      payload: {
        HospitalId: selected,
        Memo: departMemo, 
        Name: departName,
        Uid: departUid
      }
    });
    this.setState({
      visible: false,
      departName: "", 
      departMemo: ""
    })
  }

  UpdateDepartment(){
    const { 
      form:{ getFieldDecorator }
    } = this.props;
    const { departName, departMemo } =this.state;
    return (
      <Form>
        <FormItem
            {...formItemLayout}
            label="*科室名"
          >
            <Input value={departName} onChange={(e) =>{ this.setState({departName: e.target.value})}}/>
          </FormItem>
          <FormItem
            {...formItemLayout}
            label="备注"
          >
            <Input value={departMemo} onChange={(e) =>{ this.setState({departMemo: e.target.value})}}/>
          </FormItem>
      </Form>
    )
  }

  rendersForm(){
     return(
      <Row gutter={{ md: 8, lg: 24 }}>
         <Col sm={24}>
            <Button onClick={() =>  
            this.setState({
             visibles: true,
          })}>新增</Button>
            <Modal
              title="新增科室"
              visible={this.state.visibles}
              onOk={this.addKeShiOK}
              onCancel={() =>  
              this.setState({
                visibles: false,
              })}
            >
            { this.AddDepartment()}
            </Modal>
        </Col>
      </Row>
    )
  }

  addKeShiOK= () =>{
    const {  form:{ validateFieldsAndScroll }, dispatch } = this.props;
    validateFieldsAndScroll((err, values) => {
        dispatch({
          type: 'department/fetchAddkeSi',
          payload: {
            HospitalId: values.hospital,
            Memo: values.comments, 
            Name: values.keshiming
          }
        });
        this.setState({
          visibles: false,
        })
    })
  }

  AddDepartment(){
    const { 
      form:{ getFieldDecorator, },
      monitor:{ tagsList},
      department: {selected}
    } = this.props;
  
    return (
      <Form>
         <FormItem
            {...formItemLayout}
            label="医院"
          >
             {getFieldDecorator('hospital', {
            initialValue: selected,
            rules: [{ required: true, message: 'Please input your hospital!', whitespace: true }],
          })(
            <Select>
                {tagsList? tagsList.map((item) =>{
                    return(
                      <Option key={item.key}>{item.title}</Option>
                    )
                }) : ''}
            </Select>
          )}
          </FormItem>
          <FormItem
            {...formItemLayout}
            label="科室名"
          >
             {getFieldDecorator('keshiming')(
            <Input />
          )}
          </FormItem>
          <FormItem
            {...formItemLayout}
            label="备注"
          >
             {getFieldDecorator('comments')(
            <Input />
          )}
          </FormItem>
      </Form>
    )
  }
  
  render() {
    const { department:{ departmentList }, loadings } = this.props;
    const { selectedRows, visible  } = this.state;
   
    return (
        <div style={{ flex: 1}}>
            <Row gutter={24}>
                <ShareManAge types="DepartmentManAge"/>
                <Col xl={18} lg={24} md={24} sm={24} xs={24}>
                  <Card title="科室管理" bordered={false} style={{  height: "742px"}}>
                      <div className={styles.tableListForm}>
                          {this.rendersForm()}
                      </div>
                      <Row>
                          <div className={styles.tableListOperator}>
                              <StandardTable
                                  pagination={{ pageSize: 50 }} 
                                  scroll={{ y: 450 }}
                                  data={departmentList}
                                  selectedRows={selectedRows}
                                  loading={loadings}
                                  columns={this.columns}
                                  pagination={{
                                    size: "small",
                                    pageSize: 40,
                                    showSizeChanger: true,
                                    showQuickJumper: true,
                                    showTotal: function (total, range) {  //设置显示一共几条数据
                                        return  '第 ' + range[0] + ' - ' + range[1] + ' 条'+ '  ' + `（共 ${total} 条）`; 
                                    }
                                  }}
                                />
                          </div>
                          <Modal
                            title="修改科室信息"
                            visible={visible}
                            onOk={() => this.UpdateDepartOK()}
                            onCancel={() =>{
                              this.setState({
                                visible: false
                              })
                            }
                              }
                          >
                          { this.UpdateDepartment()}
                          </Modal>
                      </Row> 
                  </Card>
                </Col>
             </Row>
        </div>
    );
  }
}
