import React, { Component } from 'react';
import { connect } from 'dva';
import styles from './Administrators.less';
import { Card, Row, Col, Input, Form, Button, Modal, Icon, Select, Popconfirm} from 'antd';
import StandardTable from '@/components/StandardTable';
import GridContent from '@/components/PageHeaderWrapper/GridContent';
import { CommonInterface } from '@/services/params';
import  ShareManAge from '../ShareManAge';

const { iconstyle, formItemLayout } = CommonInterface;
const FormItem = Form.Item;
const Option = Select.Option;

@connect(({ monitor, administrators, loading }) => ({
    monitor,
    administrators,
    loading: loading.models.administrators,
}))@Form.create()

export default class Administrators extends Component {

    state ={
        visibles:false,
        visible: false,
        visibleX: false,
        selectedRows: [],
        randomPswd: '',
        AdminTypeCode: '',
        PswdId:'',
        record: {}
    }

    columns = [
          {
            title: '',
            dataIndex: 'key',
            width: '5%',
            align: 'center'
          },
          {
            title: '医院名称',
            dataIndex: 'HospitalName',
            width: '15%',
            align: 'center'
          },
          {
            title: '登录账号',
            dataIndex: 'LoginId',
            editable: true,
            width: '15%',
            align: 'center'
          },
          {
            title: '账号类型',
            dataIndex: 'AccountType',
            editable: true,
            width: '15%',
            align: 'center'
          },
          {
            title: '用户名',
            width: '15%',
            dataIndex: 'UserName',
          },
          {
            title: '操作',
            width: '15%',
            dataIndex: 'Operation',
            render: (text, record) =>(
              <span>
                 <Icon type="form" onClick={() => {
                    this.setState({
                      visible: true,
                      record
                    })
                   this.typeAdmin()
                 }} style={iconstyle}/>
                 <Icon type="setting" onClick={() =>{
                   this.setState({
                     visibleX: true,
                     PswdId: record.Id
                   })
                 }} style={iconstyle}/>
                 <Popconfirm title="确定要删除吗？"
                 onConfirm={() => this.deleteAdmin(record.Id)} 
                 onCancel={(e) => console.log(e)} okText="确定" cancelText="取消">
                  <Icon type="delete" style={iconstyle}/>
                </Popconfirm>
            </span>
            ),
          }
    ]

    rendersForm(){
        return(
         <Row gutter={{ md: 8, lg: 24 }}>
            <Col sm={24}>
               <Button onClick={() =>  
                this.setState({
                visibles: true,
               })}>新增</Button>
               <Modal
                 title="新增账号管理员"
                 visible={this.state.visibles}
                 onOk={() => this.addAdminOK()}
                 onCancel={() =>  
                 this.setState({
                   visibles: false,
                 })}
               >
                {this.addAdmin()}
               </Modal>
           </Col>
         </Row>
       )
    }
    random(){
      var values=""; 
      for(var i=0;i<6;i++) { 
          values+=Math.floor(Math.random()*10); 
      }
      this.setState({
        randomPswd: values
      })
    }

    typeAdmin(){
        this.props.dispatch({
        type: 'administrators/fetchAdminType',
        payload:{
          url: "admin/accounttype/query",
          params: `_dc=1545893438947&query=&page=1&start=0&limit=1000`,
        }
      })
    }

    addAdmin(){
      const { 
        form:{ getFieldDecorator, },
        monitor:{ tagsList},
        administrators: {selected, adminType},
        dispatch
      } = this.props;

      return (
        <Form>
          <FormItem
              {...formItemLayout}
              label="*账号类型"
            >
               {getFieldDecorator('AccountType')(
              <Select 
              onChange={(e) =>{
                adminType? adminType.forEach((c) =>{
                  if(e == c.Id){
                    this.setState({
                      AdminTypeCode: c.Code
                    })
                  }
                }):''
            }}
              onFocus={() => this.typeAdmin()}
              >
                  {adminType? adminType.map((item) =>{
                      return(
                        <Option key={item.Id}>{item.Name}</Option>
                      )
                  }) : ''}
              </Select>
            )}
            </FormItem>
           <FormItem
              {...formItemLayout}
              label="*医院"
            >
               {getFieldDecorator('HospitalId', {
              initialValue: selected,
            })(
              <Select>
                  {tagsList? tagsList.map((item) =>{
                      return(
                        <Option key={item.key}>{item.title}</Option>
                      )
                  }) : ''}
              </Select>
            )}
            </FormItem>
              <FormItem
                {...formItemLayout}
                label="*登录账号"
              >
                {getFieldDecorator('LoginId')(
                <Input />
              )}
              </FormItem>
            <FormItem
              {...formItemLayout}
              label="*密码"
            >
               <Row gutter={8}>
                  <Col span={15}>
                    {getFieldDecorator('Pswd', {
                      initialValue: this.state.randomPswd
                    })(
                      <Input />
                    )}
                  </Col>
                  <Col span={8}>
                    <Button onClick={() => this.random()}>随机密码</Button>
                  </Col>
                </Row>
            </FormItem>
            <FormItem
              {...formItemLayout}
              label="*用户名"
            >
               {getFieldDecorator('UserName')(
              <Input />
            )}
            </FormItem>
            <FormItem
              {...formItemLayout}
              label="*平台名称"
            >
               {getFieldDecorator('SysName')(
              <Input />
            )}
            </FormItem>
            <FormItem
              {...formItemLayout}
              label="备注"
            >
               {getFieldDecorator('Memo')(
              <Input />
            )}
            </FormItem>
        </Form>
      )
    }

    addAdminOK(){
      const {  form:{ validateFieldsAndScroll }, dispatch } = this.props;
      validateFieldsAndScroll((err, values) => {
         const { AccountType, LoginId, HospitalId, Pswd, UserName} = values;
          dispatch({
            type: 'administrators/fetchAdminAdd',
            payload: {
              AccountType: AccountType , 
              AccountTypeCode: this.state.AdminTypeCode, 
              HospitalId:HospitalId, 
              LoginId: LoginId, 
              Memo: values.Memo, 
              Pswd: Pswd, 
              SysName: values.SysName, 
              UserName: UserName
            }
          });
          this.setState({
            visibles: false,
          })
      })
    }

    updateAdmin(){
      const { 
        form:{ getFieldDecorator, },
        monitor:{ tagsList},
        administrators: { adminType, selected},
        dispatch
      } = this.props;
      const { record } = this.state;
      return(
        <Form>
            <FormItem
              {...formItemLayout}
              label="*账号类型"
            >
               {getFieldDecorator('AccountTypes',{
                    initialValue: record.AccountId,
               })(
              <Select 
              onChange={(e) =>{
                adminType? adminType.forEach((c) =>{
                  if(e == c.Id){
                    this.setState({
                      AdminTypeCode: c.Code
                    })
                  }
                }):''
            }}
              >
                  {adminType? adminType.map((item) =>{
                      return(
                        <Option key={item.Id}>{item.Name}</Option>
                      )
                  }) : ''}
              </Select>
            )}
            </FormItem>
            <FormItem
              {...formItemLayout}
              label="*医院"
            >
               {getFieldDecorator('HospitalIds',{
                    initialValue: selected,
               })(
              <Select>
                 {tagsList? tagsList.map((item) =>{
                      return(
                        <Option key={item.key}>{item.title}</Option>
                      )
                  }) : ''}
              </Select>
            )}
            </FormItem>
            <FormItem
                {...formItemLayout}
                label="*登录账号"
              >
                {getFieldDecorator('LoginIds',{
                     initialValue: record.LoginId,
                })(
                <Input />
              )}
              </FormItem>
            <FormItem
              {...formItemLayout}
              label="*用户名"
            >
               {getFieldDecorator('UserNames',{
                   initialValue: record.UserName,
               })(
              <Input />
            )}
            </FormItem>
            <FormItem
              {...formItemLayout}
              label="平台名称"
            >
               {getFieldDecorator('SysNames',{
                    initialValue: record.SysName,
               })(
              <Input />
            )}
            </FormItem>
            <FormItem
              {...formItemLayout}
              label="备注"
            >
               {getFieldDecorator('Memos',{
                    initialValue: record.Memo,
               })(
              <Input />
            )}
            </FormItem>
        </Form>
      )
    }
    updateAdminOK(){
      const {  form:{ validateFieldsAndScroll }, dispatch } = this.props;
      validateFieldsAndScroll((err, values) => {
        const {  AccountTypes, LoginIds, HospitalIds, UserNames, SysNames, Memos } = values;
        dispatch({
          type: 'administrators/fetchAdminUpdate',
          payload: {
            AccountType: AccountTypes,
            AccountTypeCode: this.state.AdminTypeCode,
            HospitalId: HospitalIds,
            Id: this.state.record.Id,
            LoginId: LoginIds,
            Memo: Memos,
            SysName: SysNames,
            UserName: UserNames
          }
        });
        this.setState({
          visible: false,
        })
      })
    }

    deleteAdmin(key){
      const { dispatch} = this.props;
      dispatch({
        type: 'administrators/fetchAdminDel',
        payload:{
          Id: key
        }
      })
    }

    updateAdminPswd(){
      const { 
        form:{ getFieldDecorator, }
      } = this.props;
        return(
          <Form>
              <FormItem
              {...formItemLayout}
              label="密码"
            >
               {getFieldDecorator('password')(
              <Input />
            )}
            </FormItem>
          </Form>
        )
    }

    updateAdminPswdOK(){
      const {  form:{ validateFieldsAndScroll }, dispatch } = this.props;
      validateFieldsAndScroll((err, values) => {
        const {  password } = values;
        dispatch({
          type: 'administrators/fetchAdminPswd',
          payload: {
             Id: this.state.PswdId,
             Pswd: password
          }
        });
        this.setState({
          visibleX: false,
        })
      })
    }

    render() {
        const { selectedRows, visible, visibleX } = this.state;
        const { administrators:{ adminList }, loading } = this.props;
 
        return(
            <div style={{ flex: 1}}>
                 <Row gutter={24}>
                    <ShareManAge types="Administrators"/>
                    <Col xl={18} lg={24} md={24} sm={24} xs={24}>
                        <Card title="系统账号管理" bordered={false} style={{  height: "742px"}}>
                            <div className={styles.tableListForm}>
                                {this.rendersForm()}
                            </div>
                            <Row>
                                <div className={styles.tableListOperator}>
                                    <StandardTable
                                     pagination={{ pageSize: 50 }} 
                                     scroll={{ y: 450 }}
                                     data={adminList}
                                     selectedRows={selectedRows}
                                     loading={loading}
                                     columns={this.columns}
                                     pagination={{
                                       size: "small",
                                       pageSize: 40,
                                       showSizeChanger: true,
                                       showQuickJumper: true,
                                       showTotal: function (total, range) {  //设置显示一共几条数据
                                           return  '第 ' + range[0] + ' - ' + range[1] + ' 条'+ '  ' + `（共 ${total} 条）`; 
                                       }
                                     }}
                                    />
                                </div>
                            </Row>
                        </Card>
                    </Col>
                    <Modal
                      title="修改管理员账号信息"
                      visible={visible}
                      onOk={() => this.updateAdminOK()}
                      onCancel={() =>{
                        this.setState({
                          visible: false
                        })
                      }
                        }
                    >
                    {this.updateAdmin()}
                    </Modal>
                    <Modal
                      title="设置密码"
                      visible={visibleX}
                      onOk={() => this.updateAdminPswdOK()}
                      onCancel={() =>{
                        this.setState({
                          visibleX: false
                        })
                      }
                        }
                    >
                    {this.updateAdminPswd()}
                    </Modal>
                 </Row>
            </div>
        )
    }
}


