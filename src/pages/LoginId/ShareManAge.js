import React, { Component } from 'react';
import { connect } from 'dva';
import { Card, Row, Col, Input, Form, Tree, Spin} from 'antd';
import StandardTable from '@/components/StandardTable';

import styles from './ShareManAge.less';

const FormItem = Form.Item;
const { TreeNode } = Tree;

@connect(({monitor, doctors, department, administrators, loading }) => ({
  monitor,
  doctors,
  department,
  administrators,
  loadings: loading.models.monitor,
}))

export default class ShareManAge extends Component {
    componentDidMount() {
      const { dispatch } = this.props;
      dispatch({
        type: 'monitor/fetchTags',
        payload:{
          url: "admin/hospital/query",
          params: "_dc=1545059769047&page=1&start=0&limit=1000"
        }
      });
    }

    changeSearch(e){
      const { dispatch } = this.props;
      dispatch({
        type: 'monitor/fetchSearch',
        payload: e.target.value
      });
    }

    renderForm(){
      return(
        <Row gutter={{ md: 8, lg: 24 }}>
          <Col sm={24}>
              <FormItem label="搜索">
                  <Input onChange={(e) => this.changeSearch(e)} placeholder="按医院名称搜索" /> 
              </FormItem>
          </Col>
        </Row>
      )
    }
    renderTreeNodes = data => data.map((item) => {
      return <TreeNode {...item} dataRef={item} />;
    })

    SelectTreeNode(e){
      const { dispatch, types } = this.props;
      if(types == "DoctorsManAge"){
        if(e[0]){
          dispatch({
            type: 'doctors/fetchDoctor',
            payload:{
              url: "admin/doctor/query",
              params: `_dc=1545370068287&hid=${e[0]}&page=1&start=0&limit=1000`,
            }
          });
        }else{
          dispatch({
            type: 'doctors/save',
            payload:{
              DoctorList:""
            }
          });
        }
      }else if(types=="DepartmentManAge"){
        if(e[0]){
          dispatch({
            type: 'department/fetchTreeNode',
            payload:{
              url: "admin/department/query",
              params: `_dc=1545114961248&hid=${e[0]}&page=1&start=0&limit=1000`,
            }
          });
        }else{
           dispatch({
             type: 'department/save',
             payload:{
              departmentList: ""
             }
           })
        }
      }else if(types=="Administrators"){
        if(e[0]){
          dispatch({
            type: 'administrators/fetchAdminQuery',
            payload:{
              url: "admin/account/query",
              params: `_dc=1545811706273&hid=${e[0]}&page=1&start=0&limit=1000`,
            }
          });
        }else{
           dispatch({
             type: 'administrators/save',
             payload:{
              adminList: ""
             }
           })
        }
      }
    }

    render() {

      const { monitor:{ tagsList }, loadings } = this.props;
      
      return (
          <Col xl={6} lg={24} md={24} sm={24} xs={24}>
              <Card title="医院名称" bordered={false}> 
                    <div className={styles.tableListForm}>
                        {this.renderForm()}
                    </div>
                    <Row>
                        <div className={styles.departBody}>
                            <Spin spinning={loadings} >
                              <Tree 
                              onSelect={(e) => this.SelectTreeNode(e)}
                              >
                              {this.renderTreeNodes(tagsList)}
                              </Tree>
                            </Spin>
                      </div>
                    </Row>
              </Card>
          </Col>
      );
    }
}
