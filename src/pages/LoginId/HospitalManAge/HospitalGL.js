import React, { Component } from 'react';
import { connect } from 'dva';
import { Card, Form, Row, Col, Button, Input, Icon, Modal, Select, InputNumber} from 'antd';
import StandardTable from '@/components/StandardTable';
import { CommonInterface, CompareDate } from '@/services/params';

import styles from './HospitalGL.less';

const FormItem = Form.Item;
const Option = Select.Option;
const { typeData, provinceData, formItemLayout} = CommonInterface;

@connect(({ hospital, loading }) => ({
  hospital,
  loading: loading.effects['hospital/fetch'],
}))@Form.create()

class HospitalGL extends Component {
  
  componentDidMount() {
    const { dispatch } = this.props;
    this.reqRef = requestAnimationFrame(()  =>{
      dispatch({
        type: 'hospital/fetch',
      });
    });
  }

  state ={
    selectedRows: [],
    formValues: {},
    visible: false
  }

  columns =[
    {
      title: '医院编号',
      sorter: true,
      dataIndex: 'key',
      width: '10%',
      sorter: (a, b) => a.key - b.key,
    },
    {
      title: '医院名称',
      dataIndex: 'hospitalName',
      width: '10%'
    },
    {
      title: '省份',
      dataIndex: 'Province',
      width: '10%'
    },
    {
      title: '城市',
      dataIndex: 'City',
      width: '10%'
    },
    {
      title: 'Token',
      dataIndex: 'Token',
      width: '25%'
    },
    {
      title: '模块',
      dataIndex: 'Modular',
      width: '25%'
    },
    // {
    //   title: '版本',
    //   dataIndex: 'Edition',
    //   width: '8%'
    // },
    {
      title: '创建时间',
      dataIndex: 'CreateTime',
      width: '15%',
      sorter: true,
      sorter: (a, b) => CompareDate(a.CreateTime,b.CreateTime),
    }
  ]
  
  showModal = () => {
    const { dispatch } =this.props;
    this.setState({
      visible: true,
    });
  }


  handleSearch = e =>{
     const { dispatch } =this.props;
     dispatch({
         type: 'hospital/fetchChange',
         payload: `dc=1545029917731&aid=1&name=${e.target.value}&page=1&start=0&limit=1000`
     })
  }

  SelectProvince = e =>{
    const { dispatch } =this.props;
      dispatch({
          type: 'hospital/fetchConstants',
          payload: {
             url: "admin/basic/city",
             params: `_dc=1545048540584&pid=${e}&page=1&start=0&limit=1000`
          }
      })
  }

  SelectDistrict = e =>{
    const { dispatch } =this.props;
      dispatch({
          type: 'hospital/fetchConstants',
          payload: {
             url: "admin/basic/district",
             params: `_dc=1545051514062&cid=${e}&page=1&start=0&limit=1000`
          }
      })
  }

  AddHospitalFrom = () =>{
      const { 
        form:{ getFieldDecorator }, 
        hospital:{ HGLType, HGLProvince, HGLCity, HGLDistrict }, dispatch 
      } = this.props;
      return(
        <Form>
            <FormItem
            {...formItemLayout}
            label="医院名称"
          >
             {getFieldDecorator('hospitalName', {
            rules: [{ required: true, message: 'Please input your hospitalName!', whitespace: true }],
          })(
            <Input />
          )}
          </FormItem>
          <FormItem
            {...formItemLayout}
            label="医院类别"
          >
             {getFieldDecorator('hospitalType', {
            rules: [{ required: true, message: 'Please input your hospitalType!', whitespace: true }],
          })(
            <Select onFocus={() => 
              dispatch({
                type: 'hospital/fetchConstants',
                payload: typeData
             })}>
                {HGLType? HGLType.map((item) =>{
                    return(
                      <Option key={item.Id}>{item.Name}</Option>
                    )
                }) : ''}
            </Select>
          )}
          </FormItem>
          <FormItem
            {...formItemLayout}
            label="省份"
          >
             {getFieldDecorator('province', {
            rules: [{ required: true, message: 'Please input your province!', whitespace: true }],
          })(
            <Select 
            onSelect={(e) => this.SelectProvince(e)}
            onFocus={() => 
              dispatch({
                type: 'hospital/fetchConstants',
                payload: provinceData
             })}>
               {HGLProvince? HGLProvince.map((item) =>{
                    return(
                      <Option key={item.Id}>{item.ProvinceName}</Option>
                    )
                }) : ''}
            </Select>
          )}
          </FormItem>
          <FormItem
            {...formItemLayout}
            label="城市"
          >
             {getFieldDecorator('city', {
            rules: [{ required: true, message: 'Please input your city!', whitespace: true }],
          })(
            HGLCity && HGLCity.length== 0? 
             <Select disabled />:  
             <Select
             onSelect={(e) => this.SelectDistrict(e)}
             >
              {HGLCity.map((item) =>{
                  return(
                    <Option key={item.Id}>{item.CityName}</Option>
                  )
              })}
           </Select>
           
          )}
          </FormItem>
          <FormItem
            {...formItemLayout}
            label="区县"
          >
             {getFieldDecorator('district')(
            HGLDistrict && HGLDistrict.length== 0? 
            <Select disabled />:  
            <Select
            >
             {HGLDistrict.map((item) =>{
                 return(
                   <Option key={item.Id}>{item.DistrictName}</Option>
                 )
             })}
          </Select>
          )}
          </FormItem>
          <FormItem
            {...formItemLayout}
            label="AgeBaby"
          >
           {getFieldDecorator('AgeBaby', { 
              initialValue: 6,
              rules: [{ type: 'number', required: true, message: 'Please input your AgeBaby!', whitespace: true }],
             })(
            <InputNumber style={{ width: '100%'}}/>
          )}
          </FormItem>
          <FormItem
            {...formItemLayout}
            label="AgeChild"
          >
          {getFieldDecorator('AgeChild', { 
              initialValue: 18,
              rules: [{ type: 'number', required: true, message: 'Please input your AgeChild!', whitespace: true }],
             })(
              <InputNumber style={{ width: '100%'}}/>
          )}
          </FormItem>
          <FormItem
            {...formItemLayout}
            label="同步时间"
          >
          {getFieldDecorator('SyncInterval', { 
              initialValue: 1,
              rules: [{ type: 'number', required: true, message: 'Please input your SyncInterval!', whitespace: true }],
             })(
              <InputNumber style={{ width: '100%'}}/>
          )}
          </FormItem>
          <FormItem
            {...formItemLayout}
            label="房间数"
          >
          {getFieldDecorator('RoomCount', { 
              initialValue: 1,
              rules: [{ type: 'number', required: true, message: 'Please input your RoomCount!', whitespace: true }],
             })(
              <InputNumber style={{ width: '100%'}}/>
          )}
          </FormItem>
          <FormItem
            {...formItemLayout}
            label="开通PC数"
          >
          {getFieldDecorator('PcCount', { 
              initialValue: 1,
              rules: [{ type: 'number', required: true, message: 'Please input your PcCount!', whitespace: true }],
             })(
              <InputNumber style={{ width: '100%'}}/>
          )}
          </FormItem>
          <FormItem
            {...formItemLayout}
            label="激活医院"
          >
          {getFieldDecorator('DcCount', { 
              initialValue: 1,
              rules: [{ type: 'number', required: true, message: 'Please input your DcCount!', whitespace: true }],
             })(
              <InputNumber style={{ width: '100%'}}/>
          )}
          </FormItem>
          <FormItem
            {...formItemLayout}
            label="地址"
          >
             {getFieldDecorator('Address')(
               <Input />
          )}
          </FormItem>
          <FormItem
            {...formItemLayout}
            label="电话"
          >
             {getFieldDecorator('Telephone')(
               <Input />
          )}
          </FormItem>
          <FormItem
            {...formItemLayout}
            label="院长"
          >
             {getFieldDecorator('Agent')(
               <Input />
          )}
          </FormItem>
          <FormItem
            {...formItemLayout}
            label="手机号"
          >
             {getFieldDecorator('Mobile')(
               <Input />
          )}
          </FormItem>

        </Form>
      )
  }

  renderForm(){
    return(
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col md={4} sm={24}>
              <Button onClick={this.showModal}>新增</Button>
              <Modal
                title="新增医院信息"
                visible={this.state.visible}
                // onOk={this.handleOk}
                onCancel={() =>  
                this.setState({
                  visible: false,
                })}
              >
              { this.AddHospitalFrom()}
              </Modal>
          </Col>
          <Col md={6} sm={24}>
            <FormItem label="搜索">
                <Input onChange={(e) => this.handleSearch(e)} placeholder="按医院名称搜索" /> 
            </FormItem>
          </Col>
        </Row>
    )
  }
  handleSelectRows = rows => {
    this.setState({
      selectedRows: rows,
    });
  };

  handleStandardTableChange = (pagination, filtersArg, sorter) => {
      const { dispatch } = this.props;
      const { formValues } = this.state;

      const filters = Object.keys(filtersArg).reduce((obj, key) => {
        const newObj = { ...obj };
        newObj[key] = getValue(filtersArg[key]);
        return newObj;
      }, {});

      const params = {
        currentPage: pagination.current,
        pageSize: pagination.pageSize,
        ...formValues,
        ...filters,
      };
      if (sorter.field) {
        params.sorter = `${sorter.field}_${sorter.order}`;
      }
       
      dispatch({
        type: 'hospital/fetch',
        payload: params,
      });
  };

  render() {
     const { hospital:{ HGLData } , loading } = this.props;
     const { selectedRows  } = this.state;
    return (
       <Card title="医院管理" bordered={false}>
            <div className={styles.tableList}>
              <div className={styles.tableListForm}>{this.renderForm()}</div>
              <div className={styles.tableListOperator}>
                  <StandardTable
                    pagination={{ pageSize: 50 }} 
                    scroll={{ y: 450 }}
                    selectedRows={selectedRows}
                    loading={loading}
                    data={HGLData}
                    columns={this.columns}
                    pagination={{
                      size: "small",
                      pageSize: 100,
                      showSizeChanger: true,
                      showQuickJumper: true,
                      showTotal: function (total, range) {  //设置显示一共几条数据
                          return  '第 ' + range[0] + ' - ' + range[1] + ' 条'+ '  ' + `（共 ${total} 条）`; 
                      }
                    }}
                    onSelectRow={this.handleSelectRows}
                    onChange={this.handleStandardTableChange}
                  />
              </div>
            </div>
       </Card>
    );
  }
}

export default HospitalGL;