import React, { PureComponent } from 'react';
import { connect } from 'dva';
import { Card, Row, Col, Input, Form, Button, Modal, Icon, Select, Radio, Popconfirm} from 'antd';
import StandardTable from '@/components/StandardTable';
import GridContent from '@/components/PageHeaderWrapper/GridContent';
import { CommonInterface  } from '@/services/params';
import  ShareManAge from '../ShareManAge';

import styles from './Doctors.less';

const { iconstyle, formItemLayout } = CommonInterface;
const FormItem = Form.Item;
const Option = Select.Option;

@connect(({ doctors, monitor, department, loading }) => ({
  doctors,
  monitor,
  department,
  loading: loading.models.doctors
}))@Form.create()

export default class Doctors extends PureComponent {

  state ={
    selectedRows: [],
    visible: false,
    visibles: false,
    visibleX: false,
    PswdValue: "",
    PswdId: "",
    departName: ""
  }

  columns =[
    {
      title: '医院名称',
      dataIndex: 'HospitalName',
      width: '15%'
    },
    {
      title: '科室名称',
      dataIndex: 'DepartmentName',
      width: '15%'
    },
    {
      title: '姓名',
      dataIndex: 'Name',
      width: '15%'
    },
    {
      title: '性别',
      dataIndex: 'Gender',
      width: '8%'
    },
    {
      title: '医生编号',
      dataIndex: 'Id',
      width: '12%',
      sorter: true,
      sorter: (a, b) => a.Id - b.Id
    },
    {
      title: '手机',
      dataIndex: 'Mobile',
      width: '15%'
    },
    // {
    //   title: '擅长',
    //   dataIndex: 'Expert',
    //   width: '10%'
    // },
    // {
    //   title: 'Email',
    //   dataIndex: 'Email',
    //   width: '10%'
    // },
    {
      title: '操作',
      width: '15%',
      dataIndex: 'Operation',
      render: (text, record) =>(
        <span>
          <Icon type="form" onClick={() =>
             this.setState({ 
                visible: true, 
                record: record,
              })
          } style={iconstyle}/> 
  
          <Popconfirm title="确定要删除吗？"onConfirm={() => this.deleteDoctor(record.Id)} onCancel={(e) => console.log(e)} okText="确定" cancelText="取消">
             <Icon type="delete" style={iconstyle}/>
          </Popconfirm>
          <Icon type="setting" style={iconstyle} 
          onClick={() =>  
            this.setState({
               visibleX: true,
               PswdId: record.Id
          })}/> 
      </span>
      ),
    }
  ]

  deleteDoctor(key){
    const { dispatch} = this.props;
    dispatch({
      type: 'doctors/fetchDoctorDelete',
      payload:{
         Id: key
      }
    })
  }

  UpdateDepartment(){
    const { 
      form:{ getFieldDecorator, },
      department:{ departmentList },
      doctors:{ selected },
      dispatch
    } = this.props;
    const { record } = this.state;
    return(
      <Form>
            <FormItem
              {...formItemLayout}
              label="*科室"
            >
              {getFieldDecorator('DepartmentUids', {
               initialValue: record? record.DepartmentName: "",
            })(
              <Select 
              onChange={(e) =>{
                  departmentList&& departmentList.list?  departmentList.list.forEach((c) =>{
                      if(e == c.key){
                        this.setState({
                          departName: c.Name
                        })
                      }
                  }):''
              }}
              onFocus={() =>{
                dispatch({
                  type: 'department/fetchTreeNode',
                  payload:{
                    url: "admin/department/query",
                    params: `_dc=1545114961248&hid=${selected}&page=1&start=0&limit=1000`,
                  }
                });
              }}>
                {departmentList&& departmentList.list? departmentList.list.map((item) =>{
                      return(
                        <Option key={item.key}>{item.Name}</Option>
                      )
                  }) : ''}
              </Select>
            )}
            </FormItem>
            <FormItem
              {...formItemLayout}
              label="*姓名"
            >
              {getFieldDecorator('Names', {
              initialValue: record? record.Name: "",
            })(
              <Input/>
            )}
            </FormItem>
            <FormItem
              {...formItemLayout}
              label="*手机号"
            >
              {getFieldDecorator('Mobiles', {
              initialValue: record? record.Mobile: "",
            })(
              <Input/>
            )}
            </FormItem>
            <FormItem
              {...formItemLayout}
              label="*性别"
            >
              {getFieldDecorator('Genders',{
                initialValue: record? record.Gender== "女"? "2": "1" : ""
              })(
                <Radio.Group>
                  <Radio value="1">男</Radio>
                  <Radio value="2">女</Radio>
                </Radio.Group>
              )}
          </FormItem>
            <FormItem
                {...formItemLayout}
                label="*演示账号"
              >
                {getFieldDecorator('IsSSOs',{
                  initialValue: record? record.IsSSO == 1? "1": "2" : "",
                })(
                  <Radio.Group>
                    <Radio value="1">不是</Radio>
                    <Radio value="2">是</Radio>
                  </Radio.Group>
                )}
            </FormItem>
            <FormItem
                {...formItemLayout}
                label="*PC医生账号权限"
              >
                {getFieldDecorator('Jurisdictions',{
                  initialValue: record? record.Jurisdiction == 1? "1": "2" : "",
                })(
                  <Radio.Group>
                    <Radio value="1">高级权限</Radio>
                    <Radio value="2">低级权限</Radio>
                  </Radio.Group>
                )}
            </FormItem>
            <FormItem
                {...formItemLayout}
                label="电子邮箱"
              >
                {getFieldDecorator('Emails',{
                  initialValue: record? record.Email: ""
                })(
                <Input/>
              )}
            </FormItem>
            <FormItem
              {...formItemLayout}
              label="职位"
            >
              {getFieldDecorator('Titles',{
                 initialValue: record? record.Title: ""
              })(
              <Input/>
            )}
            </FormItem>
            <FormItem
              {...formItemLayout}
              label="擅长"
            >
              {getFieldDecorator('Experts',{
                initialValue: record? record.Expert: ""
              })(
              <Input/>
            )}
            </FormItem>
            <FormItem
              {...formItemLayout}
              label="备注"
            >
              {getFieldDecorator('Comments',{
                initialValue: record? record.Comment: ""
              })(
              <Input/>
            )}
            </FormItem>
      </Form>
    )
  }

  UpdateDepartOK(){
    const {  form:{ validateFieldsAndScroll }, dispatch } = this.props;
    validateFieldsAndScroll((err, values) => {
      const {  DepartmentUids, Names, Mobiles, Genders, IsSSOs, Jurisdictions, hospital} = values;
      const {  record } = this.state;
      dispatch({
        type: 'doctors/fetchDoctorUpdate',
        payload: {
          Comment: values.Comments, 
          Department: record.DepartmentName, 
          DepartmentUid: record.key, 
          Email: values.Emails, 
          Expert: values.Experts, 
          Id: record.Id,
          Gender: Genders, 
          IsSSO: IsSSOs, 
          HospitalId: hospital,
          Jurisdiction: Jurisdictions, 
          Mobile: Mobiles, 
          Name:  Names, 
          Title: values.Titles
        }
      });
      this.setState({
        visible: false,
      })
    })
  }

  rendersForm(){
      return(
      <Row gutter={{ md: 8, lg: 24 }}>
          <Col md={3} sm={24}>
            <Button onClick={() =>  
            this.setState({
              visibles: true,
          })}>新增</Button>
            <Modal
              title="新增科室"
              visible={this.state.visibles}
              onOk={this.addKeShiOK}
              onCancel={() =>  
              this.setState({
                visibles: false,
              })}
            >
            { this.AddDepartment()}
            </Modal>
        </Col>
      </Row>
    )
  }

  AddDepartment(){
    const { 
      form:{ getFieldDecorator, },
      monitor:{ tagsList},
      department:{ departmentList },
      doctors:{ selected },
      dispatch
    } = this.props;
    return(
      <Form>
          <FormItem
              {...formItemLayout}
              label="*医院"
            >
              {getFieldDecorator('hospital', {
              initialValue: selected,
            })(
              <Select>
                  {tagsList? tagsList.map((item) =>{
                      return(
                        <Option key={item.key}>{item.title}</Option>
                      )
                  }) : ''}
              </Select>
            )}
            </FormItem>
          <FormItem
              {...formItemLayout}
              label="*科室"
            >
              {getFieldDecorator('DepartmentUid')(
              <Select 
              onChange={(e) =>{
                  departmentList&& departmentList.list?  departmentList.list.forEach((c) =>{
                      if(e == c.key){
                        this.setState({
                          departName: c.Name
                        })
                      }
                  }):''
              }}
              onFocus={() =>{
                dispatch({
                  type: 'department/fetchTreeNode',
                  payload:{
                    url: "admin/department/query",
                    params: `_dc=1545114961248&hid=${selected}&page=1&start=0&limit=250`,
                  }
                });
              }}>
                {departmentList&& departmentList.list? departmentList.list.map((item) =>{
                      return(
                        <Option key={item.key}>{item.Name}</Option>
                      )
                  }) : ''}
              </Select>
            )}
            </FormItem>
          <FormItem
              {...formItemLayout}
              label="*姓名"
            >
              {getFieldDecorator('Name')(
              <Input/>
            )}
            </FormItem>
          <FormItem
              {...formItemLayout}
              label="*手机号"
            >
              {getFieldDecorator('Mobile')(
              <Input/>
            )}
            </FormItem>
          <FormItem
              {...formItemLayout}
              label="*性别"
            >
              {getFieldDecorator('Gender')(
                <Radio.Group>
                  <Radio value="1">男</Radio>
                  <Radio value="2">女</Radio>
                </Radio.Group>
              )}
          </FormItem>
          <FormItem
              {...formItemLayout}
              label="*演示账号"
            >
              {getFieldDecorator('IsSSO')(
                <Radio.Group>
                  <Radio value="1">不是</Radio>
                  <Radio value="2">是</Radio>
                </Radio.Group>
              )}
          </FormItem>
          <FormItem
              {...formItemLayout}
              label="*PC医生账号权限"
            >
              {getFieldDecorator('Jurisdiction')(
                <Radio.Group>
                  <Radio value="1">高级权限</Radio>
                  <Radio value="2">低级权限</Radio>
                </Radio.Group>
              )}
          </FormItem>
          <FormItem
              {...formItemLayout}
              label="电子邮箱"
            >
              {getFieldDecorator('Email')(
              <Input/>
            )}
            </FormItem>
          <FormItem
            {...formItemLayout}
            label="职位"
          >
            {getFieldDecorator('Title')(
            <Input/>
          )}
          </FormItem>
          <FormItem
            {...formItemLayout}
            label="擅长"
          >
            {getFieldDecorator('Expert')(
            <Input/>
          )}
          </FormItem>
          <FormItem
              {...formItemLayout}
              label="备注"
            >
              {getFieldDecorator('Comment')(
              <Input/>
            )}
            </FormItem>
      </Form>
    )
  }

  addKeShiOK= () =>{
    const {  form:{ validateFieldsAndScroll }, dispatch } = this.props;
    validateFieldsAndScroll((err, values) => {
      const { hospital, DepartmentUid, Name, Mobile, Gender, IsSSO, Jurisdiction} = values;
      const { departName } = this.state;
      if( hospital&&DepartmentUid&&Name&&Mobile&&Gender&&IsSSO&&Jurisdiction){
        dispatch({
          type: 'doctors/fetchDoctorAdd',
          payload: {
            Comment: values.Comment, 
            Department: departName, 
            DepartmentUid: DepartmentUid, 
            Email: values.Email, 
            Expert: values.Expert, 
            Gender: Gender, 
            HospitalId: hospital, 
            IsSSO: IsSSO, 
            Jurisdiction: Jurisdiction, 
            Mobile: Mobile, 
            Name:  Name, 
            Title: values.Title
          }
        });
        this.setState({
          visibles: false,
        })
      }else{
        console.log('Received values of form: ', values);
      }
    
    })
  }

  UpdatePswdOK(){
    const { dispatch } = this.props;
    dispatch({
      type: 'doctors/fetchDoctorPswd',
      payload:{
        Id: this.state.PswdId,
        Pswd: this.state.PswdValue,
      }
    });
    this.setState({
        visibleX: false
    })
  }

  render() {

    const { doctors:{ DoctorList }, loading, dispatch } = this.props;
    const { selectedRows, visible,visibleX } = this.state;
    return (
       <div style={{ flex: 1}}>
            <Row gutter={24}>
                <ShareManAge types="DoctorsManAge" />
                <Col xl={18} lg={24} md={24} sm={24} xs={24}>
                     <Card title="医生管理" bordered={false} style={{  height: "742px"}}>
                          <div className={styles.tableListForm}>
                              {this.rendersForm()}
                          </div>
                          <Row>
                              <div className={styles.tableListOperator}>
                                  <StandardTable
                                      pagination={{ pageSize: 50 }} 
                                      scroll={{ y: 450 }}
                                      data={DoctorList}
                                      selectedRows={selectedRows}
                                      loading={loading}
                                      columns={this.columns}
                                      pagination={{
                                        size: "small",
                                        pageSize: 40,
                                        showSizeChanger: true,
                                        showQuickJumper: true,
                                        showTotal: function (total, range) {  //设置显示一共几条数据
                                            return  '第 ' + range[0] + ' - ' + range[1] + ' 条'+ '  ' + `（共 ${total} 条）`; 
                                        }
                                      }}
                                    />
                              </div>
                          </Row>
                     </Card>
                </Col>
                <Modal
                  title="修改医生信息"
                  visible={visible}
                  onOk={() => this.UpdateDepartOK()}
                  onCancel={() =>{
                    this.setState({
                      visible: false
                    })
                  }
                    }
                >
                { this.UpdateDepartment()}
                </Modal>
                <Modal
                  title="设置密码"
                  visible={visibleX}
                  onOk={() => this.UpdatePswdOK()}
                  onCancel={() =>{
                    this.setState({
                      visibleX: false
                    })
                  }
                    }
                >
                 密码: <Input placeholder="输入密码" onChange={(e) => this.setState({ PswdValue: e.target.value})} />
                </Modal>
            </Row>
       </div>
    );
  }
}
