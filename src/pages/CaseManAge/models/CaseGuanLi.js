import * as services  from '@/services/api';

export default {
    namespace: 'CaseGuanLi',
  
    state: {
      BeatCaseList: [],
      TongueList:[],
      FaceList: []
    },
  
    effects: {
        *BeatCaseList({ payload:{ url, params } }, { call, put}){
            const res = yield call(services.BeatCaseDetail, url, params);
            yield put({ type: 'save', payload: { BeatCaseList: res } });
        },
        *BeatTongueList({ payload:{ url, params } }, { call, put}){
          const res = yield call(services.BeatTongueDetail, url, params);
          yield put({ type: 'save', payload: { TongueList: res } });
        },
        *BeatFaceList({ payload:{ url, params } }, { call, put}){
          const res = yield call(services.BeatFaceDetail, url, params);
          yield put({ type: 'save', payload: { FaceList: res } });
        },
    },
  
    reducers: {
      save(state, { payload }) {
        return {
          ...state,
          ...payload,
        };
      }
    },
  };
  