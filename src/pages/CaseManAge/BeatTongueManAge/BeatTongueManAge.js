import React, { Component } from 'react';
import { connect } from 'dva';
import { Card, Row, Col } from 'antd';
import styles from '../common.less';
import StandardTable from '@/components/StandardTable';
import { CommonInterface  } from '@/services/params';

const { BeatTongueData } = CommonInterface;

@connect(({ CaseGuanLi, loading }) => ({
    CaseGuanLi,
    loading: loading.effects['CaseGuanLi/BeatTongueList'],
}))

class BeatTongueManAge extends Component {

    componentDidMount() {
        const { dispatch } = this.props;
        dispatch({
          type: 'CaseGuanLi/BeatTongueList',
          payload: BeatTongueData
        });
    }

    state ={
        selectedRows: [],
    }
 
    columns =[
        {
            title: 'Id',
            dataIndex: 'key',
            width: '10%',
            align: 'center'
        },
        {
            title: '医生姓名',
            dataIndex: 'DoctorName',
            width: '10%',
            align: 'center'
        },
        {
            title: '医生编号',
            dataIndex: 'DoctorId',
            width: '10%',
            align: 'center'
        },
        {
            title: '患者姓名',
            dataIndex: 'PatientName',
            width: '10%',
            align: 'center'
        },
        {
            title: '患者编号',
            dataIndex: 'PatientId',
            width: '10%',
            align: 'center'
        },
        {
            title: '图片1',
            dataIndex: 'Step1Pic',
            width: '10%',
            align: 'center'
        },
        {
            title: '图片2',
            dataIndex: 'Step2Pic',
            width: '10%',
            align: 'center'
        },
        {
            title: '图片3',
            dataIndex: 'ImageOut',
            width: '10%',
            align: 'center'
        },
        {
            title: '创建时间',
            dataIndex: 'CreateTime',
            width: '15%',
            align: 'center'
        }
    ]

    render() {
        const { selectedRows } = this.state;
        const {  CaseGuanLi:{ TongueList }, loading } = this.props;
        return(
            <Card title="智能舌象管理" bordered={false}>
                <div className={styles.tableList}>
                    <div className={styles.tableListOperator}>
                        <StandardTable
                        pagination={{ pageSize: 50 }} 
                        scroll={{ y: 450 }}
                        selectedRows={selectedRows}
                        data={TongueList}
                        loading={loading}
                        columns={this.columns}
                        pagination={{
                            size: "small",
                            pageSize: 50,
                            showSizeChanger: true,
                            showQuickJumper: true,
                            showTotal: function (total, range) {  //设置显示一共几条数据
                                return  '第 ' + range[0] + ' - ' + range[1] + ' 条'+ '  ' + `（共 ${total} 条）`; 
                            }
                        }}
                        />
                    </div>
                </div>
            </Card>
        )
    }
}


export default BeatTongueManAge;