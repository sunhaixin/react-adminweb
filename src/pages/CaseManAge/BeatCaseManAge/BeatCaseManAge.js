import React, { Component } from 'react';
import { connect } from 'dva';
import { Card, Row, Col } from 'antd';
import styles from '../common.less';
import StandardTable from '@/components/StandardTable';
import { CommonInterface  } from '@/services/params';

const { BeatCaseData } = CommonInterface;

@connect(({ CaseGuanLi, loading }) => ({
    CaseGuanLi,
    loading: loading.effects['CaseGuanLi/BeatCaseList'],
}))

class BeatCaseManAge extends Component {

    componentDidMount() {
        const { dispatch } = this.props;
        dispatch({
          type: 'CaseGuanLi/BeatCaseList',
          payload: BeatCaseData
        });
    }

    state ={
        selectedRows: [],
    }
 
    columns =[
        {
            title: 'Id',
            dataIndex: 'key',
            width: '15%',
            align: 'center'
        },
        {
            title: '医生姓名',
            dataIndex: 'DoctorName',
            width: '15%',
            align: 'center'
        },
        {
            title: '医生编号',
            dataIndex: 'DoctorId',
            width: '15%',
            align: 'center'
        },
        {
            title: '患者姓名',
            dataIndex: 'PatientName',
            width: '15%',
            align: 'center'
        },
        {
            title: '患者编号',
            dataIndex: 'PatientId',
            width: '15%',
            align: 'center'
        },
        {
            title: '创建时间',
            dataIndex: 'CreateTime',
            width: '15%',
            align: 'center'
        }
    ]

    render() {
        const { selectedRows } = this.state;
        const {  CaseGuanLi:{ BeatCaseList }, loading } = this.props;
        return(
            <Card title="拍病例管理" bordered={false}>
                <div className={styles.tableList}>
                    <div className={styles.tableListOperator}>
                        <StandardTable
                        pagination={{ pageSize: 50 }} 
                        scroll={{ y: 450 }}
                        selectedRows={selectedRows}
                        data={BeatCaseList}
                        loading={loading}
                        columns={this.columns}
                        pagination={{
                            size: "small",
                            pageSize: 50,
                            showSizeChanger: true,
                            showQuickJumper: true,
                            showTotal: function (total, range) {  //设置显示一共几条数据
                                return  '第 ' + range[0] + ' - ' + range[1] + ' 条'+ '  ' + `（共 ${total} 条）`; 
                            }
                        }}
                        />
                    </div>
                </div>
            </Card>
        )
    }
}


export default BeatCaseManAge;