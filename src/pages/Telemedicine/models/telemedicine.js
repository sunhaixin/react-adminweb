import * as services  from '@/services/api';
import { CommonInterface } from '@/services/params';

const { telemedicineData } = CommonInterface;

export default {
  namespace: 'telemedicine',

  state: {
    TelemedicineList: []
  },

  effects: {
     *telemedicineQuery({ payload:{ url, params } }, { call, put }){
        const res = yield call(services.TelemedicineQuery, url, params);
        yield put({ type: 'save', payload: {TelemedicineList: res } });
     },

     *fetchSearch({payload}, { call, put }){
        const { url, params  } = telemedicineData;
        const res = yield call(services.TelemedicineQuery, url, params)
        var tempList = [];
        if(payload){
          res.forEach((e) =>{
              if(e.title.indexOf(payload) != -1){
                tempList.push(e);
              }
          })
          yield put({ type: 'save', payload: { TelemedicineList: tempList } })
        }else{
          yield put({ type: 'save', payload: { TelemedicineList: res } })
        }
    },

    *telemdicineDetail({ payload: { url, params}}, { call, put }){
        const res = yield call(services.TelemdicineDetail, url, params);
        yield put({ type: 'save', payload: {detailList: res } });
    },

    *fetchAddYuanCheng({ payload:{ name }}, { call, put}){
        yield call(services.AddYuanCheng, name);
        const { url, params  } = telemedicineData;
        const res = yield call(services.TelemedicineQuery, url, params);
        yield put({ type: 'save', payload: {TelemedicineList: res } });
    }
  },

  reducers: {
    save(state, { payload }) {
      return {
        ...state,
        ...payload,
      };
    }
  },
};
