import React, { Component } from 'react';
import { connect } from 'dva';
import { Card, Row, Col, Tree, Spin, Form, Input, Button, Modal} from 'antd';
import StandardTable from '@/components/StandardTable';
import { CommonInterface } from '@/services/params';

import styles from './Telemedicine.less';

const FormItem = Form.Item;
const { TreeNode } = Tree;
const { telemedicineData, formItemLayout } = CommonInterface;

@connect(({telemedicine, loading }) => ({
    telemedicine,
    loadings: loading.effects['telemedicine/telemdicineDetail'],
    loading: loading.models.telemedicine
}))@Form.create()

export default class Telemedicine extends Component{
    state ={
        visible: false,
        selectedRows: []
    };

    columns = [
        {
            title: '',
            dataIndex: 'Id',
            width: '8%'
        },
        {
            title: '医院名称',
            dataIndex: 'HospitalName',
            width: '20%'
        },
        {
            title: '管理编号',
            dataIndex: 'key',
            width: '25%'
        }
    ]

    componentDidMount(){
        const { dispatch } = this.props;
        dispatch({
          type: 'telemedicine/telemedicineQuery',
          payload: telemedicineData
        });
    }

    changeSearch(e){
        const { dispatch } = this.props;
        dispatch({
            type: 'telemedicine/fetchSearch',
            payload: e.target.value
        });
    }

    renderForm(){
        return(
            <Row gutter={{ md: 8, lg: 24 }}>
                <Col sm={24}>
                    <FormItem>
                        <Button 
                        onClick={() => this.setState({visible: true})}
                        style={{ width: '100%'}}>新增远程医疗小组</Button>
                    </FormItem>
                    <FormItem label="搜索">
                        <Input onChange={(e) => this.changeSearch(e)}/> 
                    </FormItem>
                    <Modal
                        title="添加远程医疗小组"
                        visible={this.state.visible}
                        onOk={this.AddYuanChengOk}
                        onCancel={() => this.setState({ visible: false})}
                    >
                    { this.AddYuanCheng()}
                    </Modal>
                </Col>
            </Row>
        )
    }

    AddYuanCheng(){
        const { 
            form:{ getFieldDecorator }
          } = this.props;
         return(
          <Form>
             <FormItem
              {...formItemLayout}
              label="远程小组"
            >
               {getFieldDecorator('YuanChengName',{
                   rules: [{
                    required: true, message: '请填写远程小组!',
                  }],
               })(
              <Input />
            )}
            </FormItem>
          </Form>
         )
    }

    AddYuanChengOk =() =>{
        const {  form:{ validateFieldsAndScroll }, dispatch } = this.props;
        validateFieldsAndScroll((err, values) => {
            dispatch({
                type: 'telemedicine/fetchAddYuanCheng',
                payload: {
                    name: values.YuanChengName,
                }
            });
            this.setState({
                visible: false,
            })
        })
    }

    renderTreeNodes = data => data.map((item) => {
        return <TreeNode {...item} dataRef={item} Name={item} />;
    })

    SelectTreeNode(e){
        const { dispatch } = this.props;
        if(e[0]){
            dispatch({
                type: 'telemedicine/telemdicineDetail',
                payload:{
                    url: "admin/telemedicine/group/hospital/query",
                    params: `_dc=1546320545341&gid=${e[0]}&page=1&start=0&limit=1000`,
                }
            });
        }else{
            dispatch({
              type: 'telemedicine/save',
              payload:{
                detailList:""
              }
            });
          }
    }

    render(){
        const { telemedicine:{ TelemedicineList, detailList} , loading, loadings } = this.props;
        const { selectedRows } = this.state;
        return(
            <div style={{ flex: 1}}>
                 <Row gutter={24}>
                    <Col xl={6} lg={24} md={24} sm={24} xs={24}>
                        <Card title="添加医疗小组名称" bordered={false} style={{height: "780px"}}> 
                                <div className={styles.tableListForm}>
                                    {this.renderForm()}
                                </div>
                                <Row>
                                    <div className={styles.departBody}>
                                        <Spin spinning={loading}>
                                            <Tree 
                                            onSelect={(e) => 
                                            this.SelectTreeNode(e)}
                                            >
                                            {this.renderTreeNodes(TelemedicineList)}
                                            </Tree>
                                        </Spin>
                                </div>
                                </Row>
                        </Card>
                    </Col>
                    <Col xl={18} lg={24} md={24} sm={24} xs={24}>
                        <Card title="远程医疗小组成员" bordered={false} style={{height: "780px"}}>     
                            <Row>
                                <div className={styles.tableListOperator}>
                                    <StandardTable
                                        pagination={{ pageSize: 50 }} 
                                        scroll={{ y: 450 }}
                                        data={detailList}
                                        selectedRows={selectedRows}
                                        loading={loadings}
                                        columns={this.columns}
                                        pagination={{
                                            size: "small",
                                            pageSize: 40,
                                            showSizeChanger: true,
                                            showQuickJumper: true,
                                            showTotal: function (total, range) {  //设置显示一共几条数据
                                                return  '第 ' + range[0] + ' - ' + range[1] + ' 条'+ '  ' + `（共 ${total} 条）`; 
                                            }
                                        }}
                                        />
                                </div>
                            </Row>                 
                        </Card>                     
                    </Col>
                 </Row>
            </div>
        )
    }
}