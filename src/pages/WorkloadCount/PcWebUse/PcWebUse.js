import { connect } from 'dva';
import DataSet from "@antv/data-set";
import React, { Component } from 'react';
import { Card, Row, Col, Table } from 'antd';
import styles from './PcWebUse.less';
import { Chart, Geom, Axis, Tooltip, Label, Legend, Coord } from "bizcharts";

@connect(({pcwebuse, loading }) => ({
    pcwebuse,
    loading: loading.effects['pcwebuse/PcWebUseStat']
}))

export default class PcWebUse extends Component{

    columns = [{
        title: '项目',
        dataIndex: 'name',
        width: '20%',
      },{
        title: '工作量(人次)',
        dataIndex: 'value',
        width: '15%',
    }];

    list_ = [{
        item: "高血压病辅助辨证",
        count: 44.34},
    {
        item: "糖尿病辅助辨证",
        count: 55.66
    }]

    componentDidMount(){
        const { dispatch } = this.props;
        dispatch({
          type: 'pcwebuse/PcWebUseStat',
          payload: {
              url: 'admin/pceva/stat',
              params: 'hospitalId=1'
          }
        });
    }

    render(){
        const { DataView } = DataSet;
        const { pcwebuse :{ startTime, endTime, list, count }, loading } = this.props;
        const dv = new DataView();
        dv.source(this.list_).transform({
            type: "percent",
            field: "count",
            dimension: "item",
            as: "percent"
        });
        return(
            <div style={{ flex: 1}}>
                 <Row>
                     <Col>
                        <Card 
                            title="Web测评使用情况"
                            bordered={false} style={{height: "300px"}}>  
                            <div className={styles.peopleCenter}>
                                <p>有效使用时间: {startTime} - {endTime}</p>
                                <p>剩余服务总次数: {count}</p>
                               <div className={styles.peoplebottom}>
                                    {list.map((item,i) =>{
                                         return(
                                            <Card key={i} 
                                            loading={loading} 
                                            className={styles.peopleLeft}>
                                            <Card.Meta 
                                            title={item.name}
                                            description={<p>服务次数: {item.value}</p>}/>
                                            </Card>
                                         )
                                     })
                                    }
                               </div>
                            </div>
                        </Card>
                     </Col>
                     <Col>
                        <Card 
                            title="工作量统计"
                            bordered={false} style={{height: "500px"}}>  
                            <div className={styles.workerCenter}>
                                    <div className={styles.workerLeft}>
                                        <Table 
                                         loading={loading}
                                         columns={this.columns} 
                                         dataSource={list} 
                                         pagination={false}/>
                                    </div>
                                    <div className={styles.workerRight}>
                                            <Chart 
                                            height={400} 
                                            data={dv} 
                                            scale={[{ dataKey: 'percent'}]} 
                                            padding={[-30, 100, 100, 20]} forceFit>
                                            <Coord type="theta" radius={0.75} />
                                            <Axis name="percent" />
                                            <Legend 
                                            textStyle={{ fontSize: 15}}
                                            position="right" offsetY={-20} offsetX={-60} />
                                            <Tooltip showTitle={false}/>
                                            <Geom
                                                type="intervalStack"
                                                position="percent"
                                                color="item"
                                                tooltip={[ "item*percent",(item, percent) => {
                                                    return {
                                                         name: item,
                                                         value: Math.floor(percent* 10000) / 100 + "%"
                                                    };
                                                }]}
                                                style={{ lineWidth: 1, stroke: "#fff"}}>
                                                <Label
                                                content="percent"
                                                textStyle={{
                                                    rotate: 0,
                                                    textAlign: "center",
                                                    shadowBlur: 2,
                                                    fontSize: 15,
                                                    shadowColor: "rgba(0, 0, 0, .45)"
                                                }}
                                                formatter={(val, item) => {
                                                    return  item.point.item+ ': '+ Math.floor(val* 10000) / 100 + "%";
                                                }}
                                                />
                                            </Geom>
                                        </Chart>
                                    </div>
                            </div>
                        </Card>
                     </Col>
                 </Row>
            </div>
        )
    }
}