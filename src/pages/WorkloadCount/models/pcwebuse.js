import * as services  from '@/services/api';

export default {
  namespace: 'pcwebuse',

  state: {
     list: [],
     startTime: "",
     endTime: "",
     count:""
  },

  effects: {
     *PcWebUseStat({ payload:{ url, params } }, { call, put }){
        const res = yield call(services.PcWebUseStat, url, params);
        if(res.IsSuccess){
          if(res.Data){
            let dataList = [];
            res.Data.forEach((item, i) =>{
                dataList.push({
                   key: i,
                   name: item.Name,
                   value: item.Count
                })
            })
            yield put({ type: 'save', payload: { list: dataList } });
          }
          yield put({ 
            type: 'save', 
            payload: { 
              startTime: res.Remainder.ServiceStart, 
              endTime: res.Remainder.ServiceEnd,
              count: res.Remainder.Count
            } 
         });
        }
     },
  },

  reducers: {
    save(state, { payload }) {
      return {
        ...state,
        ...payload,
      };
    }
  },
};
