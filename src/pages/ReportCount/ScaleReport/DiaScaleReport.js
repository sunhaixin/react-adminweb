import { connect } from 'dva';
import React, { Component } from 'react';
import { CommonInterface } from '@/services/params';
import { SCALE_DIA }  from '@/constants';
import ShareReport from '../ShareReport';

const { DiaEvaData } = CommonInterface;

@connect(({reportEva, loading }) => ({
    reportEva,
    loading: loading.effects['reportEva/DiaReportEva']
}))

export default class DiaScaleReport extends Component{

    componentDidMount(){
        const { dispatch } = this.props;
        dispatch({
            type: 'reportEva/DiaReportEva',
            payload: DiaEvaData
        });
    }

    render(){
        const { reportEva:{ DiaReportEva:{ dataList, items } } , loading} = this.props;
        return(
                <ShareReport
                title="糖尿病常见证型分布Top5"
                dataList={dataList}
                columns={SCALE_DIA}
                items={items}
                loading={loading}
                scalestyle={{padding: '20px' , margin: '0 auto', width:  1000}}
                />
                 
        )
    }
}
