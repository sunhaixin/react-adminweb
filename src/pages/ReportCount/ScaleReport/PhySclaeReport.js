import { connect } from 'dva';
import DataSet from "@antv/data-set";
import React, { Component } from 'react';
import styles from './PhySclaeReport.less';
import { Card, Row, Col, Table} from 'antd';
import { CommonInterface } from '@/services/params';
import { Chart, Geom, Axis, Tooltip, Label, Legend } from "bizcharts";

const { PhyEvaData } = CommonInterface;

@connect(({reportEva, loading }) => ({
    reportEva,
    loading: loading.effects['reportEva/PhyReportEva']
}))

export default class PhySclaeReport extends Component{

    componentDidMount(){
        const { dispatch } = this.props;
        dispatch({
          type: 'reportEva/PhyReportEva',
          payload: PhyEvaData
        });
    }

    columns = [
      {
        title: '心理类型',
        dataIndex: 'name'
      },
      {
        title: '高分人群',
        dataIndex: 'value1'
      },
      {
        title: '低分人群',
        dataIndex: 'value2'
      },
    ];

    render(){
        const { reportEva:{ PhyReportEva } , loading } = this.props;
        const { dataList, RegionCount, AgeGroup } = PhyReportEva;
        const data = [
            {
              name: "高分人群",
              "Jan.": 18.9,
              "Jan1": 18.9,

            },
            {
              name: "低分人群",
              "Jan.": 12.4,
              "Jan1": 12.4,

            }
          ];
          const ds = new DataSet();
          const dv = ds.createView().source(RegionCount?RegionCount:data);
          dv.transform({
            type: "fold",
            fields: AgeGroup? AgeGroup :["Jan.", "Jan1"],
            key: "心理类型",
            value: "人次" 
          });
        return (
        <div style={{ flex: 1}}>
            <Row>
                <Col>
                    <Card title="居民心理类型分布" bordered={false} style={{height: "780px"}}>  
                        <div className={styles.peopleCenter}>
                            <div className={styles.peopleLeft}>   
                                    <Table 
                                    loading={loading}
                                    columns={this.columns} 
                                    dataSource={dataList} 
                                    size="small"
                                    bordered
                                    pagination={false}/>
                            </div>
                            <div className={styles.peopleRight}>
                                <Chart
                                padding={[ 50, 0, 80, 45 ]}
                                height={500} data={dv} forceFit>
                                    <Axis name="心理类型" />
                                    <Axis name="人次" />
                                    <Legend/>
                                    <Tooltip
                                        crosshairs={{
                                        type: "y"
                                        }}
                                    />
                                    <Geom
                                        type="interval"
                                        position="心理类型*人次"
                                        color={"name"}
                                        adjust={[
                                        {
                                            type: "dodge",
                                            marginRatio: 1 / 32
                                        }
                                        ]}
                                    >
                                        <Label 
                                        textStyle={{fontSize: 12}}
                                        content={["心理类型*人次", (name, value)=>{
                                            return `${value}`;
                                        }]}
                                        />
                                    </Geom>
                                </Chart>
                            </div>
                        </div>
                    </Card>                     
                </Col>
            </Row>
        </div>
        )
    }
} 