import { connect } from 'dva';
import React, { Component } from 'react';
import { CommonInterface } from '@/services/params';
import { SCALE_TCM }  from '@/constants';
import ShareReport from '../ShareReport';

const { TcmEvaData } = CommonInterface;

@connect(({reportEva, loading }) => ({
    reportEva,
    loading: loading.effects['reportEva/TcmReportEva']
}))

export default class TcmScaleReport extends Component{

    componentDidMount(){
        const { dispatch } = this.props;
        dispatch({
            type: 'reportEva/TcmReportEva',
            payload: TcmEvaData
        });
    }

    render(){
        const { reportEva:{ TcmReportEva:{ dataList, items } } , loading} = this.props;
        return(
                <ShareReport
                title="科研版12体质类型分布"
                dataList={dataList}
                columns={SCALE_TCM}
                items={items}
                loading={loading}
                scalestyle={{padding: '20px' , margin: '0 auto', width:  1000}}
                />
                 
        )
    }
}
