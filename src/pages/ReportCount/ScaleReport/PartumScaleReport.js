import { connect } from 'dva';
import React, { Component } from 'react';
import { CommonInterface } from '@/services/params';
import { SCALE_PARTUM }  from '@/constants';
import ShareReport from '../ShareReport';

const { PartumEvaData } = CommonInterface;

@connect(({reportEva, loading }) => ({
    reportEva,
    loading: loading.effects['reportEva/PartumReportEva']
}))

export default class PartumScaleReport extends Component{

    componentDidMount(){
        const { dispatch } = this.props;
        dispatch({
            type: 'reportEva/PartumReportEva',
            payload: PartumEvaData
        });
    }

    render(){
        const { reportEva:{ PartumReportEva:{ dataList, items } } , loading} = this.props;
        return(
                <ShareReport
                title="产后女性常见中医证型分布Top5"
                dataList={dataList}
                columns={SCALE_PARTUM}
                items={items}
                loading={loading}
                scalestyle={{padding: '20px' , margin: '0 auto', width:  1000}}
                />
                 
        )
    }
}
