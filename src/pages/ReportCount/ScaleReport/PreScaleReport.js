import { connect } from 'dva';
import React, { Component } from 'react';
import { CommonInterface } from '@/services/params';
import { SCALE_PRE }  from '@/constants';
import ShareReport from '../ShareReport';

const { PreEvaData } = CommonInterface;

@connect(({reportEva, loading }) => ({
    reportEva,
    loading: loading.effects['reportEva/PreReportEva']
}))

export default class PreScaleReport extends Component{

    componentDidMount(){
        const { dispatch } = this.props;
        dispatch({
            type: 'reportEva/PreReportEva',
            payload: PreEvaData
        });
    }

    render(){
        const { reportEva:{ PreReportEva:{ dataList_, items } } , loading} = this.props;
        return(
                <ShareReport
                title="孕期女性中医健康状态分布"
                dataList={dataList_}
                columns={SCALE_PRE}
                items={items}
                loading={loading}
                scalestyle={{padding: '20px' , margin: '0 auto', width:  1000}}
                />
                 
        )
    }
}
