import { connect } from 'dva';
import React, { Component } from 'react';
import { SCALE_OLD }  from '@/constants';
import ShareReport from '../ShareReport';

@connect(({reportEva, loading }) => ({
    reportEva,
    loading: loading.effects['reportEva/ScaleReportEva']
}))

export default class ScaleReport extends Component{

    render(){
        const { reportEva:{ ScaleReportEva:{ dataList, items }, ScaleTitle } , loading} = this.props;
        return(
            <ShareReport
            title={ScaleTitle}
            dataList={dataList}
            columns={SCALE_OLD}
            items={items}
            loading={loading}
            scalestyle={{padding: '20px' , margin: '0 auto', width:  1000}}
            />
        )
    }
}
