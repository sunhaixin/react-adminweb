import { connect } from 'dva';
import React, { Component } from 'react';
import { CommonInterface } from '@/services/params';
import { SCALE_CHD }  from '@/constants';
import ShareReport from '../ShareReport';

const { ChdEvaData } = CommonInterface;

@connect(({reportEva, loading }) => ({
    reportEva,
    loading: loading.effects['reportEva/ChdReportEva']
}))

export default class ChdScaleReport extends Component{

    componentDidMount(){
        const { dispatch } = this.props;
        dispatch({
            type: 'reportEva/ChdReportEva',
            payload: ChdEvaData
        });
    }

    render(){
        const { reportEva:{ ChdReportEva:{ dataList, items } } , loading} = this.props;
        return(
                <ShareReport
                title="儿童体质类型分布"
                dataList={dataList}
                columns={SCALE_CHD}
                items={items}
                loading={loading}
                scalestyle={{padding: '20px' , margin: '0 auto', width:  1000}}
                />
                 
        )
    }
}
