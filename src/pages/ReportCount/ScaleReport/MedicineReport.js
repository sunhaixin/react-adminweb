import { connect } from 'dva';
import React, { Component } from 'react';
import { CommonInterface } from '@/services/params';
import { SCALE_MEDICINE }  from '@/constants';
import ShareReport from '../ShareReport';

const { MedicineEvaData } = CommonInterface;

@connect(({reportEva, loading }) => ({
    reportEva,
    loading: loading.effects['reportEva/MedicineReportEva']
}))

export default class MedicineReport extends Component{

    componentDidMount(){
        const { dispatch } = this.props;
        dispatch({
            type: 'reportEva/MedicineReportEva',
            payload: MedicineEvaData
        });
    }

    render(){
        const { reportEva:{ MedicineReportEva:{ dataList, items } } , loading} = this.props;
        return(
                <ShareReport
                title="居民常见中医证型分布"
                dataList={dataList}
                columns={SCALE_MEDICINE}
                items={items}
                loading={loading}
                scalestyle={{padding: '20px' , margin: '0 auto', width:  1000}}
                />
                 
        )
    }
}
