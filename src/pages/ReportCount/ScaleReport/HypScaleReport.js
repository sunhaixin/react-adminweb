import { connect } from 'dva';
import React, { Component } from 'react';
import { CommonInterface } from '@/services/params';
import { SCALE_HYP }  from '@/constants';
import ShareReport from '../ShareReport';

const { HypEvaData } = CommonInterface;

@connect(({reportEva, loading }) => ({
    reportEva,
    loading: loading.effects['reportEva/HypReportEva']
}))

export default class HypScaleReport extends Component{

    componentDidMount(){
        const { dispatch } = this.props;
        dispatch({
            type: 'reportEva/HypReportEva',
            payload: HypEvaData
        });
    }

    render(){
        const { reportEva:{ HypReportEva:{ dataList, items } } , loading} = this.props;
        return(
                <ShareReport
                title="高血压病常见证型分布Top5"
                dataList={dataList}
                columns={SCALE_HYP}
                items={items}
                loading={loading}
                scalestyle={{padding: '20px' , margin: '0 auto', width:  700}}
                />
                 
        )
    }
}
