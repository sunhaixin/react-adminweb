import { connect } from 'dva';
import DataSet from "@antv/data-set";
import { SCALE_CHD }  from '@/constants';
import React, { Component } from 'react';
import styles from './PhySclaeReport.less';
import { Card, Row, Col, Table, Select} from 'antd';
import { CommonInterface } from '@/services/params';
import { Chart, Geom, Axis, Tooltip, Label, Legend } from "bizcharts";

const Option = Select.Option;
const { ChdBiaseData } = CommonInterface;

@connect(({reportEva, loading }) => ({
    reportEva,
    loading: loading.effects['reportEva/ChdBiasedEva']
}))

export default class ChdBiasedReport extends Component{

    componentDidMount(){
        const { dispatch } = this.props;
        dispatch({
          type: 'reportEva/ChdBiasedEva',
          payload: ChdBiaseData
        });
    }

    columns = [
      {
        title: '症状',
        dataIndex: 'name'
      },
      {
        title: '人数',
        dataIndex: 'value'
      }
    ];

    handleChange(val){
        const { dispatch } = this.props;
        dispatch({
            type: 'reportEva/ChdBiasedEva',
            payload: {
                url: 'admin/stat/11',
                params: `aid=1&type=${val}&page=1&start=0&limit=25`
            }
        });
    }

    render(){
        const cols = { sales: { tickInterval: 20 } };
        const { reportEva:{ ChdBiasedEva:{ dataList, items } } , loading} = this.props;
        const SelStyle = { margin: '0 auto', width: 150};
        return (
        <div style={{ flex: 1}}>
            <Row>
                <Col>
                    <Card title="儿童偏颇体质与症状分布" bordered={false} style={{height: "780px"}}>  
                        <div className={styles.peopleCenter}>
                            <Row gutter={11} style={SelStyle}>
                                <Col span={11}>
                                    <Select defaultValue="气虚体" style={SelStyle} onChange={(val) =>this.handleChange(val)}>
                                        {SCALE_CHD.map((x,i) =>{
                                            return(
                                                <Option value={x.title} key={x.dataIndex}>{x.title}</Option>
                                            )
                                        })}
                                    </Select>
                                </Col>
                            </Row>
                            <div className={styles.peopleLeft}>   
                                    <Table 
                                    loading={loading}
                                    columns={this.columns} 
                                    dataSource={dataList} 
                                    size="small"
                                    bordered
                                    pagination={false}/>
                            </div>
                            <div className={styles.peopleRight}>
                                <Chart height={500} 
                                data={items} 
                                padding={[ 50, 0, 80, 45 ]}
                                scale={cols} 
                                forceFit>
                                    <Axis name="name" />    
                                    <Axis name="value" />
                                    <Tooltip
                                        crosshairs={{
                                        type: "y"
                                        }}
                                    />
                                    <Geom type="interval" position="name*value">
                                        <Label content={["name*value", (name, value)=>{
                                            return `${value}`;
                                        }]}
                                        />
                                    </Geom>
                                </Chart>
                            </div>
                        </div>
                    </Card>                     
                </Col>
            </Row>
        </div>
        )
    }
} 