import { connect } from 'dva';
import React, { Component } from 'react';
import { CommonInterface } from '@/services/params';
import { SCALE_DISEASE }  from '@/constants';
import ShareReport from '../ShareReport';

const { DiseaseEvaData } = CommonInterface;

@connect(({reportEva, loading }) => ({
    reportEva,
    loading: loading.effects['reportEva/DiseaseReportEva']
}))

export default class DiseaseReport extends Component{

    componentDidMount(){
        const { dispatch } = this.props;
        dispatch({
            type: 'reportEva/DiseaseReportEva',
            payload: DiseaseEvaData
        });
    }

    render(){
        const { reportEva:{ DiseaseReportEva:{ dataList, items } } , loading} = this.props;
        return(
                <ShareReport
                title="居民常见疾病分布"
                dataList={dataList}
                columns={SCALE_DISEASE}
                items={items}
                loading={loading}
                scalestyle={{padding: '20px' , margin: '0 auto', width:  1000}}
                />
                 
        )
    }
}
