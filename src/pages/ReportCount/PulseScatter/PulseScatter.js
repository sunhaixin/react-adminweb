import { connect } from 'dva';
import React, { Component } from 'react';
import ShareScatter from '../ShareScatter';
import { CommonInterface } from '@/services/params';
import { SCALE_PuLsesLeft, SCALE_PuLseRight }  from '@/constants';

const { pulseLeftData, pulseRightData } = CommonInterface;

@connect(({reportScatter, loading }) => ({
    reportScatter,
    loading1: loading.effects['reportScatter/pulse12dataLeft'],
    loading2: loading.effects['reportScatter/pulse12dataRight']
}))

export default class PulseScatter extends Component{

    componentDidMount(){
        const { dispatch } = this.props;
        dispatch({
            type: 'reportScatter/pulse12dataLeft',
            payload: pulseLeftData
        });
        dispatch({
            type: 'reportScatter/pulse12dataRight',
            payload: pulseRightData
        });
    }

    render(){
        const { 
            reportScatter:{ pulseLeft, pulseRight} , 
            loading1, loading2
        } = this.props;

        return(
            <ShareScatter
            title="居民常见脉象分布Top7"
            rowstyle={16}
            colstyle={12}
            colnumber="2"
            coltitle1="居民左手脉象前七分布"
            coltitle2="居民右手脉象前七分布"
            loading1={loading1}
            loading2={loading2}
            columns1={SCALE_PuLsesLeft}
            columns2={SCALE_PuLseRight}
            dataSource1={pulseLeft.dataList}
            dataSource2={pulseRight.dataList}
            data1={pulseLeft.items}
            data2={pulseRight.items}
            />
        )
    }
}