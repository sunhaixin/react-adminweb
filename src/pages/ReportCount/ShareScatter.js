import React, { Component } from 'react';
import { Card, Row, Col, Table} from 'antd';
import { Chart, Geom, Axis, Tooltip, Label } from "bizcharts";

export default class ShareScatter extends Component{
    render(){      
        const { 
        data1, data2, data3, data4,
        title, rowstyle, colstyle, colnumber, 
        coltitle1, coltitle2, coltitle3, coltitle4, 
        loading1, loading2, loading3, loading4,
        columns1, columns2, columns3, columns4, 
        dataSource1, dataSource2, dataSource3, dataSource4
        } = this.props;      
        const cols = { sales: { tickInterval: 20 } };
        return(
            <div style={{ flex: 1}}>
                 <Row>
                    <Col>
                        <Card title={title} bordered={false} style={{height: "780px"}}>
                            <div style={{ padding: '20px' }}>
                                <Row gutter={rowstyle}>
                                {colnumber == 2? 
                                    <div>  
                                        <Col span={colstyle}>
                                            <Card><h3>{coltitle1}</h3></Card>
                                            <Table 
                                            loading={loading1}
                                            columns={columns1} 
                                            dataSource={dataSource1} 
                                            size="small"
                                            bordered
                                            pagination={false}/>
                                            <Chart height={400} data={data1} 
                                            padding={[ 50, 20, 100, 45 ]}
                                            scale={cols} forceFit>
                                                <Axis name="name" />    
                                                <Axis name="value" />
                                                <Tooltip
                                                    crosshairs={{
                                                    type: "y"
                                                    }}
                                                />
                                                <Geom type="interval" position="name*value">
                                                    <Label content={["name*value", (name, value)=>{
                                                        return `${value}`;
                                                    }]}
                                                    />
                                                </Geom>
                                            </Chart>
                                        </Col>
                                        <Col span={colstyle}>
                                            <Card><h3>{coltitle2}</h3></Card>
                                            <Table 
                                            loading={loading2}
                                            columns={columns2} 
                                            dataSource={dataSource2} 
                                            size="small"
                                            bordered
                                            pagination={false}/>
                                            <Chart height={400} data={data2} 
                                            padding={[ 50, 20, 100, 45 ]}
                                            scale={cols} forceFit>
                                                <Axis name="name" />    
                                                <Axis name="value" />
                                                <Tooltip
                                                    crosshairs={{
                                                    type: "y"
                                                    }}
                                                />
                                                <Geom type="interval" position="name*value">
                                                    <Label content={["name*value", (name, value)=>{
                                                        return `${value}`;
                                                    }]}
                                                    />
                                                </Geom>
                                            </Chart>
                                        </Col>
                                    </div>
                                    :(colnumber == 3?
                                    <div>  
                                        <Col span={colstyle}>
                                            <Card><h3>{coltitle1}</h3></Card>
                                            <Table 
                                            loading={loading1}
                                            columns={columns1} 
                                            dataSource={dataSource1} 
                                            size="small"
                                            bordered
                                            pagination={false}/>
                                            <Chart height={400} data={data1} 
                                            padding={[ 50, 20, 100, 45 ]}
                                            scale={cols} forceFit>
                                                <Axis name="name" />    
                                                <Axis name="value" />
                                                <Tooltip
                                                    crosshairs={{
                                                    type: "y"
                                                    }}
                                                />
                                                <Geom type="interval" position="name*value">
                                                    <Label content={["name*value", (name, value)=>{
                                                        return `${value}`;
                                                    }]}
                                                    />
                                                </Geom>
                                            </Chart>
                                        </Col>
                                        <Col span={colstyle}>
                                            <Card><h3>{coltitle2}</h3></Card>
                                            <Table 
                                            loading={loading2}
                                            columns={columns2} 
                                            dataSource={dataSource2} 
                                            size="small"
                                            bordered
                                            pagination={false}/>
                                            <Chart height={400} data={data2} 
                                            padding={[ 50, 20, 100, 45 ]}
                                            scale={cols} forceFit>
                                                <Axis name="name" />    
                                                <Axis name="value" />
                                                <Tooltip
                                                    crosshairs={{
                                                    type: "y"
                                                    }}
                                                />
                                                <Geom type="interval" position="name*value">
                                                    <Label content={["name*value", (name, value)=>{
                                                        return `${value}`;
                                                    }]}
                                                    />
                                                </Geom>
                                            </Chart>
                                        </Col>
                                        <Col span={colstyle}>
                                            <Card><h3>{coltitle3}</h3></Card>
                                            <Table 
                                            loading={loading3}
                                            columns={columns3} 
                                            dataSource={dataSource3} 
                                            size="small"
                                            bordered
                                            pagination={false}/>
                                            <Chart height={400} data={data3} 
                                            padding={[ 50, 20, 100, 45 ]}
                                            scale={cols} forceFit>
                                                <Axis name="name" />    
                                                <Axis name="value" />
                                                <Tooltip
                                                    crosshairs={{
                                                    type: "y"
                                                    }}
                                                />
                                                <Geom type="interval" position="name*value">
                                                    <Label content={["name*value", (name, value)=>{
                                                        return `${value}`;
                                                    }]}
                                                    />
                                                </Geom>
                                            </Chart>
                                        </Col>
                                    </div>  
                                    :(colnumber == 4?
                                    <div>  
                                        <Col span={colstyle}>
                                            <Card><h3>{coltitle1}</h3></Card>
                                            <Table 
                                            loading={loading1}
                                            columns={columns1} 
                                            dataSource={dataSource1} 
                                            size="small"
                                            bordered
                                            pagination={false}/>
                                            <Chart height={400} data={data1} 
                                            padding={[ 50, 20, 100, 45 ]}
                                            scale={cols} forceFit>
                                                <Axis name="name" />    
                                                <Axis name="value" />
                                                <Tooltip
                                                    crosshairs={{
                                                    type: "y"
                                                    }}
                                                />
                                                <Geom type="interval" position="name*value">
                                                    <Label content={["name*value", (name, value)=>{
                                                        return `${value}`;
                                                    }]}
                                                    />
                                                </Geom>
                                            </Chart>
                                        </Col>
                                        <Col span={colstyle}>
                                            <Card><h3>{coltitle2}</h3></Card>
                                            <Table 
                                            loading={loading2}
                                            columns={columns2} 
                                            dataSource={dataSource2} 
                                            size="small"
                                            bordered
                                            pagination={false}/>
                                            <Chart height={400} data={data2} 
                                            padding={[ 50, 20, 100, 45 ]}
                                            scale={cols} forceFit>
                                                <Axis name="name" />    
                                                <Axis name="value" />
                                                <Tooltip
                                                    crosshairs={{
                                                    type: "y"
                                                    }}
                                                />
                                                <Geom type="interval" position="name*value">
                                                    <Label content={["name*value", (name, value)=>{
                                                        return `${value}`;
                                                    }]}
                                                    />
                                                </Geom>
                                            </Chart>
                                        </Col>
                                        <Col span={colstyle}>
                                            <Card><h3>{coltitle3}</h3></Card>
                                            <Table 
                                            loading={loading3}
                                            columns={columns3} 
                                            dataSource={dataSource3} 
                                            size="small"
                                            bordered
                                            pagination={false}/>
                                            <Chart height={400} data={data3} 
                                            padding={[ 50, 20, 100, 45 ]}
                                            scale={cols} forceFit>
                                                <Axis name="name" />    
                                                <Axis name="value" />
                                                <Tooltip
                                                    crosshairs={{
                                                    type: "y"
                                                    }}
                                                />
                                                <Geom type="interval" position="name*value">
                                                    <Label content={["name*value", (name, value)=>{
                                                        return `${value}`;
                                                    }]}
                                                    />
                                                </Geom>
                                            </Chart>
                                        </Col>
                                        <Col span={colstyle}>
                                            <Card><h3>{coltitle4}</h3></Card>
                                            <Table 
                                            loading={loading4}
                                            columns={columns4} 
                                            dataSource={dataSource4} 
                                            size="small"
                                            bordered
                                            pagination={false}/>
                                            <Chart height={400} data={data4} 
                                            padding={[ 50, 20, 100, 45 ]}
                                            scale={cols} forceFit>
                                                <Axis name="name" />    
                                                <Axis name="value" />
                                                <Tooltip
                                                    crosshairs={{
                                                    type: "y"
                                                    }}
                                                />
                                                <Geom type="interval" position="name*value">
                                                    <Label content={["name*value", (name, value)=>{
                                                        return `${value}`;
                                                    }]}
                                                    />
                                                </Geom>
                                            </Chart>
                                        </Col>
                                    </div>  
                                    : ""
                                    )
                                )}
                                </Row>
                            </div>
                        </Card>
                    </Col>
                </Row>
            </div>
        )
    }
}
