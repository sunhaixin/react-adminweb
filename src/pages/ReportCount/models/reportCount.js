import * as services  from '@/services/reportCount';
import { CommonInterface } from '@/services/params';

export default {
  namespace: 'reportCount',

  state: {
    Stat1List: [],
    Stat2List: {},
    StatNo1List: [],
    FaceSeList: {},
    FaceBuList: {},
    TongueSeList: {}
  },

  effects: {
    *WorkloadList({ payload:{ url, params,  url_, params_ } }, { call, put }){
        const res = yield call(services.WorkloadList, url, params,  url_, params_);
        yield put({ type: 'save', payload: { Stat1List: res.dataList_, StatNo1List: res.dataList} });
    },
    *RegionList({ payload:{  url, params } }, { call, put }){
      const res = yield call(services.RegionList, url, params);
      yield put({ type: 'save', payload: { Stat2List: res } });
    },
    *FaceSeList({ payload: { url, params }}, { call, put }){
      const res = yield call(services.ScatterList, url, params);
      yield put({ type: 'save', payload: { FaceSeList: res } });
    },
    *FaceBuList({ payload: { url, params }}, { call, put }){
      const res = yield call(services.ScatterList, url, params);
      yield put({ type: 'save', payload: { FaceBuList: res } });
    },
    *TongueSeList({ payload: { url, params }}, { call, put }){
      const res = yield call(services.ScatterList, url, params);
      yield put({ type: 'save', payload: { TongueSeList: res } });
    }
  },

  subscriptions:{
    setup({dispatch,history}){
        return history.listen(({pathname, ...rest})=>{
            if(pathname == "/reportCount/workloadcount"){
                const { Stat1Data, StatNo1Data } = CommonInterface;
                dispatch({
                    type: 'WorkloadList',
                    payload: {
                        url: Stat1Data.url,
                        params: Stat1Data.params,
                        url_: StatNo1Data.url_,
                        params_: StatNo1Data.params_
                    }
                });
            }
        })
    }
  },

  reducers: {
    save(state, { payload }) {
      return {
        ...state,
        ...payload,
      };
    }
  },
};
