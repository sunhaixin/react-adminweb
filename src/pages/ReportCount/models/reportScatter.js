import * as services  from '@/services/reportCount';

export default {
  namespace: 'reportScatter',

  state: {
    tongue13data:{},
    taise13data:{},
    taizhi13data:{},
    shexing13data:{},
    pulseLeft:{},
    pulseRight:{}
  },

  effects: {
    *tongue13data({ payload: { url, params }}, { call, put }){
      const res = yield call(services.ScatterList, url, params);
      yield put({ type: 'save', payload: { tongue13data: res } });
    },
    *taise13data({ payload: { url, params }}, { call, put }){
        const res = yield call(services.ScatterList, url, params);
        yield put({ type: 'save', payload: { taise13data: res } });
    },
    *taizhi13data({ payload: { url, params }}, { call, put }){
        const res = yield call(services.ScatterList, url, params);
        yield put({ type: 'save', payload: { taizhi13data: res } });
    },
    *shexing13data({ payload: { url, params }}, { call, put }){
        const res = yield call(services.ScatterList, url, params);
        yield put({ type: 'save', payload: { shexing13data: res } });
    },
    *pulse12dataLeft({ payload: { url, params }}, { call, put }){
      const res = yield call(services.ScatterList, url, params);
      yield put({ type: 'save', payload: { pulseLeft: res } });
    },
    *pulse12dataRight({ payload: { url, params }}, { call, put }){
      const res = yield call(services.ScatterList, url, params);
      yield put({ type: 'save', payload: { pulseRight: res } });
    }
  },

  reducers: {
    save(state, { payload }) {
      return {
        ...state,
        ...payload,
      };
    }
  },
};
