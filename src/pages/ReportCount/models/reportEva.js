import * as services  from '@/services/reportCount';
import { CommonInterface } from '@/services/params';

export default {
  namespace: 'reportEva',

  state: {
    ScaleReportEva:{},
    TcmReportEva:{},
    PreReportEva:{},
    PartumReportEva:{},
    HypReportEva:{},
    DiaReportEva:{},
    DiseaseReportEva:{},
    MedicineReportEva:{},
    ChdReportEva:{},
    ChdBiasedEva:{},
    PhyReportEva:{},
    ScaleTitle:""
  },

  effects: {
    *ScaleReportEva({ payload: { url, params }}, { call, put }){
      const res = yield call(services.ScatterList, url, params);
      if(url=="admin/stat/4"){
         yield put({ type: 'save', payload: { ScaleTitle: "老年人体质类型分布" } });
      }else if(url=="admin/stat/3"){
        yield put({ type: 'save', payload: { ScaleTitle: "成年人9体质类型分布" } });
      }
      yield put({ type: 'save', payload: { ScaleReportEva: res } });
    },
    *TcmReportEva({ payload: { url, params }}, { call, put }){
      const res = yield call(services.ScatterList, url, params);
      yield put({ type: 'save', payload: { TcmReportEva: res } });
    },
    *PreReportEva({ payload: { url, params }}, { call, put }){
      const res = yield call(services.ScatterList, url, params);
      yield put({ type: 'save', payload: { PreReportEva: res } });
    },
    *PartumReportEva({ payload: { url, params }}, { call, put }){
      const res = yield call(services.ScatterList, url, params);
      yield put({ type: 'save', payload: { PartumReportEva: res } });
    },
    *HypReportEva({ payload: { url, params }}, { call, put }){
      const res = yield call(services.ScatterList, url, params);
      yield put({ type: 'save', payload: { HypReportEva: res } });
    },
    *DiaReportEva({ payload: { url, params }}, { call, put }){
      const res = yield call(services.ScatterList, url, params);
      yield put({ type: 'save', payload: { DiaReportEva: res } });
    },
    *DiseaseReportEva({ payload: { url, params }}, { call, put }){
      const res = yield call(services.ScatterList, url, params);
      yield put({ type: 'save', payload: { DiseaseReportEva: res } });
    },
    *MedicineReportEva({ payload: { url, params }}, { call, put }){
      const res = yield call(services.ScatterList, url, params);
      yield put({ type: 'save', payload: { MedicineReportEva: res } });
    },
    *ChdReportEva({ payload: { url, params }}, { call, put }){
      const res = yield call(services.ScatterList, url, params);
      yield put({ type: 'save', payload: { ChdReportEva: res } });
    },
    *PhyReportEva({ payload: { url, params }}, { call, put }){
      const res = yield call(services.PhyReportList, url, params);
      yield put({ type: 'save', payload: { PhyReportEva: res } });
    },
    *ChdBiasedEva({ payload: { url, params }}, { call, put }){
      const res = yield call(services.ChdBiasedList, url, params);
      yield put({ type: 'save', payload: { ChdBiasedEva: res } });
    },
  },

  subscriptions:{
    setup({dispatch,history}){
        return history.listen(({pathname, location, ...rest})=>{
            const { ScaleOldData, ScaleNineData } = CommonInterface;
            if(pathname == "/reportCount/ScaleOld"){
                dispatch({
                  type: 'ScaleReportEva',
                  payload:  ScaleOldData
                });
            }else if(pathname == "/reportCount/ScaleNine"){
              dispatch({
                type: 'ScaleReportEva',
                payload:  ScaleNineData
              });
            }
        })
    }
  },

  reducers: {
    save(state, { payload }) {
      return {
        ...state,
        ...payload,
      };
    }
  },
};
