import { connect } from 'dva';
import React, { Component } from 'react';
import ShareScatter from '../ShareScatter';
import { CommonInterface } from '@/services/params';
import { SCALE_Face, SCALE_Face1, SCALE_Face2 }  from '@/constants';

const { FaceSeData_, FaceBuData_, TongueSeData_ } = CommonInterface;

@connect(({reportCount, loading }) => ({
    reportCount,
    loadingFaceSe: loading.effects['reportCount/FaceSeList'],
    loadingFaceBu: loading.effects['reportCount/FaceBuList'],
    loadingTongueSe: loading.effects['reportCount/TongueSeList']
}))

export default class FaceScatter extends Component{

    componentDidMount(){
        const { dispatch } = this.props;
        dispatch({
            type: 'reportCount/FaceSeList',
            payload: FaceSeData_
        });
        dispatch({
            type: 'reportCount/FaceBuList',
            payload: FaceBuData_
        });
        dispatch({
            type: 'reportCount/TongueSeList',
            payload: TongueSeData_
        });
    }

    render(){
        const { 
            reportCount:{ FaceSeList, FaceBuList, TongueSeList } , 
            loadingFaceSe,  loadingFaceBu, loadingTongueSe
        } = this.props;

        return(
            <ShareScatter
            title="居民面色分布"
            rowstyle={16}
            colstyle={8}
            colnumber="3"
            coltitle1="面色（包含局部特征）排列前五指标"
            coltitle2="面部光泽"
            coltitle3="唇色"
            loading1={loadingFaceSe}
            loading2={loadingFaceBu}
            loading3={loadingTongueSe}
            columns1={SCALE_Face}
            columns2={SCALE_Face1}
            columns3={SCALE_Face2}
            dataSource1={FaceSeList.dataList}
            dataSource2={FaceBuList.dataList}
            dataSource3={TongueSeList.dataList}
            data1={FaceSeList.items}
            data2={FaceBuList.items}
            data3={TongueSeList.items}
            />
        )
    }
}
