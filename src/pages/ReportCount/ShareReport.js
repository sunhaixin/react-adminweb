import React, { Component } from 'react';
import { Card, Row, Col, Table} from 'antd';
import { Chart, Geom, Axis, Tooltip, Label } from "bizcharts";

export default class ShareReport extends Component{
    render(){
        const { title, dataList, columns, items, scalestyle, loading } = this.props;        
        const cols = { sales: { tickInterval: 20 } };
        return(
            <div style={{ flex: 1}}>
                 <Row>
                    <Col>
                        <Card title={title} bordered={false} style={{height: "780px"}}>
                            <div style={scalestyle}>
                                <Row >
                                    <Col>
                                         <Table 
                                        loading={loading}
                                        columns={columns} 
                                        dataSource={dataList} 
                                        size="small"
                                        bordered
                                        pagination={false}/>
                                        <Chart height={500} data={items} 
                                        padding={[ 50, 20, 100, 45 ]}
                                        scale={cols} forceFit>
                                            <Axis name="name" />    
                                            <Axis name="value" />
                                            <Tooltip
                                                crosshairs={{
                                                type: "y"
                                                }}
                                            />
                                            <Geom type="interval" position="name*value">
                                                <Label content={["name*value", (name, value)=>{
                                                    return `${value}`;
                                                }]}
                                                />
                                            </Geom>
                                        </Chart>
                                    </Col>
                                </Row>
                            </div>
                        </Card>
                    </Col>
                </Row>
            </div>
        )
    }
}
