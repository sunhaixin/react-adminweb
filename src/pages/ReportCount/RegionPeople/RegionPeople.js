import { connect } from 'dva';
import DataSet from "@antv/data-set";
import React, { Component } from 'react';
import styles from './RegionPeople.less';
import { Card, Row, Col, Table} from 'antd';
import { CommonInterface } from '@/services/params';
import { Chart, Geom, Axis, Tooltip, Label, Legend } from "bizcharts";

const { RegionData } = CommonInterface;

@connect(({reportCount, loading }) => ({
    reportCount,
    loading: loading.effects['reportCount/RegionList']
}))

export default class RegionPeople extends Component{

    componentDidMount(){
        const { dispatch } = this.props;
        dispatch({
          type: 'reportCount/RegionList',
          payload: RegionData
        });
    }

    columns = [
      {
        title: '年龄组段',
        dataIndex: 'name'
      },
      {
        title: '男',
        dataIndex: 'value1'
      },
      {
        title: '女',
        dataIndex: 'value2'
      },
      {
        title: '合计',
        dataIndex: 'value3'
      }
    ];

    render(){
        const { reportCount:{ Stat2List } , loading } = this.props;
        const { dataList, RegionCount, AgeGroup } = Stat2List;
        const data = [
            {
              name: "男",
              "Jan.": 18.9,
              "Jan1": 18.9,

            },
            {
              name: "女",
              "Jan.": 12.4,
              "Jan1": 12.4,

            }
          ];
          const ds = new DataSet();
          const dv = ds.createView().source(RegionCount?RegionCount:data);
          dv.transform({
            type: "fold",
            fields: AgeGroup? AgeGroup :["Jan.", "Jan1"],
            key: "年龄组段",
            value: "人次" 
          });
        return (
        <div style={{ flex: 1}}>
            <Row>
                <Col>
                    <Card title="地区常住人口年龄分布" bordered={false} style={{height: "780px"}}>  
                        <div className={styles.peopleCenter}>
                            <div className={styles.peopleLeft}>   
                                    <Table 
                                    loading={loading}
                                    columns={this.columns} 
                                    dataSource={dataList} 
                                    size="small"
                                    bordered
                                    pagination={false}/>
                            </div>
                            <div className={styles.peopleRight}>
                                <Chart
                                padding={[ 50, 0, 80, 45 ]}
                                height={500} data={dv} forceFit>
                                    <Axis name="年龄组段" />
                                    <Axis name="人次" />
                                    <Legend/>
                                    <Tooltip
                                        crosshairs={{
                                        type: "y"
                                        }}
                                    />
                                    <Geom
                                        type="interval"
                                        position="年龄组段*人次"
                                        color={"name"}
                                        adjust={[
                                        {
                                            type: "dodge",
                                            marginRatio: 1 / 32
                                        }
                                        ]}
                                    >
                                        <Label 
                                        textStyle={{fontSize: 12}}
                                        content={["年龄组段*人次", (name, value)=>{
                                            return `${value}`;
                                        }]}
                                        />
                                    </Geom>
                                </Chart>
                            </div>
                        </div>
                    </Card>                     
                </Col>
            </Row>
        </div>
        )
    }
} 