import { connect } from 'dva';
import DataSet from "@antv/data-set";
import React, { Component } from 'react';
import styles from './WorkloadCount.less';
import { CommonInterface } from '@/services/params';
import { Card, Row, Col, DatePicker, Table, Divider, Tag } from 'antd';
import { Chart, Geom, Axis, Tooltip, Label, Legend, Coord } from "bizcharts";

const { Stat1Data, StatNo1Data } = CommonInterface;

@connect(({reportCount, loading }) => ({
    reportCount,
    loading: loading.models.reportCount,
}))

export default class WorkloadCount extends Component {

    onChangeStart = (date, dateString) =>{
        const { dispatch } = this.props;
        this.setState({ startValue: dateString});
        let fixed = `aid=1&page=1&start=0&limit=1000&starttime=${dateString}`;
        const { endValue } = this.state;
        let WendValue = '';
        if (endValue && endValue != ''){
            WendValue = `&endtime=${endValue}`;
        }
        dispatch({
            type: 'reportCount/WorkloadList',
            payload: {
                url: Stat1Data.url,
                params: `${fixed}${WendValue}`,
                url_: StatNo1Data.url_,
                params_: `${fixed}${WendValue}`,
            }
        });
    }

    onChangeEnd = (date, dateString) =>{
        const { dispatch } = this.props;
        this.setState({ endValue: dateString});
        let fixed = `aid=1&page=1&start=0&limit=1000&endtime=${dateString}`;
        const { startValue } = this.state;
        let WstartValue = '';
        if (startValue && startValue != ''){
            WstartValue = `&startValue=${startValue}`;
        }
        dispatch({
            type: 'reportCount/WorkloadList',
            payload: {
                url: Stat1Data.url,
                params: `${fixed}${WstartValue}`,
                url_: StatNo1Data.url_,
                params_: `${fixed}${WstartValue}`,
            }
        });
    }

    state = {
        startValue: null,
        endValue: null,
      };

    columns = [{
        title: '项目',
        dataIndex: 'name',
        width: '20%',
      },{
        title: '工作量(人次)',
        dataIndex: 'value',
        width: '15%',
      }];

    render() {
        const { DataView } = DataSet;
        const { reportCount:{ Stat1List, StatNo1List } , loading } = this.props;
        const dv = new DataView();
        dv.source(StatNo1List).transform({
            type: "percent",
            field: "count",
            dimension: "item",
            as: "percent"
        });
        return(
            <div style={{ flex: 1}}>
                <Row gutter={24}>
                    <Col>
                         <Card title="工作量统计" bordered={false} style={{height: "780px"}}> 
                            <div className={styles.workloadCenter}>
                                起止时间：<DatePicker onChange={this.onChangeStart} />&nbsp;
                                截止时间：<DatePicker onChange={this.onChangeEnd} />
                            </div>
                            <div className={styles.workloadBody}>
                                    <div className={styles.workloadLeft}>
                                         <Table 
                                         loading={loading}
                                         columns={this.columns} 
                                         dataSource={Stat1List} 
                                         pagination={false}/>
                                    </div>
                                   <div className={styles.workloadRight}>
                                       <Chart 
                                            height={500} 
                                            data={dv} 
                                            scale={[{ dataKey: 'percent'}]} 
                                            padding={[0, 120, 80, 20]} forceFit>
                                            <Coord type="theta" radius={0.75} />
                                            <Axis name="percent" />
                                            <Legend 
                                            textStyle={{ fontSize: 15}}
                                            position="right" offsetY={-20} offsetX={-60} />
                                            <Tooltip showTitle={false}/>
                                            <Geom
                                                type="intervalStack"
                                                position="percent"
                                                color="item"
                                                tooltip={[ "item*percent",(item, percent) => {
                                                    return {
                                                         name: item,
                                                         value: Math.floor(percent* 10000) / 100 + "%"
                                                    };
                                                }]}
                                                style={{ lineWidth: 1, stroke: "#fff"}}>
                                                <Label
                                                content="percent"
                                                textStyle={{
                                                    rotate: 0,
                                                    textAlign: "center",
                                                    shadowBlur: 2,
                                                    fontSize: 15,
                                                    shadowColor: "rgba(0, 0, 0, .45)"
                                                }}
                                                formatter={(val, item) => {
                                                    return  item.point.item+ ': '+ Math.floor(val* 10000) / 100 + "%";
                                                }}
                                                />
                                            </Geom>
                                        </Chart>
                                   </div>
                            </div>
                         </Card>
                    </Col>
                </Row>
            </div>
        )
    }
}