import { connect } from 'dva';
import React, { Component } from 'react';
import ShareScatter from '../ShareScatter';
import { CommonInterface } from '@/services/params';
import { SCALE_Tongue, SCALE_Tongue1, SCALE_Tongue2, SCALE_Tongue3 }  from '@/constants';

const { start13Data1, start13Data2, start13Data3, start13Data4 } = CommonInterface;

@connect(({reportScatter, loading }) => ({
    reportScatter,
    loading1: loading.effects['reportScatter/tongue13data'],
    loading2: loading.effects['reportScatter/taise13data'],
    loading3: loading.effects['reportScatter/taizhi13data'],
    loading4: loading.effects['reportScatter/shexing13data']
}))

export default class TongueScatter extends Component{

    componentDidMount(){
        const { dispatch } = this.props;
        dispatch({
            type: 'reportScatter/tongue13data',
            payload: start13Data1
        });
        dispatch({
            type: 'reportScatter/taise13data',
            payload: start13Data2
        });
        dispatch({
            type: 'reportScatter/taizhi13data',
            payload: start13Data3
        });
        dispatch({
            type: 'reportScatter/shexing13data',
            payload: start13Data4
        });
    }

    render(){
        const { 
            reportScatter:{ tongue13data, taise13data, taizhi13data, shexing13data} , 
            loading1, loading2, loading3, loading4
        } = this.props;
    
        return(
            <ShareScatter
            title="居民舌象分布"
            rowstyle={10}
            colstyle={6}
            colnumber="4"
            coltitle1="舌色（包括舌色局部特征）"
            coltitle2="苔色"
            coltitle3="苔质"
            coltitle4="舌形"
            loading1={loading1}
            loading2={loading2}
            loading3={loading3}
            loading4={loading4}
            columns1={SCALE_Tongue}
            columns2={SCALE_Tongue1}
            columns3={SCALE_Tongue2}
            columns4={SCALE_Tongue3}
            dataSource1={tongue13data.dataList}
            dataSource2={taise13data.dataList}
            dataSource3={taizhi13data.dataList}
            dataSource4={shexing13data.dataList}
            data1={tongue13data.items}
            data2={taise13data.items}
            data3={taizhi13data.items}
            data4={shexing13data.items}
            />
        )
    }
}