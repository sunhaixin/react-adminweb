import { axiosGET, axiosPOST } from '@/utils/admin_axios';

function TagsDoctor(items) {
    let dataList = [];
    items.forEach((i, e) => {
       dataList.push({
          key:  i.DepartmentUid,
          HospitalName: i.HospitalName,
          DepartmentName: i.Department,
          Name: i.Name,
          Id: i.Id,
          Mobile: i.Mobile,
          Expert: i.Expert,
          Email: i.Email,
          Gender: i.Gender == 1? '男': '女',
          Comment: i.Comment,
          IsSSO: i.IsSSO,
          Jurisdiction: i.Jurisdiction,
          Title: i.Title
       })
    })
    const result = {
      list:  dataList,
      pagination: {
        total:  dataList.length,
        pageSize: 10,
        current:  1,
      },
    };
    return result
  }

export async function fetchDoctorList(url, params){
    let items = await axiosGET(url,params);
    let result = await TagsDoctor(items)
    return result;
}

export async function fetchDoctorAdd(Comment, Department, DepartmentUid, Email, Expert, Gender, HospitalId, IsSSO, Jurisdiction, Mobile, Name, Title) {
  let name = "admin/doctor/add";
  let params = { Comment, Department, DepartmentUid, Email, Expert, Gender, HospitalId, IsSSO, Jurisdiction, Mobile, Name, Title};
  return await axiosPOST(name, params);
}

export async function fetchDoctorUpdate(Comment, Department, DepartmentUid, Email, Expert, Gender, HospitalId, IsSSO, Jurisdiction, Mobile, Name, Title, Id) {
  let name = "admin/doctor/update";
  let params = { data:{ Comment, Department, DepartmentUid, Email, Expert, Gender, HospitalId, IsSSO, Jurisdiction, Mobile, Name, Title, Id }, token: "123456"};
  return await axiosPOST(name, params);
}

export async function fetchDoctorDelete(Id) {
  let name = "admin/doctor/delete";
  let params = { data:{ Id }, token:"123456"};
  return await axiosPOST(name, params);
}

export async function fetchDoctorPswd(Id, Pswd) {
  let name = "admin/doctor/pswd";
  let params = { data:{ Id, Pswd }, token:"123456"};
  return await axiosPOST(name, params);
}