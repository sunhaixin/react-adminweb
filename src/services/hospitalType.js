import { axiosGET, axiosPOST } from '@/utils/admin_axios';

function HandleType(items) {
    let dataList = [];
    items.forEach(i => {
      dataList.push({
          key: i.Id,
          typeName: i.Name,
          CreateTime: i.CreateTime
        })
      });
      const result = {
        list:  dataList,
        pagination: {
          total:  dataList.length,
          pageSize: 10,
          current:  1,
        },
      };
  
      return result;
}

function AdminType(items) {
    let dataList = [];
    items.forEach(i => {
      dataList.push({
          key: i.Id,
          typeName: i.Name,
          Code: i.Code,
        })
      });
      const result = {
        list:  dataList,
        pagination: {
          total:  dataList.length,
          pageSize: 10,
          current:  1,
        },
      };
  
      return result;
}

function EvaList(items) {
  let dataList = [];
    items.forEach(i => {
      dataList.push({
          key: i.Id,
          Id: i.Id,
          hospitalName: i.Name,
          Remainder: i.Remainder,
          ServiesCount: i.ServiesCount,
          ServiesEndTime: i.ServiesEndTime,
        })
      });
      const result = {
        list:  dataList,
        pagination: {
          total:  dataList.length,
          pageSize: 10,
          current:  1,
        },
      };
  
      return result;
}

function ServiesItemList(items) {
  let dataList = [];
    items.forEach(i => {
      dataList.push({
          key: i.Id,
          Id: i.Id,
          Name: i.Name,
          Eva: i.Eva,
        })
      });
      const result = {
        list:  dataList,
        pagination: {
          total:  dataList.length,
          pageSize: 10,
          current:  1,
        },
      };
  
      return result;
}

export async function typeList(url, params){
    let items = await axiosGET(url,params) ;
    let result = await HandleType(items)
    return result;
}

export async function typeAdd(Name){
    let name = "admin/hospitaltype/add";
    let params = { Name };
    return await axiosPOST(name, params);
}

export async function typeUpdate(Id, Name){
    let name = "admin/hospitaltype/update";
    let params = { Id, Name };
    return await axiosPOST(name, params);
}

export async function fetchAdminType(url, params) {
    let items = await axiosGET(url,params);
    let result = await AdminType(items);
    return result;
 }

export async function PcWebEvaList(url, params) {
  let items = await axiosGET(url,params);
  let result = await EvaList(items);
  return result;
}

export async function ServiesItem(url, params) {
  let items = await axiosGET(url,params);
  let result = await ServiesItemList(items);
  return result;
}

export async function ServiesItemAdd(eva, name) {
  let url = "admin/serviesItem/add";
  let params = { eva, name };
  return await axiosPOST(url, params);
}

export async function RolepermissionAdd(code, name) {
  let url = "admin/accounttype/add";
  let params = { token: 123456, data:{ code, name }};
  return await axiosPOST(url, params);
}