import { axiosGET, axiosPOST } from '@/utils/admin_axios';
import { apiBaseUrl } from '@/utils/admin_config';
import axios from 'axios';

function CountData(arr, arr_){
    let dataList = [], dataList_ = [], Data = [];
    arr.forEach((x, i) =>{
        dataList_.push({
            key: i,
            name: x.name,
            value: x.value
        })
        if(x.name == "总计"){
            Data.push(x.value)
        }
    })
    arr_.forEach((i,e) => {
        dataList.push({
            item: i.name,
            count: (Math.round(i.value / Data[0] * 10000) / 100.00)
         })
    })
 
    return { dataList, dataList_ }
 }

function TagsRegion(items, items_) {
    let dataList = [], RegionCount = [], AgeGroup = [];
    var boyVal = {name:"男"}, girlVal= {name:"女"};
    items.forEach((x, i) =>{
        dataList.push({
            key: i,
            name: x.name,
            value1: x.value1,
            value2: x.value2,
            value3: x.value3
        })
    })
    items_.forEach((y,i) =>{
        boyVal[y.name] = y.value1;
        girlVal[y.name] =  y.value2;
        AgeGroup.push(y.name);
    })
    RegionCount.push(boyVal, girlVal);

    return { dataList, RegionCount, AgeGroup}
}

function TagsPhy(items, items_) {
    let dataList = [], RegionCount = [], AgeGroup = [];
    var boyVal = {name:"高分人群"}, girlVal= {name:"低分人群"};
    items.forEach((x, i) =>{
        dataList.push({
            key: i,
            name: x.name,
            value1: x.value1,
            value2: x.value2
        })
    })
    items_.forEach((y,i) =>{
        boyVal[y.name] = y.value1;
        girlVal[y.name] =  y.value2;
        AgeGroup.push(y.name);
    })
    RegionCount.push(boyVal, girlVal);

    return { dataList, RegionCount, AgeGroup}
}

function TagsChdBiased(items) {
    let dataList = [];
    items? items.forEach((x, i) =>{
        dataList.push({
            key: i,
            name: x.name,
            value: x.value
        })
    }): dataList=[]
    return { dataList, items }
}

function TagsScatter(items) {
    let arr1 = [], arr2 = [];
    let obj1 = {key: 0}, obj2 = {key: 1};
    let dataList = [], dataList_= [];
    items.forEach((x, i) =>{
        arr1.push(x.value);
        arr2.push(x.percent);
        arr2[0]? arr2: arr2= []; 
        arr1.forEach((y, z) =>{
            obj1[z] = arr1[z];   
        })
        arr2.forEach((z, s) =>{
            obj2[s] = arr2[s];   
        })
    })
    dataList.push(obj1);
    dataList_.push(obj1,obj2);
    return { dataList, dataList_, items }
}

export async function WorkloadList(url, params, url_, params_){
    let res =  await axios.get(`${apiBaseUrl}${url}?${params}`) ;
    let res_  = await axios.get(`${apiBaseUrl}${url_}?${params_}`) ;
    if(res.data.IsSuccess && res_.data.IsSuccess){
        let arr = res.data.Data;
        let arr_ = res_.data.Data;
        return await CountData(arr, arr_);
    }else{
        message.error('失败')
    }
}

export async function RegionList(url, params) {
    let items = await axiosGET(url,params);
    let url_ = "admin/stat/2";
    let params_ = "aid=1&page=1&start=0&limit=1000";
    let items_ = await axiosGET(url_,params_); 
    let result = await TagsRegion(items, items_)
    return result;
}

export async function PhyReportList(url, params) {
    let items = await axiosGET(url,params);
    let url_ = "admin/stat/7";
    let params_ = "aid=1&page=1&start=0&limit=1000";
    let items_ = await axiosGET(url_,params_); 
    let result = await TagsPhy(items, items_)
    return result;
}

export async function ChdBiasedList(url, params) {
    let items = await axiosGET(url, params);
    let result = await TagsChdBiased(items)
    return result;
}

export async function ScatterList(url, params) {
    let items = await axiosGET(url, params);
    let result = await TagsScatter(items)
    return result;
}