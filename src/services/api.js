import request from '@/utils/request';
import { axiosGET, axiosPOST} from '@/utils/admin_axios';
import { apiBaseUrl } from '@/utils/admin_config';
import axios from 'axios';
import { message } from 'antd';
import { func } from 'prop-types';

export async function fakeAccountLogin(params) {
  return request('/api/login/account', {
    method: 'POST',
    body: params,
  });
}

export async function queryNotices() {
  return request('/api/notices');
}

function TagsData(items){
   let dataList = [];
   items.forEach(i => {
      dataList.push({
         key: i.Id,
         title: i.Name
      })
   })

   return dataList
}

function GroupsData(items) {
  let dataList = [];
  items.forEach((i, e) => {
     dataList.push({
        key:  i.Id,
        GroupId: i.GroupId,
        Id: e,
        Hid: i.HospitalId,
        HospitalName: i.HospitalName,
     })
  })
  const result = {
    list:  dataList,
    pagination: {
      total:  dataList.length,
      pageSize: 10,
      current:  1,
    },
  };

  return result
}

function BeatCase(items) {
  let dataList = [];
  items.forEach((i, e) => {
     let c = i.CreateTime.split("T");
     let create = c[0]+" "+c[1];
     dataList.push({
        key:  i.Id,
        DoctorName: i.DoctorName,
        DoctorId: i.DoctorId,
        PatientName: i.PatientName,
        PatientId: i.PatientId,
        CreateTime: create
     })
  })
  const result = {
    list:  dataList,
    pagination: {
      total:  dataList.length,
      pageSize: 10,
      current:  1,
    },
  };

  return result
}

function BeatTongue(items) {
  let dataList = [];
  items.forEach((i, e) => {
     let c = i.CreateTime.split("T");
     let create = c[0]+" "+c[1];
     let img1 = i.Step1Pic? i.Step1Pic.substring(0,10): "";
     let img2 = i.Step2Pic? i.Step2Pic.substring(0,10): "";
     let img3 = i.ImageOut? i.ImageOut.substring(0,10): "";
     dataList.push({
        key:  i.Id,
        DoctorName: i.DoctorName,
        DoctorId:i.DoctorId,
        PatientName: i.PatientName,
        PatientId:i.PatientId,
        Step1Pic:img1,
        Step2Pic:img2,
        ImageOut:img3,
        Comment:i.Comment,
        CreateTime: create
     })
  })
  const result = {
    list:  dataList,
    pagination: {
      total:  dataList.length,
      pageSize: 10,
      current:  1,
    },
  };

  return result
}

function BeatFace(items) {
  let dataList = [];
  items.forEach((i, e) => {
     let c = i.CreateTime.split("T");
     let create = c[0]+" "+c[1];
     let payload = "http://gyhl-test.oss-cn-hangzhou.aliyuncs.com/";
     let u1 = i.PicUrl.split("http://gyhl-test.oss-cn-hangzhou.aliyuncs.com/")[1];
     let url = i.PicUrl.indexOf(payload) != -1?  u1: i.PicUrl;
     dataList.push({
        key:  i.Id,
        DoctorName: i.DoctorName,
        DoctorId:i.DoctorId,
        PatientName: i.PatientName,
        PatientId:i.PatientId,
        PicUrl:url,
        CreateTime: create
     })
  })
  const result = {
    list:  dataList,
    pagination: {
      total:  dataList.length,
      pageSize: 10,
      current:  1,
    },
  };

  return result
}

export async function TagsConstants(url, params){
  let items = await axiosGET(url,params);
  return TagsData(items)
}

export async function MedicQuery(url, params){
  let items = await axiosGET(url,params);
  return TagsData(items)
}

// 查看医联体成员
export async function LookYilianti(url, params) {
  let res =  await axios.get(`${apiBaseUrl}${url}?${params}`) ;
  if(res.data.IsSuccess){
      if(res.data.Message){
          console.log(res.data.Message)
      }
      return res.data.Children;
  }else{
      message.error('失败')
  }
}

// 添加医联体
export async function AddYilianti(name) {
  let url = "admin/MedicalTreatmentCombination/add";
  let params = { token: 123456, data:{ name }};
  return await axiosPOST(url, params);
}

// 远程医疗list
export async function TelemedicineQuery(url, params) {
  let items = await axiosGET(url,params);
  return TagsData(items)
}

// 远程医疗详情
export async function TelemdicineDetail(url, params) {
  let items = await axiosGET(url,params);
  return GroupsData(items)
}

// 添加远程医疗小组
export async function AddYuanCheng(name) {
  let url = "admin/telemedicine/group/add";
  let params = { token: 123456, data:{ name }};
  return await axiosPOST(url, params);
}

//web测评使用情况
export async function PcWebUseStat(url, params) {
  let res =  await axios.get(`${apiBaseUrl}${url}?${params}`) ;
  if(res.data.IsSuccess){
      if(res.data.Message){
          console.log(res.data.Message)
      }
      return res.data;
  }else{
      message.error('失败')
  }
}

//拍病例
export async function BeatCaseDetail(url, params) {
  let items = await axiosGET(url,params);
  return BeatCase(items)
}

//智能舌象
export async function BeatTongueDetail(url, params) {
  let items = await axiosGET(url,params);
  return BeatTongue(items)
}

//智能面象
export async function BeatFaceDetail(url, params) {
  let items = await axiosGET(url,params);
  return BeatFace(items)
}