import { axiosGET, axiosPOST} from '@/utils/admin_axios';
import { CommonInterface  } from './params';
const { ChartData } = CommonInterface;

function QueryList(items) {
    let dataList = [];
    items.forEach((i, e) => {
       dataList.push({
          key:  e,
          HospitalName: i.HospitalName,
          LoginId: i.LoginId,
          AccountType: i.AccountType,
          AccountId: i.AccountId,
          UserName: i.UserName,
          Memo: i.Memo,
          SysName: i.SysName,
          Id: i.Id
       })
    })
    const result = {
      list:  dataList,
      pagination: {
        total:  dataList.length,
        pageSize: 10,
        current:  1,
      },
    };
  
    return result
}

export async function fetchAdminQuery(url, params){
    let items = await axiosGET(url,params);
    let result = await QueryList(items)
    return result;
}

export async function fetchAdminType(url, params) {
   let items = await axiosGET(url,params);
   return items;
}

export async function fetchAdminAdd(AccountType, AccountTypeCode, HospitalId, LoginId, Memo, Pswd, SysName, UserName) {
   let name = "admin/account/add";
   let params = { data:{ AccountType, AccountTypeCode, HospitalId, LoginId, Memo, Pswd, SysName, UserName}, token: 123456 };
   return await axiosPOST(name, params);
}

export async function fetchAdminDel(Id) {
   let name = "admin/account/delete";
   let params = { data:{ Id }, token: 123456 };
   return await axiosPOST(name, params);
}

export async function fetchAdminUpdate(AccountType, AccountTypeCode, HospitalId, Id, LoginId, Memo, SysName, UserName) {
   let name = "admin/account/update";
   let params = { data:{ AccountType, AccountTypeCode, HospitalId, Id, LoginId, Memo, SysName, UserName }, token: 123456 };
   return await axiosPOST(name, params);
}

export async function fetchAdminPswd(Id, Pswd) {
   let name = "admin/account/pswd";
   let params = { data: { Id, Pswd }, token: 123456 };
   return await axiosPOST(name, params);
}