export const CommonInterface = {
    ChartData:{
        url: 'admin/hospital/query?',
        params: "page=1&start=0&limit=1000"
    },
    ChangeData:[{
        url: 'admin/hospital/query'
    }],
    AddkeShi:[{
        url: 'admin/department/add'
    }],
    PcDatas:{
        url: 'admin/hospital/query',
        params: "query=&aid=1&view=eva&page=1&start=0&limit=1000"
    },
    UpdatekeSi:[{
        url: 'admin/department/update'
    }],
    typeData:{
        url: "admin/hospitaltype/query",
        params: "page=1&start=0&limit=25"
    },
    amdinType:{
        url:  "admin/accounttype/query",
        params: "query=&page=1&start=0&limit=1000"
    },
    provinceData:{
        url: "admin/basic/province",
        params: "query=&page=1&start=0&limit=25"
    },
    WebEvaList:{
        url: "admin/ServiesByHospitalId/query",
        params: "page=1&start=0&limit=1000"
    },
    MedicData:{
        url: "admin/MedicalTreatmentCombination/query",
        params: "hid=1&search=all&page=1&start=0&limit=1000"
    },
    telemedicineData:{
        url: "admin/telemedicine/group/query",
        params: "hid=1&code=system&page=1&start=0&limit=1000"
    },
    PcEvaData:{
        url: "Admin/data/pc/query",
        params: "hid=0&page=1&start=0&limit=10000"
    },
    AppEvaData:{
        url: "Admin/data/app/query",
        params: "hid=0&page=1&start=0&limit=10000"
    },
    WebYesData:{
        url: "admin/data/pcweb/query",
        params: "hid=0&page=1&start=0&limit=10000"
    },
    WebNoData:{
        url: "admin/data/pcweb2/query",
        params: "hid=0&page=1&start=0&limit=10000"
    },
    iconstyle: { fontSize: '20px' , cursor: 'pointer'},
    formItemLayout: {
        labelCol: {
          xs: { span: 24 },
          sm: { span: 4 },
        },
        wrapperCol: {
          xs: { span: 24 },
          sm: { span: 20 },
        },
    },
    ServiesParams:{
        url: "admin/serviesItem/query",
        params: "page=1&start=0&limit=1000"
    },
    Stat1Data:{
        url: 'admin/stat/1',
        params: 'aid=1&page=1&start=0&limit=1000'
    },

    StatNo1Data:{
        url_: 'admin/stat/no1',
        params_: 'aid=1&page=1&start=0&limit=1000'
    },
    RegionData:{
        url: 'admin/stat/2_1',
        params: 'aid=1&all=true&page=1&start=0&limit=1000'
    },
    DataManAgeColumns:[
        {
            title: '记录类型',
            dataIndex: 'Title',
            width: '15%'
        },
        {
            title: '医院名称',
            dataIndex: 'HospitalName',
            width: '15%'
        },
        {
            title: '姓名',
            dataIndex: 'PatientName',
            width: '10%'
        },
        {
            title: 'PatientId',
            dataIndex: 'PatientId',
            width: '10%'
        },
        {
            title: '医生',
            dataIndex: 'DoctorName',
            width: '10%'
        },
        {
            title: 'DoctorId',
            dataIndex: 'DoctorId',
            width: '10%'
        },
        {
            title: '测评日期',
            dataIndex: 'CreateTime',
            width: '15%'
        },
        {
            title: '更新日期',
            dataIndex: 'UpdateTime',
            width: '15%'
        }
    ],
    FaceSeData_:{
        url: 'admin/stat/14',
        params: 'aid=1&type=%E9%9D%A2%E8%89%B2&page=1&start=0&limit=1000'
    },
    FaceBuData_:{
        url: 'admin/stat/14',
        params: 'aid=1&type=%E5%85%89%E6%B3%BD&page=1&start=0&limit=1000'
    },
    TongueSeData_:{
        url: 'admin/stat/14',
        params: 'aid=1&type=%E5%94%87%E8%89%B2&page=1&start=0&limit=1000'
    },

    start13Data1:{
        url: 'admin/stat/13',
        params: 'aid=1&type=tongue&page=1&start=0&limit=1000'
    },
    start13Data2:{
        url: 'admin/stat/13',
        params: 'aid=1&type=taise&page=1&start=0&limit=1000'
    },
    start13Data3:{
        url: 'admin/stat/13',
        params: 'aid=1&type=taizhi&page=1&start=0&limit=1000'
    },
    start13Data4:{
        url: 'admin/stat/13',
        params: 'aid=1&type=shexing&page=1&start=0&limit=1000'
    },

    pulseLeftData:{
        url: 'admin/stat/12',
        params: 'aid=1&type=%E5%B7%A6%E6%89%8B&page=1&start=0&limit=1000'
    },
    pulseRightData:{
        url: 'admin/stat/12',
        params: 'aid=1&type=%E5%8F%B3%E6%89%8B&page=1&start=0&limit=1000'
    },
    ScaleOldData:{
        url: 'admin/stat/4',
        params: 'aid=1&page=1&start=0&limit=1000'
    },
    ScaleNineData:{
        url: 'admin/stat/3',
        params: 'aid=1&page=1&start=0&limit=1000'
    },
    TcmEvaData:{
        url: 'admin/stat/15',
        params: 'aid=1&page=1&start=0&limit=1000'
    },
    PreEvaData:{
        url: 'admin/stat/6',
        params: 'aid=1&page=1&start=0&limit=1000'
    },
    PartumEvaData:{
        url: 'admin/stat/17',
        params: 'aid=1&page=1&start=0&limit=1000'
    },
    HypEvaData:{
        url: 'admin/stat/8',
        params: 'aid=1&page=1&start=0&limit=1000'
    },
    DiaEvaData:{
        url: 'admin/stat/15',
        params: 'aid=1&page=1&start=0&limit=1000'
    },
    DiseaseEvaData:{
        url: 'admin/stat/9',
        params: 'aid=1&page=1&start=0&limit=1000'
    },
    MedicineEvaData:{
        url: 'admin/stat/10',
        params: 'aid=1&page=1&start=0&limit=1000'
    },
    PhyEvaData:{
        url: 'admin/stat/7',
        params: 'aid=1&page=1&start=0&limit=1000'
    },
    ChdEvaData:{
        url: 'admin/stat/5',
        params: 'aid=1&page=1&start=0&limit=1000'
    },
    ChdBiaseData:{
        url: 'admin/stat/11',
        params: 'aid=1&type=%E6%B0%94%E8%99%9A%E4%BD%93&page=1&start=0&limit=25'
    },
    BeatCaseData:{
        url: 'admin/note/query',
        params: 'page=1&start=0&limit=1000'
    },
    BeatTongueData:{
        url: 'admin/tongue/query',
        params: 'page=1&start=0&limit=1000'
    },
    BeatFaceData:{
        url: 'admin/face/query',
        params: 'page=1&start=0&limit=1000'
    }
}

export function CompareDate(d1,d2){
    return ((new Date(d1.replace(/-/g,"\/"))) - (new Date(d2.replace(/-/g,"\/"))));
}


