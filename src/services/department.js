import { axiosGET, axiosPOST} from '@/utils/admin_axios';
import { CommonInterface  } from './params';

function TagsNode(items){
    let dataList = [];
    items.forEach((i, e) => {
       dataList.push({
          key:  i.Uid,
          HospitalName: i.HospitalName,
          Name: i.Name,
          Memo: i.Memo,
          CreateTime: i.CreateTime,
          UpdateTime: i.UpdateTime
       })
    })
    const result = {
      list:  dataList,
      pagination: {
        total:  dataList.length,
        pageSize: 10,
        current:  1,
      },
    };
  
    return result
}

export async function TagsTreeNode(url, params){
    let items = await axiosGET(url,params);
    let result = await TagsNode(items)
    return result;
}

export async function TagsAddkeShi(HospitalId, Memo, Name){
  const { AddkeShi } = CommonInterface;
  let name = AddkeShi[0].url, params = { HospitalId, Memo, Name };
  return await axiosPOST(name, params);
}

export async function TagsUpdatekeSi(HospitalId, Memo, Name, Uid){
  const { UpdatekeSi } = CommonInterface;
  let name = UpdatekeSi[0].url, params = { HospitalId, Memo, Name, Uid };
  return await axiosPOST(name, params);
}

