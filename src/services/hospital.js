import { axiosGET } from '@/utils/admin_axios';
import { CommonInterface  } from './params';
const { ChartData, ChangeData } = CommonInterface;

function HandleData(items){
    let dataList = [];
    items.forEach(i => {
      dataList.push({
          key: i.Id,
          hospitalName: i.Name,
          Province: i.ProvinceName,
          City: i.CityName,
          Token: i.Token,
          Modular: i.Modules,
          Edition: i.Version,
          CreateTime: i.CreateTime
        })
      });
      const result = {
        list:  dataList,
        pagination: {
          total:  dataList.length,
          pageSize: 10,
          current:  1,
        },
      };
  
      return result;
  }
  

export async function fakeChartData() {
    const { url, params} = ChartData;
    let items = await axiosGET(url ,params);
    let result = await HandleData(items)
    return result;
}

export async function fakeChangeData(params){
    let name = ChangeData[0].url;
    let items = await axiosGET(name,params) ;
    let result = await HandleData(items)
    return result;
}

export async function fetchConstants(url, params){
    return await axiosGET(url,params);
}
