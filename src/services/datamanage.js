import { axiosGET, axiosPOST} from '@/utils/admin_axios';
import { apiBaseUrl } from '@/utils/admin_config';
import axios from 'axios';

function TagsData(items){
    let dataList = [];
    items.forEach((i, e) => {
       dataList.push({
          key: e,
          Uid:  i.Uid,
          Title: 
            i.Title == "常态女性女性健康状态测评"? "常态女性健康状态测评" : 
            i.Title == "中医心理测评"  ? "五态性格问卷测评（简）" :
            i.Title == "中医心理体质辨识" ? "五态性格问卷测评（简）": 
            i.Title == "科研版体质辨识" ? "四诊合参体质辨识":i.Title,
          HospitalName: i.HospitalName,
          PatientName: i.PatientName,
          DoctorName: i.DoctorName,
          PatientId: i.PatientId,
          DoctorId: i.DoctorId,
          Result: i.Result,
          CreateTime: i.CreateTime,
          UpdateTime: i.UpdateTime
       })
    })
    const result = {
      list:  dataList,
      pagination: {
        total:  dataList.length,
        pageSize: 10,
        current:  1,
      },
    };
  
    return result
}

function Tagshospital(items) {
    let dataList = [];
    items.forEach((i, e) => {
       if(i.Id == 0){
           i.Name == "全部医院";
           dataList.push({
            key: i.Id,
            Id:  i.Type,
            Name:  i.Name
         })
       }else{
            dataList.push({
                key: i.Id,
                Id:  i.Type,
                Name:  i.Name
            })
       }
    })
    return dataList
}

export async function fetchDataList(url, params){
    let items = await axiosGET(url,params);
    let result = await TagsData(items)
    return result;
}

export async function fetchhospital(url, params) {
    let items = await axiosGET(url,params);
    let result = await Tagshospital(items)
    return result;
}

export async function exportData(url, params) {
    return await axios.get(`${apiBaseUrl}${url}?${params}`)
}

export const EvaList = [
    {id: 0, title: '全部测评'},
    {id: 1, title: '老年人体质测评'},
    {id: 2, title: '中医体质辨识'},
    {id: 3, title: '五态性格问卷测评(简)'},
    {id: 4, title: '孕期女性健康状态测评'},
    {id: 5, title: '高血压病辅助辨证'},
    {id: 6, title: '儿童体质辨识'},
    {id: 7, title: '中成药辅助辨证'},
    {id: 8, title: '脉象数据采集'},
    {id: 9, title: '舌象数据采集'},
    {id: 10, title: '面象数据采集'},
    {id: 11, title: '糖尿病辅助辨证'},
    {id: 12, title: '四诊合参体质辨识'},
    {id: 13, title: '产后女性健康状态测评'},
    {id: 14, title: '备孕女性健康状态测评'},
    {id: 16, title: '常态女性健康状态测评'},
    {id: 17, title: '五态性格问卷测评'},
]