import axios from 'axios';
import { apiBaseUrl } from './admin_config';
import { message } from 'antd';

export async function axiosGET(name, params){
    let res =  await axios.get(`${apiBaseUrl}${name}?${params}`) ;
    if(res.data.IsSuccess){
        return res.data.Data;
    }else{
        message.error('失败')
    }
}
  
export async function axiosPOST(name, params){
    await axios.post(`${apiBaseUrl}${name}`,params)
    .then(function (response) {
        if(response.status == 200){
            message.success("成功");
        }
    })
    .catch(function (error) {
        console.log(error);
    });
}
