export default {
  'menu.home': '首页',

  'menu.loginid': '账号管理',
  'menu.loginid.hospital': '医院管理',
  'menu.loginid.department': '科室管理',
  'menu.loginid.doctor': '医生管理',
  'menu.loginid.administrators': '管理员账号管理',
  'menu.loginid.hospitalType': '医院类型管理',
  'menu.power': '权限管理',
  'menu.power.rolepermission': '账号权限管理',
  'menu.power.privilegeMgr': '模块管理',
  'menu.power.webEvaAuthorization': 'PC网页版授权管理',
  'menu.power.serviesItem': '服务项目管理',
  'menu.medicConjoin': '医联体管理',
  "menu.telemedicine": '远程医疗管理',
  "menu.dataManAge": '数据管理',
  "menu.dataManAge.PcData": 'PC数据',
  "menu.dataManAge.AppData": 'APP数据',
  "menu.dataManAge.WebDataYes": 'Web数据(有身份证号)',
  "menu.dataManAge.WebDataNo": 'Web数据(无身份证号)',
  "menu.reportCount": '报表统计',
  "menu.reportCount.workloadcount": '工作量统计',
  "menu.reportCount.regionpeople": '地区常住人口年龄分布',
  "menu.reportCount.facescatter": '居民面色分布',
  "menu.reportCount.tonguescatter": '居民舌象分布',
  "menu.reportCount.pulsescatter": '居民常见脉象分布Top7',
  "menu.reportCount.oldscatter": '老年人体质类型分布',
  "menu.reportCount.ninescatter": '成年人9体质类型分布',
  "menu.reportCount.tcmscatter": '科研版12体质类型分布',
  "menu.reportCount.prescatter": '孕期女性中医健康状态分布',
  "menu.reportCount.partumscatter": '产后女性常见中医证型分布Top5',
  "menu.reportCount.hypscatter": '高血压病常见证型分布Top5',
  "menu.reportCount.diascatter": '糖尿病常见证型分布Top5',
  "menu.reportCount.diseasescatter": '居民常见疾病分布',
  "menu.reportCount.medicinescatter": '居民常见中医证型分布',
  "menu.reportCount.physcatter": '居民心理类型分布',
  "menu.reportCount.chdscatter": '儿童体质类型分布',
  "menu.reportCount.chd18scatter": '儿童偏颇体质与症状分布',
  "menu.workloadCount": 'Web工作量统计',
  "menu.workloadCount.pcwebuse": 'Web使用情况',
  "menu.caseManAge": '病历管理',
  "menu.caseManAge.beatcase": '拍病例',
  "menu.caseManAge.beattongue": '智能舌镜',
  "menu.caseManAge.beatface": '面象拍摄',
  "menu.projectManage": '方案管理',
  "menu.xjArticleManage": '宣教文章管理',
  "menu.pushManage": '推送管理',
  "menu.pushManage.pushPatient": '推送患者',
  "menu.pushManage.pushDoctor": '推送医生',
  "menu.patientManage": '患者管理',
  "menu.journalManage": '日志管理',
  "menu.journalManage.ossjournal": 'OSS日志',
  "menu.journalManage.taskjournal": '任务日志',
  "menu.journalManage.selsynjournal": '同步日志',
  'menu.exception': '异常页',
  'menu.exception.not-permission': '403',
  'menu.exception.not-find': '404',
  'menu.exception.server-error': '500',
  'menu.exception.trigger': '触发错误',
  'menu.account': '个人页',
  'menu.account.center': '个人中心',
  'menu.account.settings': '个人设置',
  'menu.account.trigger': '触发报错',
  'menu.account.logout': '退出登录',
};
