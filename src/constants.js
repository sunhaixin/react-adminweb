export const SCALE_OLD = [
    {title: "平和质", dataIndex: 0, align: 'center'},
    {title: "气虚质", dataIndex: 1, align: 'center'},
    {title: "阳虚质", dataIndex: 2, align: 'center'},
    {title: "阴虚质", dataIndex: 3, align: 'center'},
    {title: "痰湿质", dataIndex: 4, align: 'center'},
    {title: "湿热质", dataIndex: 5, align: 'center'},
    {title: "血瘀质", dataIndex: 6, align: 'center'},
    {title: "气郁质", dataIndex: 7, align: 'center'},
    {title: "特禀质", dataIndex: 8, align: 'center'},
]

export const SCALE_PuLsesLeft = [
    {title: "脉促而虚", dataIndex: 0, align: 'center'},
    {title: "脉滑", dataIndex: 1,  align: 'center'},
    {title: "脉结虚", dataIndex: 2, align: 'center'},
    {title: "其它脉", dataIndex: 3, align: 'center'},
    {title: "脉虚弦结", dataIndex: 4, align: 'center'},
    {title: "脉虚弦促", dataIndex: 5, align: 'center'},
    {title: "脉促而滑", dataIndex: 6, align: 'center'}
];

export const SCALE_PuLseRight = [
    {title: "脉滑", dataIndex: 0, align: 'center'},
    {title: "脉结虚", dataIndex: 1,  align: 'center'},
    {title: "其它脉", dataIndex: 2, align: 'center'},
    {title: "脉促而虚", dataIndex: 3, align: 'center'},
    {title: "脉虚弦结", dataIndex: 4, align: 'center'},
    {title: "脉虚弦促", dataIndex: 5, align: 'center'},
    {title: "脉促而滑", dataIndex: 6, align: 'center'}
];

export const SCALE_Tongue = [
    {title: "舌暗红", dataIndex: 0, align: 'center'},
    {title: "舌淡红", dataIndex: 1,  align: 'center'},
    {title: "舌淡", dataIndex: 2, align: 'center'}
];

export const SCALE_Tongue1 = [
    {title: "苔白", dataIndex: 0, align: 'center'},
    {title: "苔黄白想兼", dataIndex: 1,  align: 'center'},
    {title: "苔无", dataIndex: 2, align: 'center'}
];

export const SCALE_Tongue2 = [
    {title: "苔白厚", dataIndex: 0, align: 'center'},
    {title: "苔薄", dataIndex: 1,  align: 'center'},
    {title: "苔无", dataIndex: 2, align: 'center'}
];

export const SCALE_Tongue3 = [
    {title: "有裂痕", dataIndex: 0, align: 'center'},
    {title: "胖", dataIndex: 1,  align: 'center'},
    {title: "有齿痕", dataIndex: 2, align: 'center'}
];

export const SCALE_Face = [
    {title: "面色白", dataIndex: 0, align: 'center'},
    {title: "面色红黄", dataIndex: 1,  align: 'center'},
    {title: "面色白,苍白", dataIndex: 2, align: 'center'},
    {title: "面色赤", dataIndex: 3, align: 'center'}
];

export const SCALE_Face1 = [
    {title: "少量光泽", dataIndex: 0, align: 'center'},
    {title: "有光泽", dataIndex: 1, align: 'center'},
    {title: "无光泽", dataIndex: 2, align: 'center'}
];

export const SCALE_Face2 = [
    {title: "唇色暗红", dataIndex: 0, align: 'center'},
    {title: "唇色红", dataIndex: 1, align: 'center'},
    {title: "唇色青紫", dataIndex: 2, align: 'center'},
    {title: "唇色淡", dataIndex: 3, align: 'center'}
]

export const SCALE_TCM = [
    {title: "平和质", dataIndex: 0, align: 'center'},
    {title: "气虚质", dataIndex: 1, align: 'center'},
    {title: "阳虚质", dataIndex: 2, align: 'center'},
    {title: "阴虚质", dataIndex: 3, align: 'center'},
    {title: "痰湿质", dataIndex: 4, align: 'center'},
    {title: "湿热质", dataIndex: 5, align: 'center'},
    {title: "血瘀质", dataIndex: 6, align: 'center'},
    {title: "血虚质", dataIndex: 7, align: 'center'},
    {title: "气郁质", dataIndex: 8, align: 'center'},
    {title: "特禀质", dataIndex: 9, align: 'center'},
    {title: "内热质", dataIndex: 10, align: 'center'},
    {title: "内寒质", dataIndex: 11, align: 'center'},
]

export const SCALE_PRE = [
    {title: "痰湿", dataIndex: 0, align: 'center'},
    {title: "气滞", dataIndex: 1, align: 'center'},
    {title: "脾胃虚弱", dataIndex: 2, align: 'center'},
    {title: "痰热", dataIndex: 3, align: 'center'},
    {title: "肝肾阴虚", dataIndex: 4, align: 'center'},
    {title: "阴虚血热", dataIndex: 5, align: 'center'},
    {title: "心气虚", dataIndex: 6, align: 'center'},
    {title: "健康体", dataIndex: 7, align: 'center'},
    {title: "肝火", dataIndex: 8, align: 'center'},
    {title: "脾肾阳虚", dataIndex: 9, align: 'center'},
    {title: "气虚", dataIndex: 10, align: 'center'},
    {title: "肾气虚", dataIndex: 11, align: 'center'},
    {title: "津亏气弱", dataIndex: 12, align: 'center'}
]

export const SCALE_PARTUM = [
    {title: "血瘀", dataIndex: 0, align: 'center'},
    {title: "痰湿", dataIndex: 1, align: 'center'},
    {title: "肾精亏", dataIndex: 2, align: 'center'},
    {title: "热", dataIndex: 3, align: 'center'},
    {title: "寒", dataIndex: 4, align: 'center'},
]

export const SCALE_HYP = [
    {title: "肾精亏虚", dataIndex: 0, align: 'center'},
    {title: "痰（湿）热蕴结", dataIndex: 1, align: 'center'},
    {title: "心阳虚", dataIndex: 2, align: 'center'},
    {title: "痰浊壅盛", dataIndex: 3, align: 'center'},
    {title: "阴虚火旺", dataIndex: 4, align: 'center'},
]

export const SCALE_DIA = [
    {title: "平和质", dataIndex: 0, align: 'center'},
    {title: "气虚质", dataIndex: 1, align: 'center'},
    {title: "阳虚质", dataIndex: 2, align: 'center'},
    {title: "阴虚质", dataIndex: 3, align: 'center'},
    {title: "痰湿质", dataIndex: 4, align: 'center'},
    {title: "湿热质", dataIndex: 5, align: 'center'},
    {title: "血瘀质", dataIndex: 6, align: 'center'},
    {title: "血虚质", dataIndex: 7, align: 'center'},
    {title: "气郁质", dataIndex: 8, align: 'center'},
    {title: "特禀质", dataIndex: 9, align: 'center'},
    {title: "内热质", dataIndex: 10, align: 'center'},
    {title: "内寒质", dataIndex: 11, align: 'center'}
]

export const SCALE_DISEASE = [
    {title: "自汗", dataIndex: 0, align: 'center'},
    {title: "畏寒", dataIndex: 1, align: 'center'},
    {title: "中暑", dataIndex: 2, align: 'center'},
    {title: "痄腮   ", dataIndex: 3, align: 'center'},
    {title: "中风", dataIndex: 4, align: 'center'},
    {title: "手足凉", dataIndex: 5, align: 'center'},
    {title: "四肢凉", dataIndex: 6, align: 'center'},
    {title: "痔疮", dataIndex: 7, align: 'center'},
    {title: "容易感冒", dataIndex: 8, align: 'center'},
    {title: "寒战", dataIndex: 9, align: 'center'}
]

export const SCALE_MEDICINE = [
    {title: "阳虚寒凝证", dataIndex: 0, align: 'center'},
    {title: "脾肾阳气虚证", dataIndex: 1, align: 'center'},
    {title: "风寒束表证", dataIndex: 2, align: 'center'},
    {title: "风袭表疏证   ", dataIndex: 3, align: 'center'},
    {title: "肾阴阳两虚证", dataIndex: 4, align: 'center'},
    {title: "肾阳气虚证", dataIndex: 5, align: 'center'},
    {title: "表热里寒证", dataIndex: 6, align: 'center'},
    {title: "阳气虚证", dataIndex: 7, align: 'center'},
    {title: "脾阳气虚证", dataIndex: 8, align: 'center'},
    {title: "肺卫气虚证", dataIndex: 9, align: 'center'}
]

export const SCALE_CHD = [
    {title: "健康体", dataIndex: 0, align: 'center'},
    {title: "气虚体", dataIndex: 1, align: 'center'},
    {title: "阳虚体", dataIndex: 2, align: 'center'},
    {title: "痰湿体", dataIndex: 3, align: 'center'},
    {title: "积滞体", dataIndex: 4, align: 'center'},
    {title: "肝火体", dataIndex: 5, align: 'center'},
    {title: "热盛体", dataIndex: 6, align: 'center'},
    {title: "高敏体", dataIndex: 7, align: 'center'},
    {title: "怯弱体", dataIndex: 8, align: 'center'},
]