import React, { PureComponent } from 'react';
import { Icon } from 'antd';
import styles from './UserLayout.less';

class UserLayout extends PureComponent {

  render() {
    const { children } = this.props;
    return (
      <div className={styles.container}>
        <div className={styles.lang} />
        <div className={styles.content}>
          <div className={styles.top}>
            <div className={styles.header} />
            <div className={styles.desc} />
          </div>
          {children}
        </div>
      </div>
    );
  }
}

export default UserLayout;
