English | [简体中文](./README.zh-CN.md)

<h1 align="center">react-AdminWeb</h1>

<div align="center">

An out-of-box UI solution for enterprise applications as a React boilerplate.

![](https://user-images.githubusercontent.com/8186664/44953195-581e3d80-aec4-11e8-8dcb-54b9db38ec11.png)

</div>

## Participate OR Contribution

- [sunhaixin](https://gitee.com/github-29425276/react-AdminWeb)