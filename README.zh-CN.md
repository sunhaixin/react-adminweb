[English](./README.md) | 简体中文

<h1 align="center">react-AdminWeb</h1>

<div align="center">

开箱即用的中台前端/设计解决方案。

![](https://user-images.githubusercontent.com/8186664/44953195-581e3d80-aec4-11e8-8dcb-54b9db38ec11.png)

</div>

## 参与贡献

- [sunhaixin](https://gitee.com/github-29425276/react-AdminWeb)