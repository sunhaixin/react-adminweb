export default [
  // user
  {
    path: '/user',
    component: '../layouts/UserLayout',
    routes: [
      { path: '/user', redirect: '/user/login' },
      { path: '/user/login', component: './User/Login' }
    ],
  },
  // app
  {
    path: '/',
    component: '../layouts/BasicLayout',
    Routes: ['src/pages/Authorized'],
    authority: ['admin', 'user'],
    routes: [
      { path: '/', redirect: '/LoginId/hospital' },
       // loginid
      {
        path: '/loginid',
        name: 'loginid',
        icon: 'global',
        routes: [
          {
            path: '/loginid/hospital',
            name: 'hospital',
            component: './LoginId/HospitalManAge/HospitalGL',
          },
          {
            path: '/loginid/department',
            name: 'department',
            component: './LoginId/DepartmentManAge/Department',
          },
          {
            path: '/loginid/doctors',
            name: 'doctor',
            component: './LoginId/DoctorsManAge/Doctors',
          },
          {
            path: '/loginid/administrators',
            name: 'administrators',
            component: './LoginId/Administrators/Administrators',
          },
          {
            path: '/loginid/hospitalType',
            name: 'hospitalType',
            component: './LoginId/HospitalTypeManAge/HospitalType',
          }
        ],
      },
      // power
      {
        path: '/power',
        name: 'power',
        icon: 'dashboard',
        routes: [
          {
            path: '/power/rolepermission',
            name: 'rolepermission',
            component: './Power/Rolepermission',
          },
          // {
          //   path: '/power/privilegeMgr',
          //   name: 'privilegeMgr',
          //   // component: './Power/Rolepermission/Rolepermission'
          // },
          {
            path: '/power/webEvaAuthorization',
            name: 'webEvaAuthorization',
            component: './Power/WebPcEvaManAge'
          },
          {
            path: '/power/serviesItem',
            name: 'serviesItem',
            component: './Power/ServiesItemManAge',
          }
        ],
      },
      //MedicConjoin
      {
        path: '/medicConjoin',
        name: 'medicConjoin',
        icon: 'dashboard',
        component: './MedicConjoin/MedicConjoin'
      },
      //Telemedicine
      {
        path: '/telemedicine',
        name: 'telemedicine',
        icon: 'dashboard',
        component: './Telemedicine/telemedicine'
      },
      //DataManAge
      {
        path: '/dataManAge',
        name: 'dataManAge',
        icon: 'dashboard',
        routes: [
          {
            path: '/dataManAge/PcData',
            name: 'PcData',
            component: './DataManAge/PcDataManAge/PcDataManAge',
          },
          {
            path: '/dataManAge/AppData',
            name: 'AppData',
            component: './DataManAge/AppDataManAge/AppDataManAge',
          },
          {
            path: '/dataManAge/WebDataYes',
            name: 'WebDataYes',
            component: './DataManAge/WebDataManAgeYes/WebDataManAgeYes',
          },
          {
            path: '/dataManAge/WebDataNo',
            name: 'WebDataNo',
            component: './DataManAge/WebDataManAgeNo/WebDataManAgeNo',
          }
        ],
      },
      //ReportCount
      {
        path: '/reportCount',
        name: 'reportCount',
        icon: 'dashboard',
        routes: [
          {
            path: '/reportCount/workloadcount',
            name: 'workloadcount',
            component: './ReportCount/WorkloadCount/WorkloadCount',
          },
          {
            path: '/reportCount/RegionPeople',
            name: 'regionpeople',
            component: './ReportCount/RegionPeople/RegionPeople',
          },
          {
            path: '/reportCount/FaceScatter',
            name: 'facescatter',
            component: './ReportCount/FaceScatter/FaceScatter',
          },
          {
            path: '/reportCount/TongueScatter',
            name: 'tonguescatter',
            component: './ReportCount/TongueScatter/TongueScatter',
          },
          {
            path: '/reportCount/PulseScatter',
            name: 'pulsescatter',
            component: './ReportCount/PulseScatter/PulseScatter',
          },
          {
            path: '/reportCount/ScaleOld',
            name: 'oldscatter',
            component: './ReportCount/ScaleReport/ScaleReport',
          },
          {
            path: '/reportCount/ScaleNine',
            name: 'ninescatter',
            component: './ReportCount/ScaleReport/ScaleReport',
          },
          {
            path: '/reportCount/ScaleTcm',
            name: 'tcmscatter',
            component: './ReportCount/ScaleReport/TcmScaleReport',
          },
          {
            path: '/reportCount/ScalePre',
            name: 'prescatter',
            component: './ReportCount/ScaleReport/PreScaleReport',
          },
          {
            path: '/reportCount/ScalePartum',
            name: 'partumscatter',
            component: './ReportCount/ScaleReport/PartumScaleReport',
          },
          {
            path: '/reportCount/ScaleHyp',
            name: 'hypscatter',
            component: './ReportCount/ScaleReport/HypScaleReport',
          },
          {
            path: '/reportCount/ScaleDia',
            name: 'diascatter',
            component: './ReportCount/ScaleReport/DiaScaleReport',
          },
          {
            path: '/reportCount/ScaleDisease',
            name: 'diseasescatter',
            component: './ReportCount/ScaleReport/DiseaseReport',
          },
          {
            path: '/reportCount/ScaleMedicine',
            name: 'medicinescatter',
            component: './ReportCount/ScaleReport/MedicineReport',
          },
          {
            path: '/reportCount/ScalePhy',
            name: 'physcatter',
            component: './ReportCount/ScaleReport/PhySclaeReport',
          },
          {
            path: '/reportCount/ScaleChd',
            name: 'chdscatter',
            component: './ReportCount/ScaleReport/ChdScaleReport',
          },
          {
            path: '/reportCount/ScaleChdBiased',
            name: 'chd18scatter',
            component: './ReportCount/ScaleReport/ChdBiasedReport',
          },
        ]
      },
      //WorkloadCount
      {
        path: '/WorkloadCount',
        name: 'workloadCount',
        icon: 'dashboard',
        routes:[
          {
            path: '/WorkloadCount/PcWebUse',
            name: 'pcwebuse',
            component: './WorkloadCount/PcWebUse/PcWebUse',
          },
        ]
      },
      //caseManAge
      {
        path: '/CaseManAge',
        name: 'caseManAge',
        icon: 'dashboard',
        routes:[
          {
            path: '/CaseManAge/BeatCaseManAge',
            name: 'beatcase',
            component: './CaseManAge/BeatCaseManAge/BeatCaseManAge',
          },
          {
            path: '/CaseManAge/BeatTongue',
            name: 'beattongue',
            component: './CaseManAge/BeatTongueManAge/BeatTongueManAge',
          },
          {
            path: '/CaseManAge/BeatFace',
            name: 'beatface',
            component: './CaseManAge/BeatFaceManAge/BeatFaceManAge',
          },
        ]
      },
      // ProjectManage
      {
        path: '/ProjectManage',
        name: 'projectManage',
        icon: 'dashboard',
        component: './Exception/404',
      },
      //XJArticleManage
      {
        path: '/XJArticleManage',
        name: 'xjArticleManage',
        icon: 'dashboard',
        component: './Exception/404',
      },
      // PushManage
      {
        path: '/PushManage',
        name: 'pushManage',
        icon: 'dashboard',
        routes:[
          {
            path: '/PushManage/pushPatient',
            name: 'pushPatient',
            component: './Exception/404',
          },
          {
            path: '/PushManage/pushDoctor',
            name: 'pushDoctor',
            component: './Exception/404',
          }
        ]
      },
      {
        path: '/PatientManage',
        name: 'patientManage',
        icon: 'dashboard',
        component: './Exception/404',
      },
      // journalManage
      {
        path: '/JournalManage',
        name: 'journalManage',
        icon: 'dashboard',
        routes:[
          {
            path: '/journalManage/ossjournal',
            name: 'ossjournal',
            component: './Exception/404',
          },
          {
            path: '/journalManage/taskjournal',
            name: 'taskjournal',
            component: './Exception/404',
          },
          {
            path: '/journalManage/selsynjournal',
            name: 'selsynjournal',
            component: './Exception/404',
          }
        ]
      },
      {
        name: 'exception',
        icon: 'warning',
        path: '/exception',
        routes: [
          // exception
          {
            path: '/exception/403',
            name: 'not-permission',
            component: './Exception/403',
          },
          {
            path: '/exception/404',
            name: 'not-find',
            component: './Exception/404',
          },
          {
            path: '/exception/500',
            name: 'server-error',
            component: './Exception/500',
          },
          {
            path: '/exception/trigger',
            name: 'trigger',
            hideInMenu: true,
            component: './Exception/TriggerException',
          },
        ],
      },
      {
        component: '404',
      },
    ],
  },
];
